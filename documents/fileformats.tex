\documentclass[10pt]{article}
\usepackage{color} %used for font color
\usepackage{times}
\usepackage{amssymb} %maths
\usepackage{amsmath} %maths
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{url}

\begin{document}

\title{Guide to Nigel File Formats}
\author{Jonah Scheinerman}
\date{}
\maketitle

\tableofcontents

\section{Introduction}
By default Nigel comes with a wide variety of board configurations, alphabets, and dictionaries to make your Scrabble analysis as easy as possible. Below are the built in resource files, including the path and what they are used for:\\


\textbf{Boards.} Found in \verb|nigel/resources/boards|:
\begin{center}
\begin{tabular}{|l|l|}
\hline
\emph{Name} & \emph{Description} \\ \hline\hline
\verb|scrabble.board| & Standard Scrabble board \\ \hline
\verb|superscrabble.board| & Super Scrabble board \\ \hline
\verb|wwf.board| & Words With Friends board \\ \hline
\end{tabular}
\end{center}

\textbf{Alphabets.} Found in \verb|nigel/resources/alphabets|:
\begin{center}
\begin{tabular}{|l|l|}
\hline
\emph{Name} & \emph{Description} \\ \hline\hline
\verb|english.abc| & English Scrabble tilebag \\ \hline
\verb|english-super.abc| & English Super Scrabble tilebag \\ \hline
\verb|english-wwf.abc| & English Words With Friends tilebag \\ \hline
\verb|french.abc| & French Scrabble tilebag \\ \hline
\verb|italian.abc| & Italian Scrabble tilebag \\ \hline
\end{tabular}
\end{center}

\textbf{Dictionaries.} Found in \verb|nigel/resources/dictionaries|:
\begin{center}
\begin{tabular}{|l|l|}
\hline
\emph{Name} & \emph{Description} \\ \hline\hline
\verb|CSW12.nigeldict| & Collins Scrabble Words 2012\\ \hline
\verb|ODS4.nigeldict| & Official French Scrabble word list v. 4\\ \hline
\verb|ODS5.nigeldict| & Official French Scrabble word list v. 5\\ \hline
\verb|OSPD4.nigeldict| & Official Scrabble Player's Dictionary v. 4 \\ \hline
\verb|OSWI.nigeldict| & ?\\ \hline
\verb|SOWPODS.nigeldict| & Older version of CSW12 \\ \hline
\verb|TWL06.nigeldict| & Tournament Word List 2006 \\ \hline
\verb|TWL98.nigeldict| & Tournament Word List 1998 \\ \hline
\verb|WWF.nigeldict| & Words With Friends \\ \hline
\verb|ZINGA05.nigeldict| & Zingarelli (Italian) 2005 \\ \hline
\end{tabular}
\end{center}

It is our hope that this should be sufficient for pretty much all of your analysis needs, though we understand if you want to experiment with different configurations. The purpose of this guide is to provide the specifications for the various file formats that you will need to set up your own boards, tilebags and dictionaries.


\section{Board Configuation (\texttt{*.board})}
This file format defines a Scrabble board including its size, premium squares, and location of the start square. Files of this format should use the file suffix \verb|.board|. For examples of this file format see \verb|nigel/resources/boards|.

This format is expected to be in plain text, with newlines at the end of each line. There should be no empty lines. The format consists of two sections. The first section of the file is attribute definitions. Attributes must be one per line and must take the form: \verb|ATTRIBUTE_NAME=value| (note that there is no extraneous whitespace). The following are the possible attributes:

\begin{center}
\begin{tabular}{|l|l|}
\hline
\emph{Name} & \emph{Function} \\\hline\hline
\verb|NAME| & Defines the name of the board configurations.\\\hline
\verb|ROWS| & Defines the number of rows of the board. This should be an integer greater than 0. \\ \hline
\verb|COLS| & Defines the number of columns of the board. This should be an integer greater than 0. \\ \hline
\verb|START_ROW| & Defines the row which the first play should pass through. This should be an integer between\\
& 1 and \verb|ROWS| (inclusive).\\\hline
\verb|START_ROW| & Defines the column which the first play should pass through. This should be an integer between\\
& 1 and \verb|COLS| (inclusive).\\\hline
\end{tabular}
\end{center}

The \verb|NAME| attribute is optional, but all other attributes are required. For \verb|START_ROW| and \verb|START_COL| note that the first row is row 1 and the first column is column 1 (rather than 0).

After the attribute definitions comes the board definition. Each line should be a sequence of characters which defines the location of premium squares on the board. The number of lines should be \verb|ROWS| and the number of characters in each line should be \verb|COLS|. To indicate a square which is not a premium square (no multiplier) use a \verb|-|. To specify a letter multiplying square use the uppercase letters of the alphabet. Thus \verb|B| specifies a double-letter score, \verb|C| specifies a triple-letter score, \verb|D| is a quadruple-letter score, and so on. To specify a word multiplying square use the digits \verb|2| through \verb|9|. Thus \verb|2| is a double-word score, \verb|3| is a triple-word score, and so on. 

The file should not end with a newline.

The following is an example of a correct board layout file (this file can be found at the following file path: \verb|nigel/resources/boards/scrabble.board|):\\

\begin{tabular}{|l}
\verb|NAME=Scrabble| \\
\verb|ROWS=15| \\
\verb|COLS=15| \\
\verb|START_ROW=8| \\
\verb|START_COL=8| \\
\verb|3--B---3---B--3| \\
\verb|-2---C---C---2-| \\
\verb|--2---B-B---2--| \\
\verb|B--2---B---2--B| \\
\verb|----2-----2----| \\
\verb|-C---C---C---C-| \\
\verb|--B---B-B---B--| \\
\verb|3--B---2---B--3| \\
\verb|--B---B-B---B--| \\
\verb|-C---C---C---C-| \\
\verb|----2-----2----| \\
\verb|B--2---B---2--B| \\
\verb|--2---B-B---2--| \\
\verb|-2---C---C---2-| \\
\verb|3--B---3---B--3| \\
\end{tabular}

\section{Alphabet / Tilebag (\texttt{*.abc})}

This file format defines an alphabet (or tilebag configuration)  which defines the letters in the language, their scores and frequencies. Files of this format should use the file suffix \verb|.abc|. For examples of this file format see the directory \verb|nigel/resources/alphabets|.

This format is expected to be in plain text, with newlines at the end the end of each line. There should be no empty lines. The format consists of two sections, much like the \verb|.board| file format. The first section is attribute definitions. Attributes must be one per line and must take the form: \verb|ATTRIBUTE_NAME=value| (note that there is no extraneous whitespace). The following are the possible attributes:

\begin{center}
\begin{tabular}{|l|l|}
\hline
\emph{Name} & \emph{Function} \\\hline\hline
\verb|NAME| & Defines the name of the tilebag configuration. \\ \hline
\verb|BOARD| & Specifies the name of the board that this alphabet goes with.\\ \hline
\end{tabular}
\end{center}

Both attributes are optional. The \verb|BOARD| attribute is for future usage, and serves no real purpose at the moment. In the future we suspect it will be useful to pair boards and alphabets, so we provide this option now. The value of this attribute should be the same as the name of the \verb|NAME| attribute in the corresponding \verb|.board| file.

Following the attributes definition comes the tile score and frequency definitions. This is a series of tab-delimited lines which are of the form \emph{$<$letter$>$tab$<$score$>$tab$<$frequency$>$}. The first character in each line should be the letter of the tile. For the moment this can only be a single character and should be one of \verb|A| through \verb|Z| or \verb|*| (to indicate blank tiles). The score should be a nonnegative integer indicating the value of the tile. The frequency should be a positive integer indicating the number of tiles of that type that are in a full tilebag. Tabs should be tab characters rather than any other form of whitespace.

The file should not end with a newline.

The following is an example of a correct alphabet (this alphabet resource file can be found at the following file path: \verb|nigel/resources/alphabets/english-super.abc|):\\

\begin{tabular}{|l}
\verb|NAME=English (Super)| \\
\verb|BOARD=Super Scrabble| \\
\verb|*   0   4| \\
\verb|A   1   16| \\
\verb|B   3   4| \\
\verb|C   3   6| \\
\verb|D   2   8| \\
\verb|E   1   24| \\
\verb|F   4   4| \\
\verb|G   2   5| \\
\verb|H   4   5| \\
\verb|I   1   13| \\
\verb|J   8   2| \\
\verb|K   5   2| \\
\verb|L   1   7| \\
\verb|M   3   6| \\
\verb|N   1   13| \\
\verb|O   1   15| \\
\verb|P   3   4| \\
\verb|Q   10  2| \\
\verb|R   1   13| \\
\verb|S   1   10| \\
\verb|T   1   15| \\
\verb|U   1   7| \\
\verb|V   4   3| \\
\verb|W   4   4| \\
\verb|X   8   2| \\
\verb|Y   4   4| \\
\verb|Z   10  2| \\
\end{tabular}

\section{Nigel Dictionary (\texttt{*.nigeldict})}
When using your own dictionary when working with Nigel it is possible to use simply a text file which consists of a single word on every line. However, for internal usage Nigel must convert this to a special format before it can perform analysis. This is okay if you're only planning to use this word list once. However, for repeated usage, it behooves you to make use of a special Nigel dictionary. Nigel dictionaries are directories which contain a number of different representations of a word list to make reading in and using dictionaries much faster and more space efficient.

We have provided a utility for you to construct your own Nigel dictionary automatically given a word list. The word list must be a text file with a single word on each line (each word must only use the letters A through Z, upper or lower case). To construct your own Nigel dictionary create a new directory with the name \verb|<name>.nigeldict| where \verb|<name>| is whatever you'd like. Then, make nigel and run the following command:

\begin{verbatim}
./nigel --mode builddict --wordlist path/to/in.txt --out path/to/<name>.nigeldict
\end{verbatim}

Or more succinctly:

\begin{verbatim}
./nigel -m builddict -w path/to/in.txt -o path/to/<name>.nigeldict
\end{verbatim}

This will build a Nigel dictionary in your specified output directory and in the future when using Nigel you can specify that Nigel uses this dictionary via the \verb|--dictionary| option.

Since we provide te capability for you to easily make your own Nigel dictionaries, it isn't really necessary that you understand the file format for Nigel dictionaries (or DAWGs for that matter, see next section). Nonetheless, we will give the specification for the \verb|.nigeldict| format here.

A Nigel dictionary is a directory whose name uses the suffix \verb|.nigeldict| in this directory are a number of files which specify the word list. There must be in the directory a file called \verb|list.txt| which contains every word in the word list, one per line in all capital letters with newlines as line separators. Additionally, there must be files of the format \verb|N.dawg| where \verb|N| is an integer between 2 and 15 inclusive. Each of these is a DAWG containing only the words of the length \verb|N|. There should be a single DAWG for each of the different word lengths contained in the word list. For more on the \verb|.dawg| file format see the next section. Finally the last (optional) file that can be in the directory should be \verb|aliases.txt| which is a file containing each name of the dictionary on each line.

For examples of correct Nigel dictionaries see \verb|nigel/resources/dictionaries|.

\section{Directed Acyclic Word Graph (\texttt{*.dawg})}

A directed acyclic word graph is a data structure that represents a list of words. It is similar to a trie (prefix retrieval tree) but is much more space efficient. See \url{http://en.wikipedia.edu/Directed_Acyclic_Word_Graph} for more information. The \verb|.dawg| file format is a way of storing a DAWG as effectively an adjacency list.

Building a DAWG (particularly one that has been minimized fully) is quite difficult to do by hand so again we provide a utility to do it for you. If you have a word list as a text file of words, one on each line then you can go into the Nigel directory, make Nigel and run the following command:

\begin{verbatim}
./nigel --mode builddawg --wordlist path/to/in.txt --out path/to/out.dawg
\end{verbatim}

Or more succinctly:

\begin{verbatim}
./nigel -m builddawg -w path/to/in.txt -o path/to/out.dawg
\end{verbatim}

Then once you have a DAWG you can more quickly create and use a memory efficient DAWG using the C++ API by the method \verb|DAWG::readFromDawgFile()|. For the typical user, this will not be necssary as the only time a DAWG is created is when creating a Nigel dictionary (see previous section).

The above method will create a fully minimized DAWG and save it in the appropriate format, so it isn't really necessary to understand how to use the file format. Nonetheless, we include the specification below.

The file must be plain text and use newlines as line separators. Since a DAWG is a tree structure each line represents a node in the tree and its connections to other nodes. The first line should be a nonnegative integer which is the number of nodes in the DAWG. From then on each line represents a node in the tree. The second line (the line after the number of nodes) represents node 0. The third line represnts node 1, the fourth node 2, etc.

Each line should be a tab delimited list of values. The first value must be the letter represented by the node which should be one \verb|A| through \verb|Z|. The only exception to this is that one node should be the root node (which has no letter), and for this node the first value on the line should be \verb|root|. Then every other tab-delimited value in the line should be an integer which refers to the another node, marking that node as its child. (Each node has an integer value associated with it, see above). Finally, if this node represents a complete word then the very last value on the line should be \verb|EOW|. The file should end in a newline.
\end{document}









