#include <Alphabet.h>
#include <Tile.h>

#include <gmock/gmock.h>

using namespace testing;
using namespace nigel;
using namespace std;

TEST(AlphabetTest, FromStream1) {
    stringstream ss;
    ss << "NAME=TestAlphabet\n"
       << "A\t1\t20\n"
       << "B\t2\t10";
    Alphabet alphabet(ss);
    EXPECT_THAT(alphabet.GetLetters(), UnorderedElementsAre('A', 'B'));
    EXPECT_THAT(alphabet.ContainsLetter('A'), Eq(true));
    EXPECT_THAT(alphabet.ContainsLetter('B'), Eq(true));
    EXPECT_THAT(alphabet.ContainsLetter('C'), Eq(false));
    EXPECT_THAT(alphabet.GetBlankCharacter(), Eq('?'));
    EXPECT_THAT(alphabet.TileScore('A'), Eq(1));
    EXPECT_THAT(alphabet.TileScore('B'), Eq(2));
    EXPECT_THAT(alphabet.TileFrequency('A'), Eq(20));
    EXPECT_THAT(alphabet.TileFrequency('B'), Eq(10));
    EXPECT_THAT(alphabet.CreateTile('A'), Eq(Tile('A', 1)));
    EXPECT_THAT(alphabet.CreateTile('B'), Eq(Tile('B', 2)));
    EXPECT_THAT(alphabet.CreateBlank(), Eq(Tile('?', 0)));
    EXPECT_THAT(alphabet.GetName(), Eq("TestAlphabet"));
}

TEST(AlphabetTest, FromStream2) {
    stringstream ss;
    ss << "&   0   5\n"
       << "X   1   5";
    Alphabet alphabet(ss);
    EXPECT_THAT(alphabet.GetLetters(), UnorderedElementsAre('&', 'X'));
    EXPECT_THAT(alphabet.GetBlankCharacter(), Eq('&'));
    EXPECT_THAT(alphabet.TileScore('&'), Eq(0));
    EXPECT_THAT(alphabet.TileFrequency('&'), Eq(5));
    EXPECT_THAT(alphabet.CreateTile('&'), Eq(Tile('&', 0)));
    EXPECT_THAT(alphabet.CreateBlank(), Eq(Tile('&', 0)));
}

TEST(AlphabetTest, FromStream3) {
    stringstream ss;
    ss << "BOARD=Holla";
    Alphabet alphabet(ss);
    EXPECT_THAT(alphabet.GetLetters(), IsEmpty());
    EXPECT_THAT(alphabet.GetBoardName(), Eq("Holla"));
}
