/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file main.cpp
 * @brief Main method.
 */

#include <CLI.h>
#include <Game.h>
#include <Move.h>
#include <UtilityAnalyzer.h>

#include <cmath>
#include <iostream>
#include <chrono>
#include <fstream>

using namespace std;

using namespace nigel;
using namespace nigel::analyze;
using namespace std;


int main(int argc, char* argv[]) {
    Game g;
    g.AddPlayer("1");
    g.AddPlayer("2");
    g.Play();
}
