/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Nigel3.1.h"
#include "TileBag.h"
#include "Player.h"
#include "Board.h"

#include <set>

using namespace nigel;
using namespace std;

const LinearFunction Nigel<3,1>::kSampleUtilityFn(1.6877, -23.942);

Nigel<3,1>::Nigel(int sample_size) : sample_size_(sample_size) {}

void Nigel<3,1>::Preprocess(vector<shared_ptr<Move>>& moves) {
    const Board& board = *GetPlayer()->GetBoard();
    auto alphabet = GetPlayer()->GetTileBag()->GetAlphabet();

    sample_space_ = alphabet->RemainingTiles(board, GetPlayer()->GetRack());
}

double Nigel<3,1>::Utility(Rack& leave) {
    if(GetPlayer()->GetTileBag()->IsEmpty())
        return -2 * leave.Score();

    int num_to_sample = 7 - leave.size();
    if(sample_space_.size() < num_to_sample)
        return Nigel22Resources::Instance().ComputeUtility(leave);

    string leave_str = leave.ToString();
    double total = 0;
    for(int i = 0; i < sample_size_; i++) {
        string sample = leave_str + SampleRemaining(num_to_sample);
        sort(sample.begin(), sample.end());
        total += SampleUtility(sample);
    }

    return total / sample_size_;
}

double Nigel<3,1>::SubsetsUtility(const string& s) {
    double total = 0;
    for(int i = 0; i < 7; i++) {
        string t = s.substr(0, i) + s.substr(i+1);
        for(int j = i; j < 6; j++) {
            string u = t.substr(0, j) + t.substr(j+1);
            total += Nigel22Resources::Instance().ComputeNLetterUtil(u);
        }
    }
    return total / 21;
}

double Nigel<3,1>::SampleUtility(const string& sample) {
    return kSampleUtilityFn(SubsetsUtility(sample));
}

string Nigel<3,1>::SampleRemaining(int size) {
    string sample;
    set<int> selectedIndices;
    while(sample.size() < size) {
        int randIndex = unif(0, sample_space_.size() - 1);
        if(selectedIndices.count(randIndex) == 0) {
            sample += sample_space_[randIndex].GetLetter();
            selectedIndices.insert(randIndex);
        }
    }
    return sample;
}
