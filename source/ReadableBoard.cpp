/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file ReadableBoard.cpp
 * @brief Implementation of the ReadableBoard class.
 */

#include "ReadableBoard.h"

#include <iostream>

using namespace std;
using namespace nigel;

void ReadableBoard::Print(ostream& os) const {
    for(int row = 0; row < GetRows(); row++) {
        for(int col = 0; col < GetColumns(); col++) {
            const Tile& tile = (*this)(row,col);
            if(tile.IsEmpty()) {
                os << "-";
            } else {
                os << tile.GetLetter();
            }
        }
        os << endl;
    }
}
