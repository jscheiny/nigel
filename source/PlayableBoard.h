/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file PlayableBoard.h
 * @brief Declaration of the PlayableBoard class.
 */

#ifndef PLAYABLE_BOARD_H
#define PLAYABLE_BOARD_H

#include <memory>

namespace nigel {

class Move;

struct PlayableBoard {

    virtual void Clear() = 0;
    virtual void Play(std::shared_ptr<Move> move) = 0;

};

}

#endif
