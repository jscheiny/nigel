/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Board.h
 * @brief Declaration of the Board class.
 */

#ifndef BOARD_H
#define BOARD_H

#include "Dawg.h"
#include "Tile.h"
#include "Observe.h"
#include "Utility.h"
#include "Coordinate.h"
#include "MovesFinder.h"
#include "ReadableBoard.h"
#include "PlayableBoard.h"
#include "TransposedBoard.h"

#include <memory>
#include <iosfwd>
#include <string>
#include <vector>
#include <stack>
#include <set>
#include <map>

namespace nigel {

class Move;
class Lexicon;
class Alphabet;
class BoardDefinition;

/// A Scrabble board.
/**
 * This class holds a Scrabble board which consists of a board, the positions of
 * premium (multiplier) squares, the positions of tiles, and the location of the
 * start square. The board's responsibilities include accessing locations of
 * premium squares and tiles, and find all possible moves on the board given a
 * rack, lexicon, and alphabet. Board configurations (dimensions, premium
 * squares, and start location) can be read in from a board config file and this
 * can be done through the constructor. There are a set of built in configs
 * from which boards can be created using the Config enum. Board contents (the
 * tiles on the board) can be read in through various methods from board
 * contents files.
 */
class Board : public ReadableBoard, public PlayableBoard {

public:
    /// The set of built in board configurations.
    /**
     * Defines the set of built in board definitions that can be used to
     * construct an board.
     */
    enum class Config {
        /// Used to specify the Scrabble board configuration
        SCRABBLE,
        /// Used to specify the Super Scrabble board configuration
        SUPER_SCRABBLE,
        /// Used to specify the Words With Friends board configuration
        WWF
    };

    /// Construct an empty board with the given built in configuration.
    /**
     * By default a Scrabble board.
     * @param config the configuration used to define the board
     */
    Board(std::shared_ptr<Alphabet> alphabet,
          std::shared_ptr<Lexicon> lexicon,
          Config config = Config::SCRABBLE);

    /// Construct a board with the given configuration from a file path.
    /**
     * If the file at the path can not be opened throws an OpenError. If the
     * file contains invalid data, throws an InvalidDataError.
     * @param configPath the file path from which to read the board config
     */
    Board(std::shared_ptr<Alphabet> alphabet,
          std::shared_ptr<Lexicon> lexicon,
          const std::string& configPath);

    /// Construct a board with the given configuration from an input stream.
    /**
     * If the stream contains invalid data, throws an InvalidDataError.
     * @param configStream the stream from which to read the board config
     */
    Board(std::shared_ptr<Alphabet> alphabet,
          std::shared_ptr<Lexicon> lexicon,
          std::istream& configStream);

    Board(std::shared_ptr<Alphabet> alphabet,
          std::shared_ptr<Lexicon> lexicon,
          const BoardDefinition& definition);

    Board(const Board& other);
    Board(Board&& other);
    Board& operator= (const Board& other);
    Board& operator= (Board&& other);

    virtual ~Board() {}

    /// Read the contents of the board from a file at the given path.
    /**
     * The board contents are read from the file at the path and tile scores are
     * determined given the alphabet from the tile bag.
     * @param path the file path to read from
     * @param alphabet the alphabet used to determine tile scores
     */
    void ReadBoardContents(const std::string& path, std::shared_ptr<Alphabet> alphabet);

    /// Read the contents of the board from an input stream.
    /**
     * The board contents are read from the stream at the path and tile scores
     * are determined given the alphabet from the tile bag.
     * @param is the stream to read the board contents from
     * @param alphabet the alphabet used to determine tile scores
     */
    void ReadBoardContents(std::istream& is, std::shared_ptr<Alphabet> alphabet);

    /// Removes all of the tiles from the board.
    void Clear() override;

    /// Return whether the board has no tiles on it.
    bool IsEmpty() const override {
        return occupied_ == 0;
    }

    /// Returns the number of rows on the board.
    int GetRows() const override {
        return rows_;
    }

    /// Returns the number of columns on the board.
    int GetColumns() const override {
        return cols_;
    }

    /// Returns the row of the start position.
    int GetStartRow() const override {
        return start_row_;
    }

    /// Returns the column of the start position.
    int GetStartColumn() const override {
        return start_col_;
    }

    /// Returns the number of occupied squares
    int GetOccupied() const {
        return occupied_;
    }

    /// Gets the name of the board setup.
    std::string GetConfigName() const {
        return config_name_;
    }

    std::shared_ptr<Alphabet> GetAlphabet() const {
        return alphabet_;
    }

    std::shared_ptr<Lexicon> GetLexicon() const {
        return lexicon_;
    }

    /// Gets the file path associated with the given board configuration.
    static std::string GetConfigPath(Config config) {
        return kConfigPathMap.at(config);
    }

    /// Returns whether the given row, column position is a valid coordinate.
    bool OutOfBounds(int row, int col) const override {
        return row < 0 || row >= rows_ || col < 0 || col >= cols_;
    }

    bool OutOfBounds(const Coord& coord) const override {
        return OutOfBounds(coord.row, coord.col);
    }

    /// Returns the tile at the given row and column without bounds checking.
    const Tile& operator() (int row, int col) const override {
        return board_[row][col];
    }

    /// Returns the tile at the given coordinate without bounds checking.
    const Tile& operator() (const Coord& coord) const override {
        return board_[coord.row][coord.col];
    }

    /// Returns the tile at the given row and column with bounds checking.
    const Tile& TileAt(int row, int col) const override {
        return board_.at(row).at(col);
    }

    /// Returns the tile at the given coordinate with bounds checking.
    const Tile& TileAt(const Coord& coord) const override {
        return board_.at(coord.row).at(coord.col);
    }

    /// Returns the premium character for a given row and column without bounds checking.
    char Prem(int row, int col) const override {
        return premium_[row][col];
    }

    /// Returns the premium character for a given row and column with bounds checking.
    char PremiumAt(int row, int col) const override {
        return premium_.at(row).at(col);
    }

    /// Gets the associated moves finder of this board.
    const MovesFinder& GetMovesFinder() const {
        return *moves_finder_;
    }

    void FindMoves(const Rack& rack,
                   std::vector<std::shared_ptr<Move>>* moves) {
        moves_finder_->FindMoves(rack, moves);
    }

    /// Plays the given move on the board.
    void Play(std::shared_ptr<Move> move) override;

    /// Prints a colorized version of the board to the given output stream
    void PrintWithColor(std::ostream& os, int indent = 0) const;

    /// Gets the word multiplier for a given premium character
    static int WordMultiplier(char premium);

    /// Gets the letter multiplier for a given premium character
    static int LetterMultiplier(char premium);

    void PushTempMove(std::shared_ptr<Move> move);

    std::shared_ptr<Move> PopTempMove();

    void ClearTempMoves();

private:

    static const std::map<Config, std::string> kConfigPathMap;

    /// A string used to convert a premium character to a word multiplier.
    const static std::string kWordPremiums;
    /// A string used to convert a premium character to a letter multiplier.
    const static std::string kLetterPremiums;

    using TempMove = std::pair<std::shared_ptr<Move>, std::vector<Coord>>;

    int rows_;
    int cols_;

    int start_row_;
    int start_col_;

    std::string config_name_;

    std::vector<std::vector<char>> premium_;
    std::vector<std::vector<Tile>> board_;
    int occupied_;

    std::stack<TempMove> temp_moves_;

    std::shared_ptr<Alphabet> alphabet_;
    std::shared_ptr<Lexicon> lexicon_;
    std::unique_ptr<MovesFinder> moves_finder_;

    void ReadBoardSetup(const std::string& path);
    void ReadBoardSetup(std::istream& is);

    void FinishPremiumBoard();
    void CreateEmptyBoard();

    void Swap(Board& other);

    Tile& operator() (const Coord& coord) {
        return board_[coord.row][coord.col];
    }

    std::unique_ptr<MovesFinder> MakeMovesFinder();

};

}

#endif
