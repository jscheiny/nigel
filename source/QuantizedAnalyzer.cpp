/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file QuantizedAnalyzer.cpp
 * @brief Implementation of the QuantizedAnalyzer class.
 */

#include "QuantizedAnalyzer.h"

#include "Move.h"
#include "TileBag.h"

using namespace nigel;
using namespace analyze;
using namespace std;

struct QuantizedStatUpdater {
    using GamePhase = typename QuantizedAnalyzer::GamePhase;
    using UtilityMap = typename QuantizedAnalyzer::UtilityMap;

    QuantizedStatUpdater(UtilityMap& utilities,
                         GamePhase phase,
                         shared_ptr<Alphabet> alphabet,
                         shared_ptr<Move> move)
        : utilities_(utilities)
        , phase_(phase)
        , alphabet_(alphabet)
        , score_(move->GetScore()) {}

    template<typename Iterator>
    void operator() (Iterator begin, Iterator end) {
        std::string subset = Rack{begin, end}.ToBlanksString(alphabet_);
        if(!subset.empty()) {
            sort(subset.begin(), subset.end());
            utilities_[subset.size()][phase_][subset].Add(score_);
        }
    }

private:
    UtilityMap& utilities_;
    GamePhase phase_;
    shared_ptr<Alphabet> alphabet_;
    int score_;

};

void QuantizedAnalyzer::ObserveMove(Game* game, std::shared_ptr<Move> move) {
    if(move->GetPlaced().size() + move->GetLeave().size() == 7 && move->IsMoveOnBoard()) {
        GamePhase phase =
            game->GetBoard()->GetOccupied() * quantization_bins_ / 100;
        Rack placed = move->GetPlaced();
        placed.Sort();
        QuantizedStatUpdater updater(utilities_, phase, game->GetTileBag()->GetAlphabet(), move);
        ForEachSubset(placed.begin(), placed.end(), updater);
    }
}
