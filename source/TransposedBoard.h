/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file TransposedBoard.h
 * @brief Declaration and implementation of the TransposedBoard class.
 */

#ifndef TRANSPOSED_BOARD_H
#define TRANSPOSED_BOARD_H

#include "ReadableBoard.h"

#include <iostream>

namespace nigel {

class TransposedBoard : public ReadableBoard {

public:
    TransposedBoard(const ReadableBoard& board) : board_(board) {}

    bool IsEmpty() const override {
        return board_.IsEmpty();
    }

    int GetRows() const override {
        return board_.GetColumns();
    }

    int GetColumns() const override {
        return board_.GetRows();
    }

    int GetStartRow() const override {
        return board_.GetStartColumn();
    }

    int GetStartColumn() const override {
        return board_.GetStartRow();
    }

    bool OutOfBounds(int row, int col) const override {
        return board_.OutOfBounds(col, row);
    }

    bool OutOfBounds(const Coord& coord) const override {
        return board_.OutOfBounds(coord.col, coord.row);
    }

    const Tile& operator() (int row, int col) const override {
        return board_(col, row);
    }

    const Tile& operator() (const Coord& coord) const override {
        return board_(coord.col, coord.row);
    }

    const Tile& TileAt(int row, int col) const override {
        return board_.TileAt(col, row);
    }

    const Tile& TileAt(const Coord& coord) const override {
        return board_.TileAt(coord.col, coord.row);
    }

    char Prem(int row, int col) const override {
        return board_.Prem(col, row);
    }

    char PremiumAt(int row, int col) const override {
        return board_.PremiumAt(col, row);
    }

private:
    const ReadableBoard& board_;

};

}

#endif
