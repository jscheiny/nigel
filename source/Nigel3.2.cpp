/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Nigel3.2.h"
#include "Lexicon.h"
#include "Dawg.h"
#include "Player.h"

using namespace nigel;
using namespace std;

const LinearFunction Nigel<3,2>::kHasAnagramFn(1.1326, 11.4853);
const LinearFunction Nigel<3,2>::kNoAnagramFn(1.3550, -13.6193);

Nigel<3,2>::Nigel(int sampleSize) : Nigel<3,1>(sampleSize) {
    SetupAnagrams();
}

double Nigel<3,2>::SampleUtility(const string& sample) {
    double util = SubsetsUtility(sample);
    if(HasAnagram(sample)) {
        return kHasAnagramFn(util);
    } else {
        return kNoAnagramFn(util);
    }
}

void Nigel<3,2>::SetupAnagrams() {
    GetPlayer()->GetLexicon()->GetDawg(7)->EachWord([this] (string word) {
        sort(word.begin(), word.end());
        anagrams_.insert(word);
    });
}

bool Nigel<3,2>::HasAnagram(const string& letters) {
    char blankChar = GetPlayer()->GetTileBag()->GetAlphabet()->GetBlankCharacter();
    string processed;
    for(char let : letters) {
        if(let != blankChar)
            processed += let;
    }
    return anagrams_.count(processed) != 0;
}
