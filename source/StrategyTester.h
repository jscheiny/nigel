/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file StrategyTester.h
 * @brief Declaration of the StrategyTester class.
 */

#ifndef STRATEGY_TESTER_H
#define STRATEGY_TESTER_H

#include "Game.h"
#include "Board.h"
#include "Alphabet.h"
#include "Lexicon.h"
#include "Player.h"

#include <iostream>

namespace nigel {

class TileBag;

/// A framework for analyzing two strategies playing against each other.
/**
 * The idea of a strategy tester is to create a game with two different
 * strategies, and have them play against each other for a number of match
 * rounds. A match round is two games of Scrabble where each strategy plays
 * as first player once. The tester then collects the statistics for these
 * games (wins, average player / strategy score), and makes them accessible
 * through various getter methods, and one can get a concise report via the
 * printReport method.
 *
 */
class StrategyTester {

public:
    /// Used to refer to the two strategies being tested.
    /**
     * Any method that requires access or modification of a strategy or its
     * its statistics, will require that you pass in a member of this enum
     * to indicate which one you are referring to.
     */
    enum ST_Strategy { S1, S2 };

    /// Used to refer to the two different playing positions of the strategies.
    /**
     * Any method that requires access or modification of a strategy playing in
     * a certain position (player 1 or 2) will require that you pass in a member
     * of this enum to indicate which player position you are referring to.
     */
    enum ST_Player { P1, P2 };

    /// Create a strategy tester that tests on a given game configuration.
    /**
     * The configuration of the game is by default English Scrabble.
     * @param config the configuration of the game used for testing
     */
    explicit StrategyTester(Game::Config config = Game::Config::SCRABBLE);

    /// Create a strategy tester from the individual component configurations.
    /**
     * @param board_config the configuration for the board
     * @param alphabet_config the configuration for the alphabet
     * @param lexicon_config the configuration for the lexicon
     */
    StrategyTester(Board::Config board_config,
                   Alphabet::Config alphabet_config,
                   Lexicon::Config lexicon_config);

    /// Create a strategy tester from the individual component setup paths.
    /**
     * @param board the setup path for the board
     * @param alphabet the setup path for the alphabet
     * @param lexicon the setup path for the lexicon
     */
    StrategyTester(const std::string& board,
                   const std::string& tilebag,
                   const std::string& lexicon);

    ~StrategyTester() = default;

    // Deleted copy/move construction/assignment

    StrategyTester(const StrategyTester&) = delete;
    StrategyTester(StrategyTester&) = delete;
    StrategyTester& operator= (const StrategyTester&) = delete;
    StrategyTester& operator= (StrategyTester&) = delete;

    template<typename Strat>
    StrategyTester& SetStrategy(ST_Strategy s, const std::string& name);

    template<typename Strat, typename... Args>
    StrategyTester& SetStrategy(ST_Strategy s, const std::string& name,
                                Args&&... args);

    int StratWins(ST_Strategy s) {
        return info_[s].p1_wins + info_[s].p2_wins;
    }

    int StratPlayerWins(ST_Strategy s, ST_Player p) const;

    double StratAverageScore(ST_Strategy s) {
        return double(info_[s].p1_score + info_[s].p2_score) / games_;
    }

    double StratPlayerAverageScore(ST_Strategy s, ST_Player p) const;

    int GetTies() const {
        return strategy1_->p1_ties + strategy2_->p1_ties;
    }

    int GetGames() const {
        return games_;
    }

    void ResetStats();

    void Run(int matches, bool print_messages = true);

    void PrintReport(std::ostream& os);

private:
    struct StrategyInfo {
        StrategyInfo(Player* player_ = nullptr);

        void Reset();

        Player* player;
        int p1_wins;
        int p2_wins;
        int p1_ties;

        int p1_score;
        int p2_score;
    };

    void Init();

    void Run(int matches, bool print_counter,
             int* strat1_win_counter, int* strat2_win_counter,
             int* ties_counter, int* p1_score_counter, int* p2_score_counter);

    Game game_;

    StrategyInfo info_[2];
    StrategyInfo* strategy1_;
    StrategyInfo* strategy2_;

    int games_;

};

template<typename Strat>
StrategyTester& StrategyTester::SetStrategy(ST_Strategy s,
                                            const std::string& name) {
    Player* player = info_[s].player;
    player->SetName(name);
    player->SetStrategy<Strat>();
    return *this;
}

template<typename Strat, typename... Args>
StrategyTester& StrategyTester::SetStrategy(ST_Strategy s,
                                            const std::string& name,
                                            Args&&... args) {
    Player* player = info_[s].player;
    player->SetName(name);
    player->SetStrategy<Strat>(std::forward<Args>(args)...);
    return *this;
}

}

#endif
