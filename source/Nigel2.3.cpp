/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Nigel2.3.h"
#include "Player.h"
#include "Board.h"
#include "Rack.h"

using namespace std;
using namespace nigel;

void Nigel<2,3>::Preprocess(vector<shared_ptr<Move>>& moves) {
    const Board& board = *GetPlayer()->GetBoard();
    game_state_ = board.GetOccupied() * 7 / 100;
}

double Nigel<2,3>::Utility(Rack& leave) {
    if(GetPlayer()->GetTileBag()->IsEmpty())
        return -2 * leave.Score();

    if(leave.empty())
        return Nigel22Resources::kAveragePlayScore;

    if(leave.size() >= 7)
        return 0.0;

    string leave_str = leave.ToString();
    if(leave.size() <= 3)
        return Nigel23Resources::Instance().GetUtility({leave_str, game_state_});

    return Nigel22Resources::Instance().ComputeNLetterUtil(leave_str);
}

Nigel23Resources::Nigel23Resources() {
    ReadQuantized(NIGEL_RESOURCE("quantized/1.txt"));
    ReadQuantized(NIGEL_RESOURCE("quantized/2.txt"));
    ReadQuantized(NIGEL_RESOURCE("quantized/3.txt"));
}

void Nigel23Resources::ReadQuantized(const string& path) {
    string line;
    ifstream is(path);
    while(getline(is, line)) {
        vector<string> parts = Split(line);
        string entry = parts[0];
        int game_state = atoi(parts[1].c_str());
        double utility = atof(parts[2].c_str());

        quantized_map_.emplace(QKey(entry, game_state), utility);
    }
}
