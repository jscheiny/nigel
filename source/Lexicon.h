/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Lexicon.h
 * @brief Declaration of the Lexicon class.
 */

#ifndef LEXICON_H
#define LEXICON_H

#include "Dawg.h"
#include "WeakCache.h"

#include <unordered_set>
#include <string>
#include <set>
#include <map>

namespace nigel {

/// A lexicon for use in computing the set of possible plays.
/**
 * A lexicon represents the set of words in two different ways. The first is
 * a hashed set of words used for constant time look up of whether a word is or
 * is not in the lexicon. The second is a set of dawgs, one for each of the
 * lengths of words in the lexicon. This allows for quick access to prefix
 * information.
 *
 * Lexicons can be defined from a set of built in Nigel lexicons given by the
 * Config enumeration, and passed to the appropriate constructor.
 */
class Lexicon {

public:
    /// The set of built in lexicon configurations.
    enum class Config {
        /// The Collins Scrabble Words 2012 edition lexicon.
        CSW12,
        /// The ODS version 4 lexicon (French).
        ODS4,
        /// The ODS version 5 lexicon (French).
        ODS5,
        /// The Official Scrabble Player's Dictionary version 4.
        OSPD4,
        /// The OSWI lexicon.
        OSWI,
        /// The SOWPODS lexicon.
        SOWPODS,
        /// The Tournament Word List 2006 (a.k.a OCTWL2).
        TWL06,
        /// The Tournament Word List 1998 (a.k.a OCTWL).
        TWL98,
        /// The Words With Friends lexicon.
        WWF,
        /// The Zingarelli 2005 edition lexicon (Italian).
        ZINGA05
    };

    static std::shared_ptr<Lexicon> Create(Config config);

    /// Create a lexicon from one of the built in configuration files.
    /**
     * By default a TWL06 lexicon.
     * @param config the configuration used to define the lexicon
     */
    explicit Lexicon(Config config = Config::TWL06);

    /// Create a lexicon from the file or nigel lexicon at the path.
    /**
     * If the file at the path is a plain lexicon and cannot be opened,
     * throws an OpenError. If the lexicon file / Nigel lexicon folder
     * contains invalid data an InvalidDataError is thrown.
     */
    explicit Lexicon(const std::string& path);

    /// Destructor.
    ~Lexicon();

    // Deleted copy and move construction and assignment.

    Lexicon(const Lexicon&) = delete;
    Lexicon(Lexicon&&) = delete;
    Lexicon& operator= (const Lexicon&) = delete;
    Lexicon& operator= (Lexicon&&) = delete;

    /// Returns whether the given word is in the lexicon.
    /**
     * This runs in constant time.
     * @param word the word to check if in the lexicon
     * @return true if the word is in the lexicon, false otherwise
     */
    bool Contains(const std::string & word) const;

    /// Returns the root of the Dawg containing strings of a given word length.
    /**
     * This is equivalent to lexicon.dawg(wl)->getRoot().
     * @param wl the length of the words in the Dawg
     * @return the root node of the appropriate Dawg.
     */
    const Dawg::Node* GetRoot(int word_length) const;

    /// Returns the Dawg containing strings of a given word length.
    /**
     * The returned Dawg will contain only words with the given word length,
     * will contain all words in the lexicon of that length.
     * @parm wl the length of the words in the Dawg
     * @return the Dawg containing words of the given length
     */
    const Dawg* GetDawg(int word_length) const;

    /// Returns the number of words in the lexicon.
    int size() const {
        return word_set_.size();
    }

    static std::string GetConfigPath(Config config) {
        return kConfigPathMap.at(config);
    }

    int GetMinWordLength() const { return min_word_length_; }
    int GetMaxWordLength() const { return max_word_length_; }

private:
    /// Mapping from the config enum to paths of the configuration files.
    static const std::map<Config, std::string> kConfigPathMap;

    static WeakCache<Config, Lexicon> lexiconCache;

    /// The set of words in the lexicon.
    std::unordered_set<std::string> word_set_;
    /// The set of dawgs composing the lexicon by word length.
    std::map<int, Dawg*> dawgs_;

    int min_word_length_;
    int max_word_length_;

    /// Reads a lexicon from the given path.
    void ReadDict(const std::string& path);

    /// Reads a .nigeldict from the given path.
    void ReadNigelDict(const std::string& path);

    /// Reads a plain text file lexicon from the given path.
    void ReadPlainDict(const std::string& path);

    /// Tests if a path represents a Nigel lexicon.
    bool IsNigelDictPath(const std::string & dirPath);

    /// Tests if the path contains the files to be a valid Nigel lexicon.
    bool IsValidNigelDict(const std::string & dirPath);
};

}

#endif
