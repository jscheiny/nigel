/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Dawg.h
 * @brief Declaration of the Dawg class.
 */

#ifndef DAWG_H
#define DAWG_H

#include <string>
#include <vector>
#include <set>
#include <unordered_set>

namespace nigel {

// A directed acylcic word graph.
/**
 * The Dawg class represents a set of strings over a fixed alphabet and
 * provides fast access for the set of words with a given prefix. A Dawg can
 * store its data internally as a fully minimized dawg, or as a fully expanded
 * prefix retrieval tree (trie). A minimized dawg can be constructed using the
 * MinDawg class, saved to a file and then read in via the appropriate Dawg
 * constructor. Ideally, a given word set should only be turned into a
 * minimized dawg once. Adding words to a dawg via the insert method will
 * violate the minimization of the dawg, and result in at worst a trie. Words
 * cannot be quickly and safely removed from a dawg, so that functionality
 * is not provided.
 *
 * Since there are a great many possible uses of a Dawg and cannot anticipate
 * all of them, we provide a few of the most useful functions. These are
 * inserting words, checking for containment, getting the node of a prefix,
 * and iterating over all words in the dawg. For any more specialized usage,
 * use the getRoot() method to access the root node of the Dawg.
 */
class Dawg {

public:
    /// The character of a node that represents the end of a word.
    static const char kEow;

    /// Forward declaration of node class.
    class Node;

    friend Node;

    /// Create a new, empty Dawg.
    Dawg();

    /// Copy constructor.
    /**
     * This does not copy the Dawg's internal structure. No matter whether the
     * other Dawg is minimized, this will create a fully expanded trie. The
     * behavior will be the same, but the size will be greater than or equal to
     * that of the copied Dawg.
     * @param other the Dawg to copy
     */
    Dawg(const Dawg& other);

    /// Move constructor.
    /**
     * @param other the Dawg to move into this one
     */
    Dawg(Dawg&& other);

    /// Construct a Dawg from a stream of a .dawg file.
    /**
     * If there is invalid data in the stream, throws an InvalidDataError.
     * @param is the stream from which to read the dawg
     */
    explicit Dawg(std::istream& is);

    /// Construct a Dawg from a .dawg file at the given path.
    /**
     * If the file at the path could not be opened, throws an OpenError. If
     * there is invalid data in the file, throw an InvalidDataError.
     * @param path
     */
    explicit Dawg(const std::string& path);

    /// Construct a Dawg from a range defined by a pair of iterators.
    /**
     * The iterators should dereference as strings.
     * @param begin the iterator at the start of the range
     * @param end the iterator one past the end of the range
     */
    template<typename InputIterator>
    Dawg(InputIterator begin, InputIterator end);

    /// Destructor.
    ~Dawg();

    /// Copy assignment.
    /**
     * See the copy constructor on the internal structure of the dawg, the same
     * applies to this.
     * @param other the Dawg to copy assign from
     * @return this Dawg
     */
    Dawg& operator= (const Dawg& other);

    /// Move assignment.
    /**
     * @param other the  Dawg to move assign from
     * @return this Dawg
     */
    Dawg& operator= (Dawg&& other);

    /// Returns the number of words stored in the Dawg.
    int GetWords() const {
        return words_;
    }

    /// Returns the number of nodes in the Dawg.
    int GetNodes() const {
        return nodes_;
    }

    /// Returns the root node of this Dawg.
    const Node* GetRoot() const {
        return root_;
    }

    /// Removes all words from the Dawg.
    void Clear();

    /// Insert a word into the dawg.
    /**
     * Words are not inserted in a minimized fashion. That is, if insert is the
     * only method of adding words to the Dawg, the internal structure will be
     * that of a trie, rather than a Dawg. The complexity of this method is
     * linear on the length of the word only, O(|word|).
     * @param word the word to insert into the Dawg
     * @return true if the word was already in the Dawg, false otherwise
     */
    bool Insert(const std::string& word);

    /// Returns whether a dawg contains a specific node.
    /**
     * The complexity of this method is linear on the length of the word only,
     * O(|word|).
     * @param word the word to check if in the Dawg
     * @return true if the given word is in the dawg, false otherwise
     */
    bool Contains(const std::string& word) const;

    /// Returns the node which represents the sub-dawg with the given prefix
    /**
     * The complexity of this method is linear on the length of the word only,
     * O(|prefix|).
     * @param prefix the prefix to search for
     * @return the node of the sub-dawg for the given prefix, or null if the
     * prefix is not contained in the dawg.
     */
    const Node* Prefix(const std::string& prefix) const;

    /// Calls a function on each word in the dawg alphabetically.
    /**
     * Iterates through the entire dawg in alphabetical order calling the
     * function on each word. The function should take a single string as an
     * argument, the return value is ignored. This is equivalent to calling
     * dawg.getRoot()->eachSuffix(fn).
     */
    template<typename Function>
    void EachWord(Function fn) const;

    /// Swaps this dawg with another Dawg.
    void Swap(Dawg& other);

private:
    /// The number of words in the Dawg.
    int words_;
    /// The number of nodes used to store the Dawg.
    int nodes_;
    /// The root node of the Dawg.
    Node* root_;
    /// The single end-of-word node for this Dawg.
    Node* eow_;

    /// Read the dawg from a given input stream.
    /**
     * May throw an InvalidDataError.
     * @param is the stream to read the Dawg from
     */
    void ReadFromFile(std::istream& is);

    /// Read the number of nodes in dawg from the first line of the input.
    int ReadNumNodes(std::istream& fin) const;

    /// Read and process a given line of input in a dawg file.
    /**
     * @param index the line index (or ID of the node represented by the line)
     * @param fin the file input stream
     * @param nodes the vector of all nodes in the dawg
     * @param edges the set of all edges that will be used to connect the Dawg
     * @param eowNode the end of word node for the dawg
     * @param numNodes the guaranteed number of nodes in the dawg
     */
    bool ReadNodeLine(int index,
                      std::istream& fin,
                      std::vector<Node*>& nodes,
                      std::vector<std::pair<int,int>>& edges,
                      Dawg::Node* eow_node,
                      int num_nodes);

    /// Count the number of words in the dawg.
    /**
     * Used to get a word count when reading in from a .dawg file.
     */
    void CountWords();

    /// Count the number of words in the Dawg at the given root.
    /**
     * @param root the dawg in which to count the number of words
     * @param wordCount the word counter which will be incremented for each
     * word found.
     */
    static void CountWords(Dawg::Node* root, int* words);

};

/// A node in a DAWG.
/**
 * Most dawg nodes in a fully expanded DAWG (except for the end of word
 * node) represent a single unique prefix. However, a node in a minimized
 * DAWG may represent any number of prefixes ending in the same letter,
 * depdending on the path taken to reach the node. Nodes can have up to 27
 * children, one for each of the letters 'A' through 'Z' and the end of word
 * letter (Dawg::EOW). Any method that takes a character should be passed
 * only one of those values. Nodes are unique and hence cannot be copied or
 * moved by construction or assignment. Only Dawgs may construct their own
 * nodes.
 */
class Dawg::Node {
public:
    // Deleted copy and move construction and assignment.
    Node(const Node&) = delete;
    Node(Node&&) = delete;
    Node& operator=(const Node&) = delete;
    Node& operator=(Node&&) = delete;

    /// Returns the letter that this node represents.
    char GetLetter() const {
        return letter_;
    }

    /// Puts the given node as a child of this node.
    void PutChild(Node* child) {
        children_[Index(child->letter_)] = child;
    }

    /// Returns whether this node has the given child
    /**
     * This is equivalent to checking node_ptr->getChild(let) != nullptr.
     * @param let the letter to check for the child
     * @return true if there is such a child of this node
     */
    bool HasChild(char letter) const {
        return children_[Index(letter)] != nullptr;
    }

    /// Returns the appropriate child of this node.
    /**
     * If this node has no child with that letter, returns a null pointer.
     * @param let the letter value of the child
     * @return the child with the given letter, or null if no such child
     */
    const Node* GetChild(char letter) const {
        return children_[Index(letter)];
    }

    /// Returns the appropriate child of this node.
    /**
     * If this node has no child with that letter, returns a null pointer.
     * @param let the letter value of the child
     * @return the child with the given letter, or null if no such child
     */
    Node* GetChild(char letter) {
        return children_[Index(letter)];
    }

    /// Returns a string with the letters of all the children of this node.
    /**
     * This does not include the end-of-word character if this node has an
     * end-of-word child. The return string is guaranteed to be in
     * alphabetical order.
     */
    std::string GetChildren() const;

    /// Calls a function on all of the suffixes reachable from this node.
    /**
     * The function should take as an argument a single string, its return
     * value is ignored. The strings passed to the function will be the
     * suffixes in the sub-trie represented by this node and will not
     * include this node's character at the beginning. The function will be
     * called in alphabetical order of the suffixes.
     * @param fn the function to call on each suffix
     */
    template<typename Function>
    void EachSuffix(Function fn) const;

    friend Dawg;

private:
    /// The letter of this node.
    char letter_;
    /// The children of this node.
    Node* children_[27];

    /// Create a new node of the given dawg with the given letter.
    /**
     * @param let the letter represented by the node
     * @param dawg the dawg creating this node
     */
    Node(char letter, Dawg& dawg) : letter_(letter), children_() {
        dawg.nodes_++;
    }

    /// Recursive helper function for eachSuffix().
    /**
     * @param root the current suffix root
     * @param prefix the prefix of the suffix seen so far
     * @param fn the function to call on complete suffixes
     */
    template<typename Function>
    static void EachSuffix(const Node* root, const std::string& prefix,
                           Function& fn);

    /// Return the index in the children array of the given letter.
    static int Index(char let) {
        return let - 'A';
    }
};


// Template function definitions

template<typename InputIterator>
Dawg::Dawg(InputIterator begin, InputIterator end) : Dawg() {
    for(auto itr = begin; itr != end; ++itr) {
        insert(*itr);
    }
}

template<typename Function>
void Dawg::EachWord(Function fn) const {
    root_->EachSuffix(fn);
}

template<typename Function>
void Dawg::Node::EachSuffix(Function fn) const {
    EachSuffix(this, {letter_, '\0'}, fn);
}

template<typename Function>
void Dawg::Node::EachSuffix(const Node* root, const std::string& prefix,
                            Function& fn) {
    if(root->HasChild(kEow)) {
        fn(prefix);
    }
    for(char letter = 'A'; letter <= 'Z'; letter++) {
        if(root->HasChild(letter)) {
            EachSuffix(root->GetChild(letter), prefix + letter, fn);
        }
    }
}

}

#endif
