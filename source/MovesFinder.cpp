/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file MovesFinder.cpp
 * @brief Implementation of the MovesFinder class.
 */

#include "MovesFinder.h"

#include "Rack.h"
#include "Move.h"
#include "Board.h"
#include "Alphabet.h"
#include "Coordinate.h"
#include "Lexicon.h"

#include <functional>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <memory>

using namespace std;
using namespace nigel;

using WordScore = tuple<string, int>;

Coord Next(const Coord& curr);
bool EnoughEmptySquares(int empty_squares, int rack_size);
bool ConnectsToBoard(int word_length, int empty_squares, bool adjacent);
WordScore VertSequence(const ReadableBoard& board, int row, int col, int direc);

Directional::Directional(const ReadableBoard& board,
                         shared_ptr<Alphabet> alphabet,
                         shared_ptr<Lexicon> lexicon,
                         bool horiz)
      : horiz_(horiz)
      , board_(board)
      , alphabet_(alphabet)
      , lexicon_(lexicon)
      , moves_(nullptr)
      , effected_rows_(board_.GetRows(), false)
      , prev_rack_size_(-1) {
    SetupCrossWordData();
}

void Directional::FindMoves(Rack rack, vector<shared_ptr<Move>>* moves) const {
    moves_ = moves;
    max_playable_word_ = min(lexicon_->GetMaxWordLength(), board_.GetColumns());

    PrepareForMove(rack.size());
    rack.Sort();

    for(int word_length = lexicon_->GetMinWordLength();
            word_length <= max_playable_word_; word_length++) {
        for(const auto& entry : incr_start_pos_[word_length]) {
            for(const Coord& start : entry.second) {
                MovesAt(start, start, lexicon_->GetRoot(word_length),
                        rack, Rack(), Rack(), 0, 0, 1, word_length, "");
            }
        }
    }
}

void Directional::MovesAt(const Coord& start,
                          const Coord& curr,
                          const Dawg::Node* root,
                          const Rack& leave,
                          const Rack& placed,
                          const Rack& tiles_on_board,
                          int score,
                          int perp_score,
                          int word_multiplier,
                          int word_length,
                          const string& prefix) const {
    // If the word length has become empty, we've probably reached a complete play.
    if(word_length == 0) {
        if(!root->HasChild(Dawg::kEow))
            return;
        score = (score * word_multiplier) + perp_score +
                (placed.size() == 7 ? 50 : 0);
        moves_->push_back(
            make_shared<Move>(prefix, leave, placed, tiles_on_board,
                              CorrectCoord(start), score));
        return;
    }

    // If there is a tile on the board, not much to do, just move to the next
    // space if the letter is the child of the current dawg node.
    const Tile& onboard = board_(curr);
    if(!onboard.IsEmpty()) {
        if(!root->HasChild(onboard.GetLetter()))
            return;

        Rack recur_tiles_on_board(tiles_on_board);
        recur_tiles_on_board.push_back(onboard);
        score += onboard.GetScore();
        MovesAt(start, curr.Right(), root->GetChild(onboard.GetLetter()),
                Rack(leave), Rack(placed), recur_tiles_on_board,
                score, perp_score, word_multiplier, word_length - 1,
                prefix + onboard.GetLetter());
        return;
    }

    const CrossWordData& cwd = crosswords_[curr.row][curr.col];
    // If there are perpendicular tiles but no letter can be played attached to
    // them, no word can be played, stop.
    if(cwd.PerpWordExists() && !cwd.AnyTilesValid())
        return;
    // Get the set of cross word letters. If no perpendicular tiles, this is the
    // set of all possible letters.
    const set<char>& cross_word_letters = cwd.PerpWordExists() ?
        cwd.GetValidLetters() : alphabet_->GetLetters();

    string dawg_next_letters = root->GetChildren();
    // If the dawg node has no children, no word can be played, stop.
    if(dawg_next_letters.size() == 0)
        return;

    // Compute the intersection of the dawg node child letters and the cross
    // word data valid letters.
    vector<char> possible_letters;
    set_intersection(cross_word_letters.begin(), cross_word_letters.end(),
                     dawg_next_letters.begin(), dawg_next_letters.end(),
                     back_inserter(possible_letters));
    // If the intersection is empty, no letters at all can be palyed, stop.
    if(possible_letters.empty())
        return;

    // Compute the premium values.
    char premium = board_.Prem(curr.row, curr.col);
    int curr_word_mult = Board::WordMultiplier(premium);
    int curr_letter_mult = Board::LetterMultiplier(premium);
    word_multiplier *= curr_word_mult;

    auto blank_loc = find_if(leave.begin(), leave.end(), mem_fn(&Tile::IsBlank));
    if(blank_loc != leave.end()) {
        for(char letter : possible_letters) {
            Tile next(letter, 0);
            MovesAtRecur(start, curr, root, leave, placed, tiles_on_board,
                         score, perp_score, word_multiplier, word_length,
                         prefix, next, cwd, curr_word_mult, curr_letter_mult);
        }
    }

    // Compute the intersection of the rack letters and the already computed
    // possible letters (intersection of dawg children and cross word data
    // letters).
    string rack_letters = leave.ToString();
    vector<char> rack_possible_letters;
    set_intersection(possible_letters.begin(), possible_letters.end(),
                     rack_letters.begin(), rack_letters.end(),
                     back_inserter(rack_possible_letters));
    // If there are no letters that can be played form the rack, stop.
    if(rack_possible_letters.size() == 0)
        return;

    // For each letter that can be played, recur playing that tile.
    for(char letter : rack_possible_letters) {
        Tile next = alphabet_->CreateTile(letter);
        MovesAtRecur(start, curr, root, leave, placed, tiles_on_board,
                     score, perp_score, word_multiplier, word_length,
                     prefix, next, cwd, curr_word_mult, curr_letter_mult);
    }
}

void Directional::MovesAtRecur(const Coord& start,
                               const Coord& curr,
                               const Dawg::Node* root,
                               Rack leave,
                               Rack placed,
                               Rack tiles_on_board,
                               int score,
                               int perp_score,
                               int word_multiplier,
                               int word_length,
                               const string& prefix,
                               const Tile& next_played,
                               const CrossWordData& cwd,
                               int curr_word_mult,
                               int curr_letter_mult) const {
    if(next_played.IsBlank()) {
        leave.erase(find_if(leave.begin(), leave.end(), mem_fn(&Tile::IsBlank)));
    } else {
        leave.erase(find(leave.begin(), leave.end(), next_played));
    }

    int letter_score = next_played.GetScore() * curr_letter_mult;
    int perp_increment = 0;
    if(cwd.PerpWordExists())
        perp_increment = (cwd.GetPerpLettersScore() + letter_score) * curr_word_mult;

    placed.push_back(next_played);
    tiles_on_board.push_back(next_played);

    MovesAt(start, curr.Right(), root->GetChild(next_played.GetLetter()),
            leave, placed, tiles_on_board,
            score + letter_score, perp_score + perp_increment, word_multiplier,
            word_length - 1, prefix + next_played.GetLetter());
}

MoveCoord Directional::CorrectCoord(const Coord& curr) const {
    if(horiz_) {
        return MoveCoord(curr.row, curr.col, true);
    } else {
        return MoveCoord(curr.col, curr.row, false);
    }
}

Coord Next(const Coord& curr) {
    return Coord(curr.row, curr.col + 1);
}

void Directional::SetupCrossWordData() {
    for(int r = 0; r < board_.GetRows(); r++) {
        vector<CrossWordData> row(board_.GetColumns(), CrossWordData());
        crosswords_.push_back(row);
    }
}

void Directional::ClearCrossWordData() const {
    for(auto& row : crosswords_) {
        for(auto& col : row) {
            col.Clear();
        }
    }
}

void Directional::UpdateMetaBoard(const vector<Coord>& coords) {
    if(!coords.empty()) {
        GenerateEffectedRows(coords);
        UpdateCrossWordData(coords);
    }
}

bool Directional::IsHorizontalMove(const vector<Coord>& coords) {
    return coords.size() == 1 || (coords[0].row == coords[1].row);
}

void Directional::GenerateEffectedRows(const vector<Coord>& coords) {
    fill(effected_rows_.begin(), effected_rows_.end(), false);
    if(IsHorizontalMove(coords)) {
        for(const Coord& c : coords) {
            const auto& cwd = crosswords_[c.row][c.col];
            MarkEffectedRow(c.row - cwd.GetPreWord().size() - 1);
            MarkEffectedRow(c.row + cwd.GetPostWord().size() + 1);
        }
        effected_rows_[coords.front().row] = true;
    } else {
        for(const Coord& c : coords) {
            MarkEffectedRow(c.row);
        }
        Coord top = coords.front();
        const auto& top_cwd = crosswords_[top.row][top.col];
        MarkEffectedRow(top.row - top_cwd.GetPreWord().size() - 1);
        Coord bottom = coords.back();
        const auto& bottom_cwd = crosswords_[bottom.row][bottom.col];
        MarkEffectedRow(bottom.row + bottom_cwd.GetPostWord().size() + 1);
    }
}

void Directional::MarkEffectedRow(int row) {
    if(row >= 0 && row < board_.GetRows()) {
        effected_rows_[row] = true;
    }
}

void Directional::UpdateCrossWordData(const vector<Coord>& coords) {
    const set<char>& letters = alphabet_->GetLetters();
    if(IsHorizontalMove(coords)) {
        for(const Coord& c : coords) {
            CrossWordData cwd = crosswords_[c.row][c.col];
            const string& pre_word = cwd.GetPreWord();
            const string& post_word = cwd.GetPostWord();
            const Tile& tile = board_(c);
            string perp_word = pre_word + tile.GetLetter() + post_word;
            int perp_score = cwd.GetPreScore() + tile.GetScore() + cwd.GetPostScore();

            Coord above = c.Up(pre_word.size() + 1);
            if(!board_.OutOfBounds(above)) {
                crosswords_[above.row][above.col].UpdatePostWordScore(
                    perp_word, perp_score, lexicon_, letters);
            }
            Coord below = c.Down(post_word.size() + 1);
            if(!board_.OutOfBounds(below)) {
                crosswords_[below.row][below.col].UpdatePreWordScore(
                    perp_word, perp_score, lexicon_, letters);
            }
        }
    } else {
        Coord top = coords.front();
        Coord bottom = coords.back();
        const auto& top_cwd = crosswords_[top.row][top.col];
        const auto& bottom_cwd = crosswords_[bottom.row][bottom.col];
        string middle_word;
        int middle_score = 0;
        Coord end = bottom.Down(1);
        for(Coord m = top; m != end; m = m.Down()) {
            const auto& tile = board_(m);
            middle_word += tile.GetLetter();
            middle_score += tile.GetScore();
        }
        string word = top_cwd.GetPreWord() + middle_word + bottom_cwd.GetPostWord();
        int score = top_cwd.GetPreScore() + middle_score + bottom_cwd.GetPostScore();

        Coord above = top.Up(top_cwd.GetPreWord().size() + 1);
        if(!board_.OutOfBounds(above)) {
            crosswords_[above.row][above.col].UpdatePostWordScore(
                    word, score, lexicon_, letters);
        }
        Coord below = bottom.Down(bottom_cwd.GetPostWord().size() + 1);
        if(!board_.OutOfBounds(below)) {
            crosswords_[below.row][below.col].UpdatePreWordScore(
                    word, score, lexicon_, letters);
        }
    }
    for(const Coord& c : coords) {
        crosswords_[c.row][c.col].Clear();
    }
}

void Directional::GenerateUpdatedStartCoords(int rack_size) const {
    bool empty = board_.IsEmpty();
    for(int word_length = lexicon_->GetMinWordLength();
            word_length <= max_playable_word_; word_length++) {
        if(empty) {
            GenerateInitialStartCoords(word_length, rack_size);
        } else if(prev_rack_size_ == rack_size) {
            for(int row = 0; row < board_.GetRows(); row++) {
                if(effected_rows_[row]) {
                    GenerateRowStartCoords(row, word_length, rack_size);
                }
            }
        } else {
            for(int row = 0; row < board_.GetRows(); row++) {
                GenerateRowStartCoords(row, word_length, rack_size);
            }
        }
    }
    prev_rack_size_ = rack_size;
}

void Directional::GenerateInitialStartCoords(int word_length,
                                             int rack_size) const {
    WordLengthStarts& starts = incr_start_pos_[word_length];
    starts.clear();
    if(word_length > rack_size)
        return;
    vector<Coord>& startRow = starts[board_.GetStartRow()];
    for(int col = board_.GetStartColumn() - word_length + 1;
            col <= board_.GetStartColumn(); col++) {
        startRow.push_back(Coord(board_.GetStartRow(), col));
    }
}

void Directional::GenerateRowStartCoords(int row,
                                         int word_length,
                                         int rack_size) const {
    vector<Coord>& starts = incr_start_pos_[word_length][row];
    starts.clear();
    for(int col = 0; col <= board_.GetColumns() - word_length; col++) {
        if(IsLeftSquareEmpty(row, col) &&
           IsRightSquareEmpty(row, col, word_length)) {
            bool perp_words_valid = true;
            bool adjacent = false; // True if there occupied squares above/below the area.

            int empty_squares = CountEmptySquares(row, col, word_length,
                    &perp_words_valid, &adjacent);
            if(perp_words_valid && EnoughEmptySquares(empty_squares, rack_size) &&
               ConnectsToBoard(word_length, empty_squares, adjacent)) {
                starts.push_back(Coord(row, col));
            }
        }
    }
}

int Directional::CountEmptySquares(int row, int col, int word_length,
                                   bool* perp_words_valid,
                                   bool* adjacent) const {
    int empty_squares = 0;
    for(int c = col; c < col + word_length; c++) {
        if(board_(row, c).IsEmpty()) {
            const CrossWordData& cwd = crosswords_[row][c];
            if(cwd.PerpWordExists() and !cwd.AnyTilesValid()) {
                *perp_words_valid = false;
                break;
            }
            empty_squares++;
        }
        if(IsAboveSquareFull(row, c) || IsBelowSquareFull(row, c)) {
            *adjacent = true;
        }
    }
    return empty_squares;
}

bool Directional::IsLeftSquareEmpty(int row, int col) const {
    if(col == 0)
        return true;
    return board_(row, col - 1).IsEmpty();
}

bool Directional::IsRightSquareEmpty(int row, int col, int word_length) const {
    if(col == board_.GetColumns() - word_length)
        return true;
    return board_(row, col + word_length).IsEmpty();
}

bool Directional::IsAboveSquareFull(int row, int col) const {
    return row > 0 and !board_(row - 1, col).IsEmpty();
}

bool Directional::IsBelowSquareFull(int row, int col) const {
    return row < board_.GetRows() - 1 and !board_(row + 1, col).IsEmpty();
}

bool EnoughEmptySquares(int empty_squares, int rack_size) {
    return empty_squares > 0 and empty_squares <= rack_size;
}

bool ConnectsToBoard(int word_length, int empty_squares, bool adjacent) {
    return empty_squares < word_length or adjacent;
}

void Directional::PrepareForMove(int rack_size) const {
    if(board_.IsEmpty()) {
        ClearCrossWordData();
    }
    GenerateUpdatedStartCoords(rack_size);
}

void Directional::CrossWordData::Clear() {
    perp_letters_score_ = -1;
    pre_score_ = 0;
    post_score_ = 0;
    valid_letters_.clear();
    pre_word_.clear();
    post_word_.clear();
}

void Directional::CrossWordData::Update(const ReadableBoard& board,
                                        int row, int col,
                                        shared_ptr<Lexicon> lexicon,
                                        const set<char>& alphabet) {
    valid_letters_.clear();
    pre_word_.clear();
    post_word_.clear();
    pre_score_ = 0;
    post_score_ = 0;
    perp_letters_score_ = -1;
    if(board(row, col).IsEmpty()) {
        tie(pre_word_, pre_score_) = VertSequence(board, row - 1, col, -1);
        tie(post_word_, post_score_) = VertSequence(board, row + 1, col, 1);
        if(!pre_word_.empty() || !post_word_.empty()) {
            for(char let : alphabet) {
                if(lexicon->Contains(pre_word_ + let + post_word_)) {
                    valid_letters_.insert(let);
                }
            }
            perp_letters_score_ = pre_score_ + post_score_;
        }
    }
}

void Directional::CrossWordData::UpdatePreWordScore(const string& new_pre_word,
                                                    int new_pre_score,
                                                    shared_ptr<Lexicon> lexicon,
                                                    const set<char>& alphabet) {
    pre_word_ = new_pre_word;
    pre_score_ = new_pre_score;
    UpdateValidLetters(lexicon, alphabet);

}

void Directional::CrossWordData::UpdatePostWordScore(const string& new_post_word,
                                                     int new_post_score,
                                                     shared_ptr<Lexicon> lexicon,
                                                     const set<char>& alphabet) {
    post_word_ = new_post_word;
    post_score_ = new_post_score;
    UpdateValidLetters(lexicon, alphabet);
}

void Directional::CrossWordData::UpdateValidLetters(shared_ptr<Lexicon> lexicon,
                                                    const set<char>& alphabet) {
    valid_letters_.clear();
    for(char let : alphabet) {
        if(lexicon->Contains(pre_word_ + let + post_word_)) {
            valid_letters_.insert(let);
        }
    }
    perp_letters_score_ = pre_score_ + post_score_;
}


WordScore VertSequence(const ReadableBoard& board, int row, int col, int direc) {
    if(board.OutOfBounds(row, col)) {
        return make_tuple(string(), 0);
    }
    string word;
    int score = 0;
    while(!board.OutOfBounds(row, col) && !board(row, col).IsEmpty()) {
        const Tile& tile = board(row, col);
        score += tile.GetScore();
        word += tile.GetLetter();
        row += direc;
    }
    if(direc < 0)
        reverse(word.begin(), word.end());
    return make_tuple(word, score);
}

MovesFinder::MovesFinder(const ReadableBoard& board,
                         shared_ptr<Alphabet> alphabet,
                         shared_ptr<Lexicon> lexicon)
      : board_(board),
        transposed_(board),
        horizontal_(board_, alphabet, lexicon, true),
        vertical_(transposed_, alphabet, lexicon, false) {

}

void MovesFinder::FindMoves(const Rack& rack,
                            vector<shared_ptr<Move>>* moves) const {
    horizontal_.FindMoves(rack, moves);
    vertical_.FindMoves(rack, moves);
}

void MovesFinder::UpdateLocations(const vector<Coord>& coords) {
    vector<Coord> coords_transposed;
    for(const auto& c : coords) {
        coords_transposed.push_back(c.Transpose());
    }
    horizontal_.UpdateMetaBoard(coords);
    vertical_.UpdateMetaBoard(coords_transposed);
}
