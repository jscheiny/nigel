/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Rack.cpp
 * @brief Implementation of the Rack class.
 */

#include "Rack.h"

#include "Alphabet.h"

using namespace std;
using namespace nigel;

Rack::Rack(const string& source, shared_ptr<Alphabet> alphabet) {
    for(char letter : source)
        push_back(alphabet->CreateTile(letter));
}

string Rack::ToString() const {
    string str;
    for(const Tile& tile : *this)
        str += tile.GetLetter();
    return str;
}

string Rack::ToBlanksString(shared_ptr<Alphabet> alphabet) const {
    char blank = alphabet->GetBlankCharacter();
    string str;
    for(const Tile& tile : *this) {
        if(tile.IsBlank()) {
            str += blank;
        } else {
            str += tile.GetLetter();
        }
    }
    return str;
}

int Rack::Score() const {
    int score = 0;
    for(const Tile& tile : *this)
        score += tile.GetScore();
    return score;
}

void Rack::Remove(const Rack& to_remove) {
    for(const Tile& tile : to_remove) {
        auto itr = find_if(begin(), end(),
            [&tile] (const Tile& rack_tile) -> bool {
                if(tile.IsBlank())
                    return rack_tile.IsBlank();
                return tile.GetLetter() == rack_tile.GetLetter();
            });
        erase(itr);
    }
}

void Rack::PrintWithLowercaseBlanks(ostream& os) const {
    for(const Tile& tile : *this) {
        if(tile.IsBlank()) {
            os << (char)tolower(tile.GetLetter());
        } else {
            os << tile.GetLetter();
        }
    }
}

void Rack::PrintWithBlankSymbol(ostream& os, char blank_character) const {
    for(const Tile& tile : *this) {
        os << (tile.IsBlank() ? blank_character : tile.GetLetter());
    }
}

ostream& nigel::operator<< (ostream& os, const Rack& rack) {
    os << "[ ";
    for(const Tile& tile : rack) {
        os << tile << ' ';
    }
    return os << ']';
}

