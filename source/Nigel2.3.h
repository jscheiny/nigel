/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NIGEL_2_Q_H
#define NIGEL_2_Q_H

#include "Nigel2.2.h"
#include "Nigel1.h"
#include "Utility.h"

#include <map>
#include <fstream>

namespace nigel {

/// A strategy that defines utilities using a quantized version of Nigel 2.2.
NIGEL_DEFINE_STRATEGY(2,3) : public Nigel<1,0> {

public:
    void Preprocess(std::vector<std::shared_ptr<Move>>& moves) override;
    double Utility(Rack& leave) override;

private:
    int game_state_;

};

class Nigel23Resources {

public:
    static Nigel23Resources& Instance() {
        static Nigel23Resources instance;
        return instance;
    }

    Nigel23Resources(const Nigel23Resources&) = delete;
    Nigel23Resources(Nigel23Resources&&) = delete;
    Nigel23Resources& operator= (const Nigel23Resources&) = delete;
    Nigel23Resources& operator= (Nigel23Resources&&) = delete;

    struct QKey;

    double GetUtility(const QKey& entry) { return quantized_map_[entry]; }

private:
    std::map<QKey, double> quantized_map_;

    Nigel23Resources();
    void ReadQuantized(const std::string& path);

};

struct Nigel23Resources::QKey {

    QKey(const std::string& entry_, int game_state_) :
        entry(entry_), game_state(game_state_) {}

    bool operator< (const QKey& other) const {
        if(entry != other.entry)
            return entry < other.entry;
        return game_state < other.game_state;
    }

    std::string entry;
    int game_state;

};

}

#endif
