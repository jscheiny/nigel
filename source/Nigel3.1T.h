/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NIGEL_3_1T_H
#define NIGEL_3_1T_H

#include "Nigel3.1.h"

namespace nigel {

/// A Nigel 3.1 strategy that uses threading to compute the valuations faster.
class Nigel31T : public Nigel<3,1> {

public:
    Nigel31T(int sample_size, int num_threads);

    int GetNumThreads() const {
        return num_threads_;
    }

    double Utility(Rack& leave) override;

private:
    int num_threads_;

    std::pair<double, int> PerformSamples(const std::string& leaveStr, int num);

};

}

#endif
