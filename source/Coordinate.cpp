/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Coordinate.cpp
 * @brief Implementation of the Coord and MoveCoord classes.
 */

#include "Coordinate.h"

#include <sstream>
#include <fstream>
#include <cstdlib>
#include <string>

using namespace std;
using namespace nigel;

bool Coord::operator< (const Coord& other) const {
    if(row != other.row)
        return row < other.row;
    return col < other.col;
}

bool Coord::operator== (const Coord& other) const {
    return row == other.row && col == other.col;
}

bool Coord::operator!= (const Coord& other) const {
    return row != other.row || col != other.col;
}

ostream& nigel::operator<< (ostream& os, const Coord& c) {
    return os << "(" << c.row << ", " << c.col << ")";
}

const string MoveCoord::kAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

MoveCoord::MoveCoord(const string& coord_str) {
    SetWithCoordStr(coord_str);
}

void MoveCoord::SetWithCoordStr(const string& coord_str) {
    if(kAlphabet.find(coord_str[0]) == string::npos) {
        horiz = true;
        row = atoi(coord_str.substr(0, coord_str.size() - 1).c_str()) - 1;
        col = kAlphabet.find(coord_str[coord_str.size() - 1]);
    } else {
        horiz = false;
        col = kAlphabet.find(coord_str[0]);
        row = atoi(coord_str.substr(1).c_str()) - 1;
    }
}

string MoveCoord::ToString() const {
    stringstream ss;
    if(horiz) {
        ss << row + 1;
        ss << kAlphabet[col];
    } else {
        ss << kAlphabet[col];
        ss << row + 1;
    }
    return ss.str();
}

bool MoveCoord::operator< (const MoveCoord& other) const {
    if(row != other.row)
        return row < other.row;
    if(col != other.col)
        return col < other.col;
    return horiz < other.horiz;
}

bool MoveCoord::operator==(const MoveCoord& other) const {
    return row == other.row &&
           col == other.col &&
           horiz == other.horiz;
}

bool MoveCoord::operator!=(const MoveCoord& other) const {
    return row != other.row ||
           col != other.col ||
           horiz != other.horiz;
}

ostream& nigel::operator<< (ostream& os, const MoveCoord& c) {
    return os << c.ToString();
}
