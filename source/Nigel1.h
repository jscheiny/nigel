/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NIGEL_1_H
#define NIGEL_1_H

#include "Strategy.h"
#include "Versioning.h"

namespace nigel {

class Player;

/// A strategy that choses the play with the highest score.
NIGEL_DEFINE_STRATEGY(1,0) : public Strategy {

public:
    /// Overridden to do nothing
    void Preprocess(std::vector<std::shared_ptr<Move>>& moves) override { }
    /// Overridden to do nothing
    void Postprocess(std::vector<std::shared_ptr<Move>>& moves) override { }
    /// Overridden to return 0 (no utility valuation).
    double Utility(Rack& leave) override { return 0; }

};

}

#endif
