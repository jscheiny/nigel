/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file CLI.h
 * @brief Implementation of the CLI class.
 */

#include "CLI.h"

#include "Utility.h"
#include "MinDawg.h"
#include "Player.h"
#include "Nigel.h"
#include "Game.h"

#include <fstream>
#include <iostream>

using namespace std;
using namespace nigel;

using CLI = CommandLineInterface;

bool CLI::CommandArg::operator== (const string& probe) const {
    if(probe.size() > 2) {
        if(StartsWith(probe, "--")) {
            return probe.substr(2) == full_cmd_;
        }
        return false;
    }
    if(probe.size() == 2) {
        return probe[1] == abbreviation_;
    }
    return false;
}

void CLI::CommandArg::Parse(const ArgVec& args, ArgVec::iterator& itr) {
    auto next = itr + 1;
    if(needs_arg_) {
        if(next == args.end() || (*next).empty() || (*next)[0] == '-')
            throw Error(error_);
        parser_(*next);
        ++itr;
    } else {
        parser_("");
    }
}

const map<string, CLI::CliMemberFunctionPtr> CLI::kModeOpts = {
    {"moves",     &CLI::Moves},
    {"selfplay",  &CLI::SelfPlay},
    {"builddict", &CLI::BuildDict},
    {"builddawg", &CLI::BuildDawg},
    {"gui",       &CLI::Gui}
};

const map<string, Board::Config> CLI::kBoardOpts = {
    {"[scrabble]",Board::Config::SCRABBLE},
    {"[super]",   Board::Config::SUPER_SCRABBLE},
    {"[wwf]",     Board::Config::WWF}
};

const map<string, Alphabet::Config> CLI::kAlphabetOpts = {
    {"[english]", Alphabet::Config::ENGLISH},
    {"[eng_ss]",  Alphabet::Config::ENGLISH_SS},
    {"[eng_wwf]", Alphabet::Config::ENGLISH_WWF},
    {"[french]",  Alphabet::Config::FRENCH},
    {"[italian]", Alphabet::Config::ITALIAN}
};

const map<string, Lexicon::Config> CLI::kLexiconOpts = {
    {"[CSW12]",   Lexicon::Config::CSW12},
    {"[ODS4]",    Lexicon::Config::ODS4},
    {"[ODS5]",    Lexicon::Config::ODS5},
    {"[OSPD4]",   Lexicon::Config::OSPD4},
    {"[OSWI]",    Lexicon::Config::OSWI},
    {"[SOPWODS]", Lexicon::Config::SOWPODS},
    {"[TWL06]",   Lexicon::Config::TWL06},
    {"[TWL98]",   Lexicon::Config::TWL98},
    {"[WWF]",     Lexicon::Config::WWF},
    {"[ZINGA05]", Lexicon::Config::ZINGA05},
};

const string CLI::kShortHelpPath = NIGEL_RESOURCE("info/shorthelp.txt");
const string CLI::kFullHelpPath = NIGEL_RESOURCE("info/help.txt");
const string CLI::kVersionPath = NIGEL_RESOURCE("info/version.txt");

CLI::CommandLineInterface(int argc, char* argv[]) :
mode_(&CLI::Moves),
board_config_(Board::Config::SCRABBLE),
alphabet_config_(Alphabet::Config::ENGLISH),
lexicon_config_(Lexicon::Config::TWL06),
computer_("2.2"), num_self_play_(1000), quiet_(false) {
    InitParsers();
    transform(argv, argv + argc, back_inserter(args_),
              [](char* arg) { return string(arg); });
    try {
        Parse();
    } catch(Error& e) {
        cerr << "Error:" << e.msg << endl;
    }
}

void CLI::InitParsers() {
    parsers_ = {
        CommandArg("mode", "No mode specified.",
        [this](const string& arg) {
            auto mode_itr = kModeOpts.find(arg);
            if(mode_itr == kModeOpts.end())
                throw Error("Unknown mode" + arg);
            mode_ = mode_itr->second;
        }),

        CommandArg("computer", "No computer player type specified.",
                   SetField(computer_)),

        CommandArg("position", "No position gcg file specified.",
                   SetField(gcg_path_)),

        CommandArg("board", "No contents path specified.",
                   SetField(contents_path_)),

        CommandArg("out", "No output path specified.",
                   SetField(out_path_)),

        CommandArg("wordlist", "No word list path specified.",
                   SetField(word_list_path_)),

        CommandArg("rack", "No rack specified.", SetField(rack_)),

        CommandArg("lex", "No lexicon specified.",
        [this](const string& arg) {
            auto lexicon_itr = kLexiconOpts.find(arg);
            if(lexicon_itr == kLexiconOpts.end())
                lexicon_config_path_ = arg;
            else
                lexicon_config_ = lexicon_itr->second;
        }),

        CommandArg("alphabet", "No alphabet configuration specified.",
        [this](const string& arg) {
            auto alphabet_itr = kAlphabetOpts.find(arg);
            if(alphabet_itr == kAlphabetOpts.end())
                alphabet_config_path_ = arg;
            else
                alphabet_config_ = alphabet_itr->second;
        }),

        CommandArg("config", 'g', "No board configuration specified.",
        [this](const string& arg) {
            auto board_itr = kBoardOpts.find(arg);
            if(board_itr == kBoardOpts.end())
                board_config_path_ = arg;
            else
                board_config_ = board_itr->second;
        }),

        CommandArg("numselfplay", "No self play value specified.",
        [this](const string& arg) {
            num_self_play_ = atoi(arg.c_str());
            if(num_self_play_ <= 0)
                throw Error("Number of self play games must be a positive integer.");
        }),

        CommandArg("quiet", [this]() { quiet_ = true; }),

        CommandArg("help", [this]() { PrintFile(kFullHelpPath); }),

        CommandArg("version", [this]() { PrintFile(kVersionPath); })
    };
}

void CLI::Parse() {
    if(args_.size() <= 1) {
        PrintFile(kShortHelpPath);
        return;
    }
    for(auto arg_itr = args_.begin() + 1; arg_itr != args_.end(); arg_itr++) {
        auto parser_itr = find(parsers_.begin(), parsers_.end(), *arg_itr);
        if(parser_itr != parsers_.end()) {
            parser_itr->Parse(args_, arg_itr);
        } else {
            throw Error(string("Unknown option: ") + *arg_itr);
        }
    }

    (this->*mode_)();
}

void CLI::CheckForNext(const vector<string>::iterator& next,
                                        const string& message) const {
    if(next == args_.end() || (*next).empty() || (*next)[0] == '-')
        throw Error(message);
}

void CLI::PrintFile(const string& path) {
    string line;
    ifstream is(path);

    while(getline(is, line)) {
        cout << line << endl;
    }
}

void CLI::Moves() {
    string board_path = BoardPath();
    string alphabet_path = AlphabetPath();
    string lexicon_path = LexiconPath();

    auto alphabet = make_shared<Alphabet>(alphabet_path);
    auto lexicon = make_shared<Lexicon>(lexicon_path);
    auto tilebag = make_shared<TileBag>(alphabet);
    auto board = make_shared<Board>(alphabet, lexicon, board_path);

    if(!contents_path_.empty()) {
        board->ReadBoardContents(contents_path_, alphabet);
        tilebag->RefillFromBoard(*board);
    }

    Rack player_rack;
    if(!rack_.empty()) {
        for(auto letter : rack_) {
            player_rack.push_back(alphabet->CreateTile(letter));
        }
    } else {
        player_rack = tilebag->Draw(7);
    }

    Player player("", board, tilebag, lexicon, player_rack);
    SetPlayerStrategy(&player);

    auto moves = player.Moves();
    for(auto move : moves) {
        cout << move << endl;
    }
}

void CLI::SelfPlay() {
    string board_path = BoardPath();
    string alphabet_path = AlphabetPath();
    string lexicon_path = LexiconPath();

    Game game(board_path, alphabet_path, lexicon_path);
    SetPlayerStrategy(game.AddPlayer("Player1"));
    SetPlayerStrategy(game.AddPlayer("Player2"));
    for(int i = 0; i < num_self_play_; i++) {
        if(!quiet_) {
            cout << "Game #" << i << endl;
        }
        game.Play(!quiet_, !quiet_);
        game.Reset();
    }
}

string CLI::BoardPath() const {
    return board_config_path_.empty()
        ? Board::GetConfigPath(board_config_)
        : board_config_path_;
}

string CLI::AlphabetPath() const {
    return alphabet_config_path_.empty()
        ? Alphabet::GetConfigPath(alphabet_config_)
        : alphabet_config_path_;
}

string CLI::LexiconPath() const {
    return lexicon_config_path_.empty()
        ? Lexicon::GetConfigPath(lexicon_config_)
        : lexicon_config_path_;
}

void CLI::SetPlayerStrategy(Player* player) const {
    if(computer_ == "1") {
        player->SetStrategy<Nigel<1,0>>();
    } else if(computer_ == "2.1") {
        player->SetStrategy<Nigel<2,1>>();
    } else if(computer_ == "2.2") {
        player->SetStrategy<Nigel<2,2>>();
    } else if(computer_ == "2.3") {
        player->SetStrategy<Nigel<2,3>>();
    } else if(computer_ == "3.1") {
        player->SetStrategy<Nigel<3,1>>();
    } else if(computer_ == "3.2") {
        player->SetStrategy<Nigel<3,2>>();
    } else {
        throw Error("Unknown computer player type: " + computer_);
    }
}

void CLI::BuildDawg() {
    ofstream out = OpenOutPath();
    vector<string> word_list = ReadWordList();
    MinDawg dawg(word_list.begin(), word_list.end());
    int start = dawg.GetNodes();
    dawg.Minimize();
    int end = dawg.GetNodes();

    double percent = 1 - (double)end / start;
    percent = (int)(percent * 10000) / 100.;
    cout << "Dawg minimized by " << percent << " percent" << endl;
    dawg.Save(out);
}

void CLI::BuildDict() {
    if(word_list_path_.empty())
        throw Error("Word list path must be specified.");
    if(out_path_.empty())
        throw Error("Out path must be specified.");
    if(!EndsWith(out_path_, "/"))
        out_path_ += '/';
    BuildNigelDict(word_list_path_, out_path_, quiet_);
}

void CLI::Gui() {

}

vector<string> CLI::ReadWordList() {
    if(word_list_path_.empty())
        throw Error("Word list must be specified.");
    ifstream fin(word_list_path_);
    if(!fin)
        throw OpenError(word_list_path_);

    vector<string> word_list;
    string line;
    while(getline(fin, line)) {
        word_list.push_back(Strip(line));
    }
    return word_list;
}

ofstream CLI::OpenOutPath() {
    if(out_path_.empty())
        throw Error("Out path must be specified.");
    ofstream out(out_path_);
    if(!out)
        throw OpenError(out_path_);
    return out;
}
