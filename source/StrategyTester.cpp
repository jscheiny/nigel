/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file StrategyTester.cpp
 * @brief Implementation of the StrategyTester class.
 */

#include "StrategyTester.h"

#include "Board.h"
#include "TileBag.h"
#include "Alphabet.h"
#include "Lexicon.h"

#include <iostream>

using namespace std;
using namespace nigel;

StrategyTester::StrategyInfo::StrategyInfo(Player* player_) : player(player_) {
    Reset();
}

void StrategyTester::StrategyInfo::Reset() {
    p1_wins = 0;
    p2_wins = 0;
    p1_ties = 0;
    p1_score = 0;
    p2_score = 0;
}

StrategyTester::StrategyTester(Game::Config config) : game_(config) {
    Init();
}

StrategyTester::StrategyTester(Board::Config board_config,
                               Alphabet::Config alphabet_config,
                               Lexicon::Config lexicon_config) :
game_(board_config, alphabet_config, lexicon_config) {
    Init();
}

StrategyTester::StrategyTester(const string& board,
                               const string& tilebag,
                               const string& lexicon) :
game_(board, tilebag, lexicon) {
    Init();
}

void StrategyTester::Init() {
    info_[0] = StrategyInfo(game_.AddPlayer("Strat1"));
    info_[1] = StrategyInfo(game_.AddPlayer("Strat1"));
    strategy1_ = info_;
    strategy2_ = &info_[1];
}

void StrategyTester::ResetStats() {
    info_[0].Reset();
    info_[1].Reset();
    games_ = 0;
}

int StrategyTester::StratPlayerWins(ST_Strategy s, ST_Player p) const {
    if(p == ST_Player::P1)
        return info_[s].p1_wins;
    return info_[s].p2_wins;
}

double StrategyTester::StratPlayerAverageScore(ST_Strategy s, ST_Player p) const {
    if(p == ST_Player::P1)
        return 2 * double(info_[s].p1_score) / games_;
    return 2 * double(info_[s].p2_score) / games_;
}

void StrategyTester::Run(int matches, bool print_messages) {
    if(print_messages)
        cout << "Running " << matches << " games with " << strategy1_->player->GetName()
             << " as player 1 and " << strategy2_->player->GetName() << " as player 2."
             << endl;
    Run(matches, print_messages, &strategy1_->p1_wins, &strategy2_->p2_wins,
        &strategy1_->p1_ties, &strategy1_->p1_score, &strategy2_->p2_score);

    if(print_messages)
        cout << endl << "Running " << matches << " games with " << strategy2_->player->GetName()
             << " as player 1 and " << strategy1_->player->GetName() << " as player 2."
             << endl;
    Run(matches, print_messages, &strategy1_->p2_wins, &strategy2_->p1_wins,
        &strategy2_->p1_ties, &strategy2_->p1_score, &strategy1_->p2_score);

    if(print_messages)
        cout << endl;
}

void StrategyTester::Run(int matches, bool printCounter,
                         int* strat1_win_counter, int* strat2_win_counter,
                         int* ties_counter, int* p1_score_counter,
                         int* p2_score_counter) {
    for(int i = 1; i <= matches; i++) {
        cout << "Game #" << i << endl;
        games_++;
        game_.Reset();
        game_.Play(false, false);
        Player* winner = game_.Winner();
        if(!winner) {
            (*ties_counter)++;
        } else if(winner == strategy1_->player) {
            (*strat1_win_counter)++;
        } else {
            (*strat2_win_counter)++;
        }

        (*p1_score_counter) += game_.GetPlayer(0)->GetScore();
        (*p2_score_counter) += game_.GetPlayer(1)->GetScore();
    }
    game_.RotatePlayers();
}

void StrategyTester::PrintReport(ostream& os) {
    string strat1_name = strategy1_->player->GetName();
    string strat2_name = strategy2_->player->GetName();
    os << "STRATEGY REPORT: " << strat1_name << " vs. " << strat2_name
       << endl;
    os << "Player 1: " << strat1_name << ", Player2: " << strat2_name << " ("
       << games_ / 2 << " games)" << endl;
    os << "\t" << strat1_name<< " wins = " << strategy1_->p1_wins
       << " " << strat2_name << " wins = " << strategy2_->p2_wins
       << " Ties = " << strategy1_->p1_ties << endl;
    os << "\t" << strat1_name << " average score = "
       << StratPlayerAverageScore(ST_Strategy::S1, ST_Player::P1) << endl;
    os << "\t" << strat2_name << " average score = "
       << StratPlayerAverageScore(ST_Strategy::S2, ST_Player::P2) << endl;

    os << "Player 1: " << strat2_name << ", Player2: " << strat1_name << " ("
        << games_ / 2 << " games)" << endl;
    os << "\t" << strat2_name << " wins = " << strategy2_->p1_wins
       << " " << strat1_name << " wins = " << strategy1_->p2_wins
       << " Ties = " << strategy2_->p1_ties << endl;
    os << "\t" << strat2_name << " average score = "
       << StratPlayerAverageScore(ST_Strategy::S2, ST_Player::P1) << endl;
    os << "\t" << strat1_name << " average score = "
       << StratPlayerAverageScore(ST_Strategy::S1, ST_Player::P2) << endl;

    os << "Overall stats (" << games_ << " games):" << endl;
    os << "\t" << strat1_name << ": wins = " << StratWins(ST_Strategy::S1)
       << ", avg score = " << StratAverageScore(ST_Strategy::S1) << endl;
    os << "\t" << strat2_name << ": wins = " << StratWins(ST_Strategy::S2)
       << ", avg score = " << StratAverageScore(ST_Strategy::S2) << endl;
    os << endl;
}
