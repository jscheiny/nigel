#ifndef END_GAME_STRATEGY_H
#define END_GAME_STRATEGY_H

#include "Strategy.h"

namespace nigel {

class Player;

class EndGameStrategy : public Strategy {

public:
    void Preprocess(std::vector<std::shared_ptr<Move>>& moves) override {}
    void Postprocess(std::vector<std::shared_ptr<Move>>& moves) override;
    double Utility(Rack& leave) override { return 0; }

private:

    static const int kThreshold;
    static const int kNumSubMovesValuated;

    double ValuateMove(std::shared_ptr<Move> move,
                       const Rack& player_rack, const Rack& opponent_rack,
                       int score_diff, int score_modifier, int consec_skips, int level=1);

};

}

#endif
