/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Strategy.h
 * @brief Declaration and implementation of the Strategy abstract class.
 */

#ifndef STRATEGY_H
#define STRATEGY_H

#include "Utility.h"

#include <list>
#include <memory>
#include <vector>

namespace nigel {

class Player;
class Move;
class Rack;

/// A strategy for picking the best play.
/**
 * A strategy is an interface that defines how a player selects the what they
 * consider to be their best play. The Strategy class should be subclassed to
 * provide specific strategies (see all the descendants of Nigel1 for the built
 * in strategies). Players own their own instantiation of a Strategy subclass,
 * and use that instance to pick their best plays. Note that Players construct
 * their own strategies, and own it and destroy it, and that a user constructed
 * Strategy cannot be attached to a player. A subclass of Strategy must provide
 * a constructor that has its first argument be a Player pointer, and calls the
 * protected Strategy constructor.
 *
 * Strategies are defined around the idea of assigning a utility to each leave,
 * and then the best play is the one with the highest score + utility. For every
 * possible leave of a given play the Utility(Rack&) function is called and
 * returns a value, that value is assigned to to every move that has that leave.
 * To help with the process, two extra methods preprocess and postprocess are
 * provided. The preproces method is given the set of moves for the given turn
 * (not including swaps and the pass play), and with unset utilities (0). The
 * postprocess method is given the est of moves for the given turn including
 * all swaps and the pass play, and with all of their utilities set based on the
 * leave of the play. The order of calls by a player choosing a play is
 * preprocess, utility (for every leave), postprocess.
 *
 * If one wants to define a strategy that is based less on leave than on say
 * forward looking through the game tree, one should focus on the postprocess
 * method and simply have the utility method return 0.
 */
class Strategy {

public:
    /// Virtual destructor
    virtual ~Strategy() {}

    /// Called prior to computing utilities.
    /**
     * This preprocesses the set of moves and the state of the game to setup
     * data for use in common to ever call of the utility function. The set of
     * moves passed in will be the moves that are playable on the board, and
     * does not include swap plays or the passing play. The utilities of these
     * moves will be 0, and will be overridden after the player computes
     * utilities.
     * @param moves the moves on the board (excluding swaps and the pass play)
     */
    virtual void Preprocess(std::vector<std::shared_ptr<Move>>& moves) = 0;

    /// Called after computing utilities.
    /**
     * After this method is called, the move with the largest score + utility
     * will be chosen as the best play for the calling player. The set of moves
     * passed in will include all plays (including swaps and the passing play),
     * with their utilities set based on the leave of the play.
     * @param moves the moves on the board (including swaps and the pass play)
     */
    virtual void Postprocess(std::vector<std::shared_ptr<Move>>& moves) = 0;

    virtual double Utility(Rack& leave) = 0;

    friend Player;

protected:
    Player* GetPlayer() {
        return player_;
    }

private:
    void SetPlayer(Player* player) {
        player_ = player;
    }

    Player* player_;

};

}

#endif
