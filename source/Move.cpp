/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Move.cpp
 * @brief Implementation of the Move class and its subclasses.
 */

#include "Move.h"

#include "Game.h"
#include "Tile.h"
#include "Board.h"
#include "Player.h"
#include "Utility.h"

#include <cctype>
#include <fstream>
#include <iostream>

using namespace std;
using namespace nigel;

// ========================================================
// MOVE
// ========================================================

Move::Move(const string& word,
           const Rack& leave,
           const Rack& placed,
           const Rack& tiles_on_board,
           int row, int col, bool horiz,
           int score)
      : empty_(false)
      , word_(word)
      , leave_(leave)
      , placed_(placed)
      , tiles_on_board_(tiles_on_board)
      , row_(row)
      , col_(col)
      , horiz_(horiz)
      , score_(score)
      , raw_utility_(0)
      , player_(nullptr)
      , score_after_play_(0) {
}

Move::Move(const string& word,
           const Rack& leave,
           const Rack& placed,
           const Rack& tiles_on_board,
           const MoveCoord& coord,
           int score)
      : Move(word, leave, placed, tiles_on_board, coord.row, coord.col, coord.horiz, score) {
}

bool Move::operator< (const Move& other) const {
    if(GetUtility() != other.GetUtility())
        return GetUtility() < other.GetUtility();
    if(word_ != other.word_)
        return word_ < other.word_;
    return GetCoord() < other.GetCoord();
}

bool Move::ComparePtrs(const shared_ptr<Move> a, const shared_ptr<Move> b) {
    return *a < *b;
}

bool Move::ComparePtrsDesc(const shared_ptr<Move> a, const shared_ptr<Move> b) {
    return *b < *a;
}

shared_ptr<Move> Move::Clone() const {
    return make_shared<Move>(*this);
}

void Move::Print(ostream& os) const {
    PrintPlayerInfo(os);
    PrintMoveDetails(os);
    PrintScoreInfo(os);
    if(HasNote()) {
        os << "#note " << note_ << '\n';
    }
}

void Move::PrintMoveDetails(ostream& os) const {
    PrintPlayerRack(os);
    os << ' ' << GetCoord() << ' ';
    tiles_on_board_.PrintWithLowercaseBlanks(os);
}

void Move::PrintPlayerInfo(ostream& os) const {
    os << '>';
    if(player_ != nullptr)
        os << player_->GetNickname();
    os << ": ";
}

void Move::PrintPlayerRack(ostream& os) const {
    placed_.PrintWithBlankSymbol(os, blank_character_);
    leave_.PrintWithBlankSymbol(os, blank_character_);
}

void Move::PrintScoreInfo(ostream& os) const {
    os << ' ' << (score_ >= 0 ? "+" : "") << score_ << ' ' << score_after_play_
       << '\n';
}

void Move::UpdateBlank() {
    const Game* game = player_->GetGame();
    if(game) {
        blank_character_ = game->GetTileBag()->GetAlphabet()->GetBlankCharacter();
    }
}

// ========================================================
// SWAP
// ========================================================

Swap::Swap(const Rack& leave, const Rack& swapped) :
Move("[exch]", leave, swapped, swapped, -1, -1, true, 0) {

}

shared_ptr<Move> Swap::Clone() const {
    return make_shared<Swap>(*this);
}

void Swap::PrintMoveDetails(ostream& os) const {
    PrintPlayerRack(os);
    os << " -";
    GetSwapped().PrintWithLowercaseBlanks(os);
}

// ========================================================
// ENDGAMEGAIN
// ========================================================

EndGameGain::EndGameGain(const Rack& gained, int multiplier) :
Move("[endgamegain]", gained, Rack(), Rack(), -1, -1, true,
     gained.Score() * multiplier) {
}

shared_ptr<Move> EndGameGain::Clone() const {
    return make_shared<EndGameGain>(*this);
}

void EndGameGain::PrintMoveDetails(ostream& os) const {
    os << '(';
    GetLeave().PrintWithBlankSymbol(os, blank_character());
    os << ')';
}

// ========================================================
// ENDGAMELOSS
// ========================================================

EndGameLoss::EndGameLoss(const Rack& lost) :
Move("[endgameloss]", lost, Rack(), Rack(), -1, -1, true, -lost.Score()) {

}

shared_ptr<Move> EndGameLoss::Clone() const {
    return make_shared<EndGameLoss>(*this);
}

void EndGameLoss::PrintMoveDetails(ostream& os) const {
    GetLeave().PrintWithBlankSymbol(os, blank_character());
    os << " (";
    GetLeave().PrintWithBlankSymbol(os, blank_character());
    os << ')';
}

// ========================================================
// TIMEPENALTY
// ========================================================

TimePenalty::TimePenalty(const Rack& rack, int penalty) :
Move("[timepenalty]", rack, Rack(), Rack(), -1, -1, true, penalty) {

}

shared_ptr<Move> TimePenalty::Clone() const {
    return make_shared<TimePenalty>(*this);
}

void TimePenalty::PrintMoveDetails(ostream& os) const {
    GetLeave().PrintWithBlankSymbol(os, blank_character());
    os << " (time)";
}

// ========================================================
// WITHDRAWNPHONEY
// ========================================================

WithdrawnPhoney::WithdrawnPhoney(const Rack& rack, int lost_points) :
Move("[withdrawnphoney]", rack, Rack(), Rack(), -1, -1, true, lost_points) {

}

shared_ptr<Move> WithdrawnPhoney::Clone() const {
    return make_shared<Move>(*this);
}

void WithdrawnPhoney::PrintMoveDetails(ostream& os) const {
    GetLeave().PrintWithBlankSymbol(os, blank_character());
    os << " ";
}

// ========================================================
// CHALLENGEBONUS
// ========================================================

ChallengeBonus::ChallengeBonus(const Rack& rack) : ChallengeBonus(rack, 5) {

}

ChallengeBonus::ChallengeBonus(const Rack& rack, int bonus) :
Move("(challenge)", rack, Rack(), Rack(), -1, -1, true, bonus) {

}

shared_ptr<Move> ChallengeBonus::Clone() const {
    return make_shared<Move>(*this);
}

void ChallengeBonus::PrintMoveDetails(ostream& os) const {
    GetLeave().PrintWithBlankSymbol(os, blank_character());
    os << " (challenge)";
}
