/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Lexicon.cpp
 * @brief Implementation of the Lexicon class.
 */

#include "Lexicon.h"

#include "Utility.h"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace nigel;
using namespace std;

const map<Lexicon::Config, string> Lexicon::kConfigPathMap {
    {Config::CSW12,   NIGEL_RESOURCE("lexicons/CSW12.nigeldict")},
    {Config::ODS4,    NIGEL_RESOURCE("lexicons/ODS4.nigeldict")},
    {Config::ODS5,    NIGEL_RESOURCE("lexicons/ODS5.nigeldict")},
    {Config::OSPD4,   NIGEL_RESOURCE("lexicons/OSPD4.nigeldict")},
    {Config::OSWI,    NIGEL_RESOURCE("lexicons/OSWI.nigeldict")},
    {Config::SOWPODS, NIGEL_RESOURCE("lexicons/SOWPODS.nigeldict")},
    {Config::TWL06,   NIGEL_RESOURCE("lexicons/TWL06.nigeldict")},
    {Config::TWL98,   NIGEL_RESOURCE("lexicons/TWL98.nigeldict")},
    {Config::WWF,     NIGEL_RESOURCE("lexicons/WWF.nigeldict")},
    {Config::ZINGA05, NIGEL_RESOURCE("lexicons/ZINGA05.nigeldict")}
};

WeakCache<Lexicon::Config, Lexicon> Lexicon::lexiconCache {};

shared_ptr<Lexicon> Lexicon::Create(Config config) {
    return lexiconCache.Retrieve(config);
}

Lexicon::Lexicon(Config config) {
    ReadNigelDict(kConfigPathMap.at(config));
}

Lexicon::Lexicon(const string& path) {
    ReadDict(path);
}

Lexicon::~Lexicon() {
    for(auto entry : dawgs_) {
        delete entry.second;
    }
}

bool Lexicon::Contains(const string & word) const {
    return word_set_.find(word) != word_set_.end();
}

const Dawg::Node* Lexicon::GetRoot(int word_length) const {
    const Dawg* d = GetDawg(word_length);
    if(d == nullptr)
        return nullptr;
    return d->GetRoot();
}

const Dawg* Lexicon::GetDawg(int word_length) const {
    if(dawgs_.count(word_length) == 0)
        return nullptr;
    return dawgs_.at(word_length);
}

void Lexicon::ReadDict(const string& path) {
    if(IsNigelDictPath(path)) {
        if(IsValidNigelDict(path)) {
            ReadNigelDict(path);
        }
    } else {
        ReadPlainDict(path);
    }
}

void Lexicon::ReadPlainDict(const string & path) {
    ifstream is(path);
    if(!is)
        throw OpenError();
    string line;
    bool first = true;
    while(getline(is, line)) {
        line = Upper(line);
        word_set_.insert(line);
        int word_length = line.size();

        if(first) {
            max_word_length_ = word_length;
            min_word_length_ = word_length;
            first = false;
        } else if(word_length < min_word_length_) {
            min_word_length_ = word_length;
        } else if(word_length > max_word_length_) {
            max_word_length_ = word_length;
        }

        if(dawgs_.count(word_length) == 0) {
            dawgs_.emplace(word_length, new Dawg());
        }
        dawgs_[word_length]->Insert(line);
    }
    // Fill in any remaining empty dawgs for word lengths with no words.
    for(int word_length = min_word_length_; word_length <= max_word_length_;
            word_length++) {
        if(dawgs_.count(word_length) == 0) {
            dawgs_.emplace(word_length, new Dawg());
        }
    }
    is.close();
}

void Lexicon::ReadNigelDict(const string& path) {
    string temp_path = path;
    if(temp_path[temp_path.length() - 1] != '/')
        temp_path += "/";
    ifstream is(temp_path + "list.txt");
    string line;
    bool first = true;
    while(getline(is, line)) {
        line = Upper(line);
        int word_length = line.size();
        if(first) {
            max_word_length_ = word_length;
            min_word_length_ = word_length;
            first = false;
        } else if(word_length < min_word_length_) {
            min_word_length_ = word_length;
        } else if(word_length > max_word_length_) {
            max_word_length_ = word_length;
        }
        word_set_.insert(line);
    }
    is.close();
    for(int word_length = min_word_length_; word_length <= max_word_length_;
            word_length++) {
        stringstream ss;
        ss << temp_path << word_length << ".dawg";
        string path = ss.str();
        if(FileExists(path)) {
            dawgs_.emplace(word_length, new Dawg(path));
        } else {
            dawgs_.emplace(word_length, new Dawg());
        }
    }
}

bool Lexicon::IsNigelDictPath(const string & dir_path) {
    return EndsWith(dir_path, ".nigeldict/") ||
           EndsWith(dir_path, ".nigeldict");
}

bool Lexicon::IsValidNigelDict(const string & dir_path) {
    string temp_path = dir_path;
    if(temp_path[temp_path.length() - 1] != '/')
        temp_path += "/";
    if(!FileExists(temp_path + "list.txt"))
        return false;
    return true;
}
