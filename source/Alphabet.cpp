/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Alphabet.cpp
 * @brief Implementation of the Alphabet class.
 */

#include "Alphabet.h"

// #include "Utility.h"
#include "Board.h"
#include "Rack.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace nigel;

const map<Alphabet::Config, string> Alphabet::kConfigPathMap {
    {Config::ENGLISH,     NIGEL_RESOURCE("/alphabets/english.abc") },
    {Config::ENGLISH_SS,  NIGEL_RESOURCE("/alphabets/english-super.abc") },
    {Config::ENGLISH_WWF, NIGEL_RESOURCE("/alphabets/english-wwf.abc") },
    {Config::FRENCH,      NIGEL_RESOURCE("/alphabets/french.abc") },
    {Config::ITALIAN,     NIGEL_RESOURCE("/alphabets/italian.abc") }
};

WeakCache<Alphabet::Config, Alphabet> Alphabet::alphabetCache {};

shared_ptr<Alphabet> Alphabet::Create(Config config) {
    return alphabetCache.Retrieve(config);
}

Alphabet::Alphabet(Config config) : Alphabet(kConfigPathMap.at(config)) {}

Alphabet::Alphabet(istream& is) : blank_character_('?') {
    ReadFromFile(is);
}

Alphabet::Alphabet(const string& path) : blank_character_('?') {
    ReadFromFile(path);
}

void Alphabet::ReadFromFile(const string& path) {
    ifstream fin(path);
    if(!fin)
        throw OpenError(path);
    try {
        ReadFromFile(fin);
    } catch(InvalidDataError& e) {
        throw InvalidDataError(path);
    }
    fin.close();
}

void Alphabet::ReadFromFile(istream& is) {
    string line;
    while(getline(is, line)) {
        if(line.empty())
            continue;

        if(StartsWith(line, "NAME=")) {
            name_ = line.substr(5);

        } else if(StartsWith(line, "BOARD=")) {
            board_name_ = line.substr(6);

        } else {
            vector<string> parts = Split(line);
            if(parts.size() < 3)
                throw InvalidDataError();

            char letter = parts[0][0];
            int score, freq;
            try {
                score = ConvertToInt(parts[1]);
                freq = ConvertToInt(parts[2]);
            } catch(ConvertError& e) {
                throw InvalidDataError();
            }

            if(letters_.count(letter) == 0) {
                letters_.insert(letter);
                score_freq_map_.emplace(letter, ScoreFreq(score, freq));
            }
            if(score == 0) {
                blank_character_ = letter;
            }
        }
    }
}

Rack Alphabet::RemainingTiles(const Board& board, const Rack& rack) const {
    map<char, int> tile_histogram;
    for(auto entry : score_freq_map_) {
        tile_histogram[entry.first] = entry.second.freq;
    }
    for(const Tile& tile : rack) {
        if(tile.IsBlank()) {
            tile_histogram[blank_character_]--;
        } else {
            tile_histogram[tile.GetLetter()]--;
        }
    }
    for(int row = 0; row < board.GetRows(); row++) {
        for(int col = 0; col < board.GetColumns(); col++) {
            const Tile& tile = board(row, col);
            if(!tile.IsEmpty()) {
                if(tile.IsBlank()) {
                    tile_histogram[blank_character_]--;
                } else {
                    tile_histogram[tile.GetLetter()]--;
                }
            }
        }
    }
    Rack remaining;
    for(auto entry : tile_histogram) {
        Tile tile = CreateTile(entry.first);
        fill_n(back_inserter(remaining), entry.second, tile);
    }
    return remaining;
}

