/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Tile.cpp
 * @brief Implementation of the Tile class.
 */

 #include "Tile.h"

#include <iostream>

using namespace std;
using namespace nigel;

const char Tile::kEmptyLetter = '\0';
const int Tile::kEmptyScore = -1;

bool Tile::operator< (const Tile& other) const {
    if(letter_ != other.letter_)
        return letter_ < other.letter_;
    return score_ < other.score_;
}

bool Tile::operator== (const Tile& other) const {
    return letter_ == other.letter_ && score_ == other.score_;
}

bool Tile::operator!= (const Tile& other) const {
    return letter_ != other.letter_ || score_ != other.score_;
}

ostream& nigel::operator<< (ostream& os, const Tile& tile) {
    return os << tile.GetLetter() << tile.GetScore();
}
