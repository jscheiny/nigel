/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Human.cpp
 * @brief Implementation of the Human class.
 */

#include "Human.h"

#include "Move.h"
#include "Board.h"
#include "Player.h"
#include "Coordinate.h"

#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>


using namespace std;
using namespace nigel;

Human::Human(ostream& out, istream& in)
      : out_(out)
      , in_(in) {
}

void Human::Postprocess(vector<shared_ptr<Move>>& moves) {
    for(shared_ptr<Move> move : moves)
        move->SetRawUtility(-move->GetScore());

    GetPlayer()->GetBoard()->PrintWithColor(out_);
    if(GetPlayer()->GetName() == "") {
        out_ << "Your score: " << GetPlayer()->GetScore() << endl;
        out_ << "Your rack:  " << GetPlayer()->GetRack() << endl;
    } else {
        out_ << GetPlayer()->GetName() + "'s score: " << GetPlayer()->GetScore() << endl;
        out_ << GetPlayer()->GetName() + "'s rack:  " << GetPlayer()->GetRack() <<endl;
    }

    out_ << GetPlayer()->GetRack() << endl;
    out_ << "Your move." << endl;
    out_ << "\tEnter your move as: \"<position> <word>\"." << endl;
    out_ << "\tTo exchange tiles enter: \"exch <tiles>\"." << endl;
    out_ << "\tTo check if a word is in the dictionary: \"check <word>\"."
         << endl;

    string line;
    while(true) {
        out_ << "Move: " << flush;
        getline(in_, line);
        stringstream ss(line);
        // string first; ss >> word;
        // if(first == "exch") {

        // } else if(first == "check") {

        // } else {

        // }
    }

}

shared_ptr<Move> Human::FindMove(const string & word,
                                 const MoveCoord& coord,
                                 vector<shared_ptr<Move>>& moves) {
    auto itr = find_if(moves.begin(), moves.end(),
    [&word, &coord] (shared_ptr<Move> move) {
        return move->GetCoord() == coord &&
               move->GetWord() == word;
    });
    if(itr == moves.end())
        return nullptr;
    return *itr;
}

shared_ptr<Move> Human::FindSwap(string swapped,
                                 vector<shared_ptr<Move>>& moves) {
    sort(swapped.begin(), swapped.end());
    auto itr = find_if(moves.begin(), moves.end(),
    [&swapped] (shared_ptr<Move> move) -> bool {
        if(move->IsSwap()) {
            string placed = move->GetPlaced().ToString();
            sort(placed.begin(), placed.end());
            return placed == swapped;
        }
        return false;
    });
    if(itr == moves.end())
        return nullptr;
    return *itr;
}
