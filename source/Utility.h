/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Utility.h
 * @brief Declaration of utility functions.
 */

#ifndef UTILITY_H
#define UTILITY_H

#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <list>

namespace nigel {

#ifndef NIGEL_RESOURCES_DIR
#define NIGEL_RESOURCES_DIR resources
#endif

#define NIGEL_STR(token) #token
#define NIGEL_MACRO_STR(macro) NIGEL_STR(macro)

#define NIGEL_RESOURCE(relative) NIGEL_MACRO_STR(NIGEL_RESOURCES_DIR) "/" relative

class Alphabet;

/// A function object which whose call operation acts like a linear function.
struct LinearFunction {
    /// Create a linear function with the given slope and y-intercept
    LinearFunction(double slope, double y_int) :
        slope_(slope), y_int_(y_int) {}

    /// Returns m * x + b.
    double operator() (double x) const {
        return slope_ * x + y_int_;
    }

private:
    double slope_;
    double y_int_;
};

/// The base class of all errors thrown by Nigel classes and functions.
struct Error {
    Error(const char* msg_) : msg(msg_) {};
    Error(const std::string& msg_) : msg(msg_) {}
    const std::string msg;
};

/// The base class of all IO based errors thrown by Nigel classes and functions.
struct IOError : public Error {
    IOError(const char* msg) : Error(msg) {}
    IOError(const std::string& msg) : Error(msg) {}
};

/// Thrown in the event of an error opening a file.
struct OpenError : public IOError {
    OpenError(const std::string& path) :
        IOError("Could not open file: " + path + ".") {}
    OpenError() : IOError("Could not open file.") {}
};

/// Thrown in the event of reading in bad data from a file or stream.
struct InvalidDataError : public IOError {
    InvalidDataError(const std::string& path) :
        IOError("Invalid data in file: " + path + ".") {}
    InvalidDataError() : IOError("Invalid data in file.") {}
};

/// Thrown in the even of a bad conversion (say between string and int).
struct ConvertError : public Error {
    ConvertError() : Error("Conversion failed.") {}
};

/// Returns true if the given string ends with the given suffix.
/**
 * @param str the string that should be checked
 * @param suffix the suffix to check the string for
 * @return true if the string ends with the given suffix
 */
bool EndsWith(const std::string & str,
              const std::string & suffix);

/// Returns true if the given string starts with the given prefix.
/**
 * @param str the string that should be checked
 * @param prefix the prefix to check the string for
 * @return true if the string starts with the given prefix
 */
bool StartsWith(const std::string & str,
                const std::string & prefix);

/// Converts the given string to an integer.
/**
 * If the string contains invalid data (is not formatted as an integer), throws
 * a ConvertError.
 * @param str the string to convert to an integer
 * @return the converted integer
 */
int ConvertToInt(const std::string& str);

/// Returns true if the file at the given path exists.
/**
 * @param filename the path of the file.
 * @return true if the file at the path exists, false otherwise.
 */
bool FileExists(const std::string& filename);

/// Splits a given string in to whitespace delimited components.
/**
 * @param input the string to split
 * @return a vector of the whitespace delimited substrings of the input
 */
std::vector<std::string> Split(const std::string& input);

std::string Join(const std::vector<std::string>& parts);

std::string Join(const std::vector<std::string>& parts,
                 const std::string& delimiter);

template<typename Itr>
std::string Join(Itr begin, Itr end, const std::string& delimiter = " ");

/// Converts the given string to uppercase and returns the result.
std::string Upper(const std::string& str);

/// Strips the leading and trailing whitespace from the string.
std::string Strip(const std::string& str);

/// Calls the function on each subset of the specified range.
/**
 * For each subset of the elements contained in the range, this calls the
 * provided function. The provided function should take two arguments that
 * should me iterators to lists of the type of the value type of the input
 * iterators, the first one a begin iterator, the second one an end iterator.
 * @param begin the beginning of the range of elements
 * @param end one past the end of the range of elements
 */
template<typename In, typename Function>
Function ForEachSubset(In begin, In end, Function&& function);

/// Helper functions for template functions, implementation details, don't use.
namespace impl {

template<typename In, typename Val, typename Function>
void ForEachSubset(In begin, In end, std::list<Val>& stack, Function& function);

} /* namespace impl */

/// Return a uniform random number between 0 and 1.
double unif();

/// Return a uniform random integer between a and b inclusive.
/**
 * @param a the lower bound
 * @param b the upper bound
 */
int unif(double a, double b);

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args...>(args)...));
}

// Template function definitions

template<typename Itr>
std::string Join(Itr begin, Itr end, const std::string& delimiter) {
    std::string result = *begin;
    ++begin;
    for(auto itr = begin; itr != end; ++itr) {
        result += " ";
        result += *itr;
    }
    return result;
}

template<typename In, typename Function>
Function ForEachSubset(In begin, In end, Function&& function) {
    std::list<typename std::iterator_traits<In>::value_type> stack;
    impl::ForEachSubset(begin, end, stack, function);
    return function;
}

template<typename In, typename Val, typename Function>
void impl::ForEachSubset(In begin, In end, std::list<Val>& stack, Function& function) {
    if(begin == end) {
        function(stack.begin(), stack.end());
        return;
    }
    stack.push_back(*begin);
    ++begin;
    ForEachSubset(begin, end, stack, function);
    stack.pop_back();
    ForEachSubset(begin, end, stack, function);
}

}

#endif
