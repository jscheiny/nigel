/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Game.h
 * @brief Declaration of the Game class.
 */

#ifndef GAME_H
#define GAME_H

#include "Board.h"
#include "Alphabet.h"
#include "Lexicon.h"
#include "Observe.h"

#include <memory>
#include <string>
#include <vector>
#include <tuple>
#include <map>

namespace nigel {

class Move;
class Player;
class TileBag;

/// A Scrabble game
/**
 * Represents a game of Scrabble between one or more players.
 */
class Game : public Subject<std::shared_ptr<Move>> {

public:
    /// The set of built in game configurations.
    /**
     * Game configurations are 3-tuples of board, alphabet, and lexicon
     * configurations that are considered to be common groupings.
     */
    enum class Config {
        /// English Scrabble: Scrabble board, English Scrabble alphabet, TWL06
        /// lexicon.
        SCRABBLE,
        /// French Scrabble: Scrabble board, French Scrabble alphabet, ODS5
        /// lexicon.
        SCRABBLE_FRENCH,
        /// Italian Scrabble: Scrabble board, Italian alphabet, Zingarelli 2005
        /// lexicon.
        SCRABBLE_ITALIAN,
        /// English Super Scrabble: Super Scrabble board, English Super Scrabble
        /// alphabet, TWL06 lexicon.
        SUPER_SCRABBLE,
        /// Words With Friends: WWF board, English WWF alphabet, WWF lexicon.
        WWF
    };

    static std::shared_ptr<Game> FromConfig(Config config) {
        return std::make_shared<Game>(config);
    }

    static std::shared_ptr<Game> FromParts(Board::Config board_config,
                                           Alphabet::Config alphabet_config,
                                           Lexicon::Config lexicon_config) {
        return std::make_shared<Game>(board_config, alphabet_config, lexicon_config);
    }

    /// Create a Scrabble game with the given configuration.
    /**
     * By default an English Scrabble game.
     * @param config the configuration used to define the game
     */
    explicit Game(Config config = Config::SCRABBLE);

    /// Create a Scrabble game with each of the given component configurations.
    /**
     * @param board_config the config used to define the game's board
     * @param alphabet_config the config used to define the game's tilebag
     * @param lexicon_config the config used to define the game's lexicon
     */
    Game(Board::Config board_config,
         Alphabet::Config alphabet_config,
         Lexicon::Config lexicon_config);

    /// Create a Scrabble game with each of the given component setup paths.
    /**
     * @param board_setup the setup used to define the game's board
     * @param alphabet_setup the setup used to define the game's tilebag
     * @param lexicon_setup the setup used to define the game's lexicon
     */
    Game(const std::string& board_setup,
         const std::string& alphabet_setup,
         const std::string& lexicon_setup);

    Game(const std::string& path, Config config = Config::SCRABBLE);

    Game(const std::string& path,
         Board::Config board_config,
         Alphabet::Config alphabet_config,
         Lexicon::Config lexicon_config);

    Game(const std::string& path,
         const std::string& board_setup,
         const std::string& alphabet_setup,
         const std::string& lexicon_setup);

    Game(std::istream& is, Config config = Config::SCRABBLE);

    Game(std::istream& is,
         Board::Config board_config,
         Alphabet::Config alphabet_config,
         Lexicon::Config lexicon_config);

    Game(std::istream& is,
         const std::string& board_setup,
         const std::string& alphabet_setup,
         const std::string& lexicon_setup);

    /// Destroys the game and its players.
    virtual ~Game();

    std::shared_ptr<Board> GetBoard() const {
        return board_;
    }

    std::shared_ptr<TileBag> GetTileBag() const {
        return tilebag_;
    }

    std::shared_ptr<Lexicon> GetLexicon() const {
        return lexicon_;
    }

    /// Adds a player with the given name to the game.
    /**
     * The player is given the name and has a default strategy (Nigel1).
     * @param name the name of the created player
     * @return the player added to the game
     */
    Player* AddPlayer(const std::string& name);

    /// Returns the player at the given index of play.
    /**
     * The valid indices are between 0 and numPlayers() - 1 (inclusive).
     * @param index the index of the player
     * @return the player at the given index
     */
    const Player* GetPlayer(int index) const {
        return players_.at(index);
    }

    /// Returns the number of players in the game.
    /**
     * @return the number of players in the game
     */
    int NumPlayers() const {
        return players_.size();
    }

    /// Returns the currently playing player.
    const Player* OnTurnPlayer() const {
        return players_[current_turn_];
    }

    /// Removes the given player from the game.
    /**
     * If the given player is not in the game, this does nothing.
     * @param player the player to remove from the game
     */
    void RemovePlayer(Player* player) {
        players_.erase(find(players_.begin(), players_.end(), player));
    }

    /// Removes the player at the given index of play.
    /**
     * The player at the given index is destroyed. The valid indices are between
     * 0 and numPlayers() - 1 (inclusive).
     * @param index the index of the player to remove
     */
    void RemovePlayer(int index) {
        players_.erase(players_.begin() + index);
    }

    /// Moves each player one position forward in play.
    /**
     * If this is a three player game in the order player A, B, C then calling
     * this method will result in the order of play being C, A, B. If there are
     * only two players in the game, this will swap the order of the players.
     */
    void RotatePlayers();

    const Player* OtherPlayer(const Player* player) const;

    /// Returns the winning player of the game.
    /**
     * If the game is not over, then this throws an Error("Game not over!").
     * If the game is over, and there is a distinct winner, returns that player.
     * If there is no distinct winner (two or more players tied for first),
     * returns nullptr.
     * @return if the game is over, returns the winner, or nullptr if a tie
     */
    Player* Winner() const;

    /// Returns whether the game has ended or not.
    /**
     * @param returns true if the game is over, false otherwise
     */
    bool IsOver() const {
        return game_over_;
    }

    std::string GetTitle() const {
        return title_;
    }

    void SetTitle(const std::string& title) {
        title_ = title;
    }

    std::string GetDescription() const {
        return description_;
    }

    void SetDescription(const std::string& description) {
        description_ = description;
    }

    /// Resets the game to its initial state to play a new game.
    /**
     * This makes the game not over, clears all moves, clears the board and
     * refills the tile bag.
     */
    void Reset();

    /// Makes the move of the currently on-turn player.
    /**
     * This consults the currently on-turn player to get their best move with
     * their rack, and then sets the on-turn player to the next player.
     * @return the move of the on-turn player
     */
    std::shared_ptr<Move> MakeNextMove();

    std::shared_ptr<Move> LastMove() {
        if(moves_.empty())
            throw std::out_of_range("No previous moves");
        return moves_.back();
    }

    /// Plays the game to completion
    /**
     * @param printMoves set to true if the moves of the players should be
     * printed to standard out throughout the game play
     * @param printBoards set to true if the board should be printed to standard
     * out after every play
     */
    void Play(bool print_moves = true, bool print_boards = false);

    void QuickPlay() {
        Reset();
        Play(false, false);
    }

    /// Saves the game as a .gcg file at the given path
    /**
     * If the file could not be opened, throws an OpenError.
     *
     */
    void SaveGcg(const std::string& path);

    /// Saves the game in the .gcg file format to the given stream
    void SaveGcg(std::ostream& os);

private:
    struct ParseData {
        ParseData(std::shared_ptr<Alphabet> alphabet_) : alphabet(alphabet_) {}

        std::shared_ptr<Alphabet> alphabet;
        std::string incomplete_rack;
        std::map<int, std::string> specified_racks;
    };

    /// A tuple of board, alphabet, and lexicon configurations
    using ConfigTuple = std::tuple<Board::Config,
                                   Alphabet::Config,
                                   Lexicon::Config>;

    /// Mapping of the Config enum to tuples of configs.
    static const std::map<Config, ConfigTuple> kConfigMap;

    using PragmaParseMemFn = void (Game::*)(std::vector<std::string>&, ParseData*);

    static const std::map<std::string, PragmaParseMemFn> kPragmaParsers;

    /// The number of consecutive skips by all players that forces a game end.
    static const int kNumConsecSkipsForceEnd;

    /// The tilebag used for the game.
    std::shared_ptr<TileBag> tilebag_;
    /// The lexicon used for the game.
    std::shared_ptr<Lexicon> lexicon_;
    /// The board used for the game.
    std::shared_ptr<Board> board_;

    /// The players in the game.
    std::vector<Player*> players_;
    /// All moves made in the game.
    std::vector<std::shared_ptr<Move>> moves_;
    /// The index of the on-turn player.
    int current_turn_;
    /// Indicates whether the game is over or not.
    bool game_over_;
    /// The number of consecutive passes or swaps made by the players.
    int consec_skips_;

    std::string title_;
    std::string description_;
    std::string idAuth_;
    std::string uniqId_;

    /// Construct a game from a configuration tuple.
    Game(const ConfigTuple& tup);

    Game(const std::string& path, const ConfigTuple& tup);

    Game(std::istream& is, const ConfigTuple& tup);

    void ReadGcg(std::istream& is,
                 const std::string& board_setup,
                 const std::string& alphabet_setup,
                 const std::string& lexicon_setup);

    void ParsePragma(const std::string& line, ParseData* data);

    void ParsePlayerPragma(std::vector<std::string>& parts, ParseData* data);
    void ParseRackPragma(std::vector<std::string>& parts, ParseData* data);
    void ParseTitlePragma(std::vector<std::string>& parts, ParseData* data);
    void ParseDescriptionPragma(std::vector<std::string>& parts, ParseData* data);
    void ParseIncompletePragma(std::vector<std::string>& parts, ParseData* data);
    void ParseNotePragma(std::vector<std::string>& parts, ParseData* data);
    void ParseIdPragma(std::vector<std::string>& parts, ParseData* data);

    void ParseEvent(const std::string& line, ParseData* data);

    Player* GetPlayerByNickname(const std::string& nickname);

    int GetPlayerIndex(const Player* player) const;

    /// Handle the end condition of a forced end by too many consecutive skips.
    void HandleConsecSkipsEndGame();
    /// Handle the normal end game condition.
    void HandleNormalEndGame();

};

}

#endif
