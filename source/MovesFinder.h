/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file MovesFinder.h
 * @brief Declaration of the MovesFinder class.
 */

#ifndef MOVES_FINDER_H
#define MOVES_FINDER_H

#include "Dawg.h"
#include "Observe.h"
#include "TransposedBoard.h"

#include <memory>
#include <vector>
#include <set>
#include <map>

#include <sstream>

namespace nigel {

class Rack;
class Move;
class Board;
class Lexicon;
class Alphabet;
class ReadableBoard;

class MovesFinder;

class Directional {

public:

    friend MovesFinder;

private:

    Directional(const ReadableBoard& board_,
                std::shared_ptr<Alphabet> alphabet,
                std::shared_ptr<Lexicon> lexicon_,
                bool horiz_);

    virtual ~Directional() {}

    void FindMoves(Rack rack, std::vector<std::shared_ptr<Move>>* moves) const;

    class CrossWordData;

    bool horiz_;

    const ReadableBoard& board_;
    std::shared_ptr<Alphabet> alphabet_;
    std::shared_ptr<Lexicon> lexicon_;

    mutable std::vector<std::shared_ptr<Move>>* moves_;

    using WordLengthStarts = std::map<int, std::vector<Coord>>; // indexed by row
    mutable std::map<int, WordLengthStarts> incr_start_pos_;
    std::vector<bool> effected_rows_;
    mutable int prev_rack_size_;

    mutable std::vector< std::vector<CrossWordData> > crosswords_;
    mutable std::map< int, std::vector<Coord> > start_positions_;
    mutable int max_playable_word_;

    void MovesAt(const Coord& start,
                 const Coord& curr,
                 const Dawg::Node* root,
                 const Rack& leave,
                 const Rack& placed,
                 const Rack& tilesOnBoard,
                 int score,
                 int perpScore,
                 int wordMultiplier,
                 int wordLength,
                 const std::string& prefix) const;

    void MovesAtRecur(const Coord& start,
                      const Coord& curr,
                      const Dawg::Node* root,
                      Rack leave,
                      Rack placed,
                      Rack tilesOnBoard,
                      int score,
                      int perpScore,
                      int wordMultiplier,
                      int wordLength,
                      const std::string& prefix,
                      const Tile& nextPlayed,
                      const CrossWordData& cwd,
                      int currWordMult,
                      int currLetterMult) const;

    MoveCoord CorrectCoord(const Coord& curr) const;

    void SetupCrossWordData();
    void ClearCrossWordData() const;

    // Metaboard update methods
    void UpdateMetaBoard(const std::vector<Coord>& coords);
    bool IsHorizontalMove(const std::vector<Coord>& coords);

    void GenerateEffectedRows(const std::vector<Coord>& coords);
    void MarkEffectedRow(int row);

    void UpdateCrossWordData(const std::vector<Coord>& coords);

    void GenerateUpdatedStartCoords(int rack_size) const;
    void GenerateInitialStartCoords(int word_length, int rack_size) const;
    void GenerateRowStartCoords(int row, int word_length, int rack_size) const;

    // Utilities for start coords
    int CountEmptySquares(int row, int col, int word_length,
                          bool* perp_words_valid, bool* adjacent) const;
    bool IsLeftSquareEmpty(int row, int col) const;
    bool IsRightSquareEmpty(int row, int col, int word_length) const;
    bool IsAboveSquareFull(int row, int col) const;
    bool IsBelowSquareFull(int row, int col) const;

    void PrepareForMove(int rack_size) const;
};

class MovesFinder {

private:
    friend Board;

    MovesFinder(const ReadableBoard& board,
                std::shared_ptr<Alphabet> alphabet,
                std::shared_ptr<Lexicon> lexicon);

    void FindMoves(const Rack& rack,
                   std::vector<std::shared_ptr<Move>>* moves) const;

    void UpdateLocations(const std::vector<Coord>& coords);

    const ReadableBoard& board_;
    TransposedBoard transposed_;
    Directional horizontal_;
    Directional vertical_;

};

class Directional::CrossWordData {

public:
    CrossWordData() : perp_letters_score_(-1), pre_score_(0), post_score_(0) {}

    void Clear();

    void Update(const ReadableBoard& board, int row, int col,
                std::shared_ptr<Lexicon> lexicon,
                const std::set<char>& alphabet);

    void UpdatePreWordScore(const std::string& new_pre_word, int new_pre_score,
                            std::shared_ptr<Lexicon> lexicon,
                            const std::set<char>& alphabet);

    void UpdatePostWordScore(const std::string& new_post_word, int new_post_score,
                             std::shared_ptr<Lexicon> lexicon,
                             const std::set<char>& alphabet);

    int GetPerpLettersScore() const {
        return perp_letters_score_;
    }

    bool AnyTilesValid() const {
        return valid_letters_.size() != 0;
    }

    bool PerpWordExists() const {
        return perp_letters_score_ != -1;
    }

    const std::set<char>& GetValidLetters() const { return valid_letters_; }

    int GetPreScore() const { return pre_score_; }
    int GetPostScore() const { return post_score_; }

    const std::string& GetPreWord() const {  return pre_word_; }
    const std::string& GetPostWord() const { return post_word_; }

private:
    int perp_letters_score_;
    std::set<char> valid_letters_;
    int pre_score_;
    std::string pre_word_;
    int post_score_;
    std::string post_word_;

    void UpdateValidLetters(std::shared_ptr<Lexicon> lexicon,
                            const std::set<char>& alphabet);

};

}

#endif
