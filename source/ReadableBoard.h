/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file ReadableBoard.h
 * @brief Declaration of the ReadableBoard class.
 */

#ifndef READABLE_BOARD_H
#define READABLE_BOARD_H

#include "Coordinate.h"
#include "Tile.h"

#include <iosfwd>

namespace nigel {

class Move;

class ReadableBoard {

public:
    virtual bool IsEmpty() const = 0;
    virtual int GetRows() const = 0;
    virtual int GetColumns() const = 0;
    virtual int GetStartRow() const = 0;
    virtual int GetStartColumn() const = 0;
    virtual bool OutOfBounds(int row, int col) const = 0;
    virtual bool OutOfBounds(const Coord& coord) const = 0;
    virtual const Tile& operator() (int row, int col) const = 0;
    virtual const Tile& operator() (const Coord& coord) const = 0;
    virtual const Tile& TileAt(int row, int col) const = 0;
    virtual const Tile& TileAt(const Coord& coord) const = 0;
    virtual char Prem(int row, int col) const = 0;
    virtual char PremiumAt(int row, int col) const = 0;
    void Print(std::ostream& os) const;

};

}

#endif
