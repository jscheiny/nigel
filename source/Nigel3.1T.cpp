/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Nigel3.1T.h"
#include "Player.h"
#include "TileBag.h"

#include <iostream>
#include <future>

using namespace nigel;
using namespace std;

Nigel31T::Nigel31T(int sample_size, int num_threads)
    : Nigel<3,1>(sample_size)
    , num_threads_(num_threads) {
}

double Nigel31T::Utility(Rack& leave) {
    if(GetPlayer()->GetTileBag()->IsEmpty())
        return -2 * leave.Score();

    int num_to_sample = 7 - leave.size();
    if(GetSampleSpace().size() < num_to_sample)
        return Nigel22Resources::Instance().ComputeUtility(leave);

    string leaveStr = leave.ToString();
    vector< future< pair<double, int> > > threads;
    for(int i = 0; i < num_threads_ - 1; i++) {
        threads.push_back(async(launch::async,
            bind(&Nigel31T::PerformSamples, this, ref(leaveStr),
                 GetSampleSize() / num_threads_)));
    }

    auto total = PerformSamples(leaveStr, GetSampleSize() / num_threads_
                                        + GetSampleSize() % num_threads_);
    for(auto& thread : threads) {
        auto result = thread.get();
        total.first += result.first;
        total.second += result.second;
    }

    if(total.second != GetSampleSize())
        cout << total.second << "\t" << GetSampleSize() << endl;
    return total.first / total.second;
}

pair<double, int> Nigel31T::PerformSamples(const string& leaveStr, int num) {
    double total = 0;
    int num_to_sample = 7 - leaveStr.size();
    for(int i = 0; i < num; i++) {
        string sample(leaveStr);
        sample += SampleRemaining(num_to_sample);
        sort(sample.begin(), sample.end());
        total += SampleUtility(sample);
    }
    return make_pair(total, num);
}
