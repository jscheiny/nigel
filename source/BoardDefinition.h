#ifndef BOARD_DEFINITION_H
#define BOARD_DEFINITION_H

#include "Utility.h"

namespace nigel {

class Board;
class Tile;

class BoardDefinition {

public:
    static const BoardDefinition SCRABBLE;
    static const BoardDefinition SUPER_SCRABBLE;
    static const BoardDefinition WWF;

    BoardDefinition(const std::string& name,
                    int start_row, int start_col,
                    const std::vector<std::string>& board);

    friend class Board;

private:

    std::string name_;
    int start_row_;
    int start_col_;
    std::size_t rows_;
    std::size_t cols_;
    std::vector<std::vector<char>> premium_;
    std::vector<std::vector<Tile>> board_;

};

struct BoardDefinitionError : Error {
    BoardDefinitionError(const std::string& msg) : Error(msg) {}

    static BoardDefinitionError Empty() {
        return {"Cannot create an empty board."};
    }
};

}

#endif
