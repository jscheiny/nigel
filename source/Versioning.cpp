/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Versioning.h"

using namespace std;

namespace nigel {

namespace impl {

RuntimeVersions& RuntimeVersions::Instance() {
    static RuntimeVersions instance;
    return instance;
}

unique_ptr<Strategy> RuntimeVersions::MakeVersion(version_no_t major,
                                              version_no_t minor) {
    auto& inst = Instance();
    auto itr = inst.versions_.find(make_tuple(major, minor));
    if (itr == inst.versions_.end()) {
        throw VersionError(major, minor);
    }
    return itr->second();
}

}

VersionError::VersionError(version_no_t major, version_no_t minor)
      : Error("The version number " + to_string(major) + "." + to_string(minor) +
              "does not exist.") {
}

VersionError::VersionError(const string& version)
      : Error("The version string " + version + " is invalid.") {

}

unique_ptr<Strategy> MakeStrategy(version_no_t major, version_no_t minor) {
    return impl::RuntimeVersions::MakeVersion(major, minor);
}

unique_ptr<Strategy> MakeStrategy(const string& version) {
    auto dot_index = version.find(".");
    if(dot_index == string::npos) {
        throw VersionError(version);
    }
    version_no_t major, minor;
    try {
        major = ConvertToInt(version.substr(0, dot_index));
        minor = ConvertToInt(version.substr(dot_index));
    } catch (ConvertError&) {
        throw VersionError(version);
    }
    return MakeStrategy(major, minor);
}

}
