/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Human.h
 * @brief Declaration of the Human class.
 */

#ifndef HUMAN_H
#define HUMAN_H

#include "Strategy.h"

#include <iostream>

namespace nigel {

struct MoveCoord;

class Human : public Strategy {

public:
    Human(std::ostream& out = std::cout,
          std::istream& in = std::cin);

    void Preprocess(std::vector<std::shared_ptr<Move>>& moves) override { }

    void Postprocess(std::vector<std::shared_ptr<Move>>& moves) override;

    double Utility(Rack & leave) override { return 0.0; }

private:
    std::ostream& out_;
    std::istream& in_;

    std::shared_ptr<Move> FindMove(const std::string& word,
                                   const MoveCoord& coord,
                                   std::vector<std::shared_ptr<Move>>& moves);

    std::shared_ptr<Move> FindSwap(std::string swapped,
                                   std::vector<std::shared_ptr<Move>>& moves);

};

}

#endif
