/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file TileBag.h
 * @brief Declaration of the TileBag class.
 */

#ifndef TILE_BAG_H
#define TILE_BAG_H

#include "Alphabet.h"
#include "Utility.h"

#include <random>
#include <iosfwd>
#include <string>

namespace nigel {

class Tile;
class Rack;
class Board;

/// A bag from which tiles can be randomly drawn.
/**
 * A tile bag is based on an alphabet, and is filled according to the score and
 * frequency of the letters in the alphabet. The main capabilties of bags are
 * to draw and swap tiles, refill the bag, and fill the bag excluding the
 * contents of a Board.
 */
class TileBag {

public:

    /// Create a full tile bag using a given alphabet configuration.
    /**
     * By default using the English Scrabble alphabet.
     * @param config the configuration used to define the alphabet
     */
    explicit TileBag(Alphabet::Config config = Alphabet::Config::ENGLISH);

    /// Create a full tile bag from the given alphabet configuration stream.
    /**
     * If the stream contains invalid data, throws an InvalidDataError.
     * @param is the input stream containing the alphabet configuration
     */
    explicit TileBag(std::istream& is);

    /// Create a full tile bag from the given alphabet configuration file path.
    /**
     * If the file at the path cannot be opened throws an OpenError. If the file
     * contains invalid data, throws an InvalidDataError.
     * @param path the path of the configuration file
     */
    explicit TileBag(const std::string& path);

    /// Create a full tile bag using a given alphabet.
    /**
     * @param alphabet_ the alphabet used to fill the tile bag
     */
    explicit TileBag(std::shared_ptr<Alphabet> alphabet);

    /// Create a tile bag using a given alphabet, filled based on on the board.
    /**
     * This fills the tilebag, excluding the tiles that are on the board. This
     * is used to set up a tile bag that represents the current state of the
     * game.
     * @param alphabet_ the alphabet used to fill the tile bag
     * @param board the board whose tiles should be excluded from the bag
     */
    TileBag(std::shared_ptr<Alphabet> alphabet, const Board& board);

    // Defaulted copy/move construction/assignment and destruction

    TileBag(const TileBag&) = default;
    TileBag(TileBag&&) = default;
    ~TileBag() = default;
    TileBag& operator= (const TileBag&) = default;
    TileBag& operator= (TileBag&&) = default;

    /// Draws the specified number of tiles from the bag.
    /**
     * If there aren't enough tiles left in the bag, simply returns the rest
     * of the bag.
     * @param number the number of tiles to draw
     * @return the drawn tiles (the which maybe less than number if the bag is
     * almost empty)
     */
    Rack Draw(int number);

    /// Swaps the specified tiles into the bag.
    /**
     * Note that this does not check to see if the bag has the required to size
     * to swap in a Scrabble game, that must be done by the caller. The swap
     * method is to first draw the required tiles, and then put the swapped
     * tiles into the bag. Thus swapping into an empty bag will return an empty
     * vector, and the bag's contents will become the swapped tiles.
     * @param toSwap the tiles to swap into the bag
     * @return the tiles drawn to replace the swapped tiles
     */
    Rack Swap(const Rack& to_swap);

    /// Refills the bag to its full contents.
    void Refill();

    /// Refills the bag, excluding the tiles on the board.
    /**
     * The bag is refilled based on this tile bag's alphabet, but will not
     * include tiles placed on the board.
     * @param board the board whose tiles should be excluded from the refilled
     * bag
     */
    void RefillFromBoard(const Board& board);

    /// Shuffles the contents of the tile bag.
    void Shuffle();

    /// Returns true if the bag is empty, false otherwise.
    bool IsEmpty() const {
        return bag_.empty();
    }

    /// Returns the number of tiles left in the bag.
    int Size() const {
        return bag_.size();
    }

    /// Returns the number of tiles in the full bag.
    int GetFullSize() const {
        return full_size_;
    }

    /// Returns this tile bag's alphabet.
    std::shared_ptr<Alphabet> GetAlphabet() const {
        return alphabet_;
    }

private:
    /// Compute the number of tiles in the full bag based on the alphabet
    void ComputeFullBagSize();

    /// The alphabet used to generate the tile bag.
    std::shared_ptr<Alphabet> alphabet_;
    /// The tiles left in the bag.
    std::vector<Tile> bag_;
    /// A random number generator used for shuffling the bag.
    std::default_random_engine random_engine_;
    /// The number of tiles in the full bag.
    int full_size_;
};

}

#endif
