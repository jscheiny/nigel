/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Nigel2.2.h"
#include "Player.h"
#include "Rack.h"

#include <fstream>

using namespace nigel;
using namespace std;

double Nigel<2,2>::Utility(Rack& leave) {
    if(GetPlayer()->GetTileBag()->IsEmpty())
        return -2 * leave.Score();

    return Nigel22Resources::Instance().ComputeUtility(leave);
}

const int Nigel22Resources::kCountThreshold = 100;
const LinearFunction Nigel22Resources::kLeave4Fn{1.3191, -11.502};
const LinearFunction Nigel22Resources::kLeave5Fn{1.2885, -10.312};
const LinearFunction Nigel22Resources::kLeave6Fn{1.3211, -11.143};
const double Nigel22Resources::kAveragePlayScore = 33.9522396965;

Nigel22Resources::Nigel22Resources() {
    ReadUtility(NIGEL_RESOURCE("utility/1.txt"));
    ReadUtility(NIGEL_RESOURCE("utility/2.txt"));
    ReadUtility(NIGEL_RESOURCE("utility/3.txt"));
    ReadUtility(NIGEL_RESOURCE("utility/4.txt"));
    ReadUtility(NIGEL_RESOURCE("utility/5.txt"));
    ReadUtility(NIGEL_RESOURCE("utility/6.txt"));
}

double Nigel22Resources::ComputeUtility(const Rack& leave) {
    if(leave.empty())
        return kAveragePlayScore;

    if(leave.size() >= 7)
        return 0.0;

    string leave_str = leave.ToString();
    if(leave.size() <= 3)
        return utility_map_[leave_str];

    return ComputeNLetterUtil(leave_str);
}

double Nigel22Resources::ComputeNLetterUtil(const string& leave) {
    auto itr = utility_map_.find(leave);
    if(itr != utility_map_.end()) {
        return itr->second;
    }

    double avg = 0;
    int count = 0;

    for(int i = 0; i < leave.size(); i++) {
        string substr = leave.substr(0, i) + leave.substr(i+1);
        avg += ComputeNLetterUtil(substr);
        count ++;
    }
    avg /= count;
    if(leave.size() == 4) {
        return kLeave4Fn(avg);
    } else if(leave.size() == 5) {
        return kLeave5Fn(avg);
    } else {
        return kLeave6Fn(avg);
    }
}

void Nigel22Resources::ReadUtility(const std::string& path) {
    string line;
    ifstream fin(path);
    while(getline(fin, line)) {
        vector<string> parts = Split(line);
        string key = parts[0];
        double average = atof(parts[1].c_str());
        int count = atoi(parts[3].c_str());
        if(count >= kCountThreshold) {
            utility_map_.emplace(key, average);
        }
    }
}
