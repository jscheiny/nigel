/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file RunningStats.h
 * @brief Declaration of the RunningStats class.
 */

#ifndef RUNNING_STATS_H
#define RUNNING_STATS_H

#include <cmath>
#include <iosfwd>

namespace nigel {
namespace analyze {

class RunningStats {

public:
    void Add(int value);

    int GetNumber() const {
        return number_;
    }

    double GetMean() const {
        return mean_;
    }

    double GetVariance() const {
        return (number_ > 1) ? variance_ / (number_ - 1) : 0;
    }

    double GetStddev() const {
        return sqrt(GetVariance());
    }

private:
    int number_ = 0;
    double mean_ = 0;
    double variance_ = 0;

};

std::ostream& operator<< (std::ostream& os, const RunningStats& stats);

}
}

#endif
