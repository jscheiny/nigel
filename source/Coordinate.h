/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Coordinate.h
 * @brief Declaration of the Coord and MoveCoord classes.
 */

#ifndef COORDINATE_H
#define COORDINATE_H

#include <iosfwd>
#include <string>

namespace nigel {

// A coordinate with a row and a column.
struct Coord {
    /// The row value
    int row;
    /// The column value
    int col;

    /// Construct a coordinate with the given row and column, by default 0.
    /**
     * @param row the row of the coordinate
     * @param col the column of the coordinate
     */
    Coord(int row_ = 0, int col_ = 0);

    Coord Up(int amount = 1) const;

    Coord Down(int amount = 1) const;

    Coord Left(int amount = 1) const;

    Coord Right(int amount = 1) const;

    Coord Transpose() const;

    /// Compares coordinates first by row, then by column.
    /**
     * @param other the coordinate to compare this coordinate to
     * @return true if this coordinate is 'less than' the other coordinate
     */
    bool operator< (const Coord& other) const;

    /// Returns true if coordinates have the same row and column.
    /**
     * @param other the coordinate to compare this coordinate to
     * @return true if the two coordinates are equal
     */
    bool operator== (const Coord& other) const;

    /// Returns true if the coordinates differ in their rows or columns
    /**
     * @param other the coordinate to compare this coordinate to
     * @param true if the two coordinates are unequal
     */
    bool operator!= (const Coord& other) const;
};

/// Print a coordinate to the output stream in the format (row, col)
std::ostream& operator<< (std::ostream& os, const Coord& c);

/// A coordinate with a row, column and horizontal or vertical orientation.
/**
 * A move coordinate provides the additional functionality of being able to
 * convert to and from a string representation of a Scrabble move. A Scrabble
 * move is represented as a column letter and a row number. If the move is
 * vertical, the column letter comes first, if the move is horizontal, the row
 * number comes first.
 */
struct MoveCoord : public Coord {
    /// The alphabet which defines the column names for the coordinate.
    const static std::string kAlphabet;
    /// Whether this coordinate is horizontal.
    bool horiz;

    /// Creates a move coordinate that is by default horizontal at (0, 0).
    /**
     * @param row the row of the move
     * @param col the column of the move
     * @param horiz true if horizontal, false if vertical
     */
    MoveCoord(int row_ = 0, int col_ = 0, bool horiz_ = true);

    /// Creates a move coordinate from a coordinate string.
    /**
     * @param coordStr the Scrabble coordinate string
     */
    explicit MoveCoord(const std::string& coord_str);

    /// Sets the value of this move coordinate via a coordinate string.
    /**
     * @param coordStr the Scrabble coordinate string
     */
    void SetWithCoordStr(const std::string& coord_str);

    /// Returns the Scrabble coordinate string representation of this coordinate.
    /**
     * @return the Scrabble coordinate string for this coordinate.
     */
    std::string ToString() const;

    /// Returns the next coordinate after this coordinate.
    /**
     * If this is is a horizontal coordinate this returns the same coordinate
     * with the column increased by one. If this is a vertical coordinate this
     * returns the same coordinate with the row increased by one.
     * @return the next coordinate after this coordinate.
     */
    MoveCoord Next() const;

    /// Compares coordinates first by row, then column, then orientation.
    /**
     * @param coord the coordinate to compare this one to
     * @return true if this coordinate is 'less than' the other coordinate
     */
    bool operator< (const MoveCoord& other) const;

    /// Returns true if coordinates have the same row, column, and orientation.
    /**
     * @param other the coordinate to compare this coordinate to
     * @return true if the two coordinates are equal
     */
    bool operator== (const MoveCoord& other) const;

    /// Returns true if the coordinates differ in their rows, columns, or orientations.
    /**
     * @param other the coordinate to compare this coordinate to
     * @param true if the two coordinates are unequal
     */
    bool operator!= (const MoveCoord& other) const;
};

/// Print a move coordinate to the output stream as a standard coordinate string.
std::ostream& operator<< (std::ostream& os, const MoveCoord& c);

// Inline Coord functions

inline Coord::Coord(int row_, int col_) : row(row_), col(col_) {};

inline Coord Coord::Up(int amount) const {
    return Coord(row - amount, col);
}

inline Coord Coord::Down(int amount) const {
    return Coord(row + amount, col);
}

inline Coord Coord::Left(int amount) const {
    return Coord(row, col - amount);
}

inline Coord Coord::Right(int amount) const {
    return Coord(row, col + amount);
}

inline Coord Coord::Transpose() const {
    return Coord(col, row);
}

// Inline MoveCoord functions

inline MoveCoord::MoveCoord(int row_, int col_, bool horiz_) :
    Coord(row_,col_), horiz(horiz_) {}

inline MoveCoord MoveCoord::Next() const {
    return MoveCoord(row + (horiz ? 0 : 1),
                     col + (horiz ? 1 : 0),
                     horiz);
}

}

#endif
