/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Alphabet.h
 * @brief Declaration of the Alphabet class.
 */

#ifndef ALPHABET_H
#define ALPHABET_H

#include "Tile.h"
#include "Utility.h"
#include "WeakCache.h"

#include <memory>
#include <iosfwd>
#include <string>
#include <map>
#include <set>

namespace nigel {

class Rack;
class Board;

/// An alphabet that is used to build a tile bag.
/**
 * A tile bag defines a set of letters and their associated frequencies and
 * scores. An alphabet is not mutable and only a single alphabet object should
 * realistically be created for a given alphabet. Alphabets can be defined by
 * a set of built in alphabet files which have been abstracted into the Config
 * enumeration.
 */
class Alphabet {

public:
    /// The set of built in alphabet configurations.
    /**
     * Defines the set of built in alphabet definitions that can be used to
     * construct an alphabet.
     */
    enum class Config {
        /// Used to specify the English Scrabble tile set.
        ENGLISH,
        /// Used to specify the English Super Scrabble tile set.
        ENGLISH_SS,
        /// Used to specify the English Words with Friends tile set.
        ENGLISH_WWF,
        /// Used to specify the French Scrabble tile set.
        FRENCH,
        /// Used to specify the Italian Scrabble tile set.
        ITALIAN
    };

    struct ScoreFreq {
        ScoreFreq(int score_, int freq_) : score(score_), freq(freq_) {}

        int score;
        int freq;
    };

    static std::shared_ptr<Alphabet> Create(Config config);

    /// Creates an alphabet with one of the built in alphabet configurations.
    /**
     * By default an English Scrabble alphabet.
     * @param config the configuration used to define the alphabet
     */
    explicit Alphabet(Config config = Config::ENGLISH);

    /// Creates an alphabet from the given alphabet configuration in a stream.
    /**
     * If the stream contains invalid data, throws an InvalidDataError.
     * @param is the input stream containing the alphabet configuration
     */
    explicit Alphabet(std::istream& is);

    /// Creates an alphabet by reading the alphabet configuration from a file.
    /**
     * If the file at the path cannot be opened throws an OpenError. If the file
     * contains invalid data, throws an InvalidDataError.
     * @param path the path of the configuration file
     */
    explicit Alphabet(const std::string& path);

    /// Creates an alphabet from the given alphabet data.
    /**
     * @param scoreFreqMap_ a map in which the keys are the alphabet letters and
     * whose values are the associated (score, frequency) 2-tuple.
     * @param alphabetName_ the name of the alphabet
     * @param boardName_ the name of the suggested board for this alphabet
     */
    // Alphabet(const std::map<char, ScoreFreq>& score_freq_map,
    //          const std::string& alphabet_name = std::string(),
    //          const std::string& board_name = std::string());

    // Default copy and move constructors and assignment.

    Alphabet(const Alphabet&) = default;
    Alphabet(Alphabet&&) = default;
    Alphabet& operator= (const Alphabet&) = default;
    Alphabet& operator= (Alphabet&&) = default;

    // Default destructor

    ~Alphabet() = default;

    /// Returns whether the given letter is a letter in this alphabet.
    /**
     * @param letter the letter to test
     * @return true if the letter is in this alphabet's letter set, false
     * otherwise
     */
    bool ContainsLetter(char letter) const {
        return letters_.count(letter) != 0;
    }

    /// Returns the score of the tile with the given letter.
    /**
     * @param letter the letter to get the score of
     * @param the score of the tile with the given letter
     */
    int TileScore(char letter) const {
        return score_freq_map_.at(letter).score;
    }

    /// Returns the frequency of the tile with the given letter.
    /**
     * @param letter the letter to get the frequency of
     * @param the frequency of the tile with the given letter
     */
    int TileFrequency(char letter) const {
        return score_freq_map_.at(letter).freq;
    }

    /// Creates a tile for the given letter.
    /**
     * @param letter the letter to get the tile of
     * @return the tile with the given letter and its score
     */
    Tile CreateTile(char letter) const {
        return Tile(letter, TileScore(letter));
    }

    Tile CreateBlank() const {
        return Tile(blank_character_, 0);
    }

    /// Returns the set of letters in the alphabet.
    /**
     * @return the set of valid letters in the alphabet
     */
    const std::set<char>& GetLetters() const {
        return letters_;
    }

    char GetBlankCharacter() const {
        return blank_character_;
    }

    /// Returns the name of the alphabet if defined in the configuration.
    /**
     * If no alphabet name was defined returns an empty string.
     * @return the name of the alphabet
     */
    std::string GetName() {
        return name_;
    }

    /// Returns the name of the suggested board for use with this alphabet.
    /**
     * This may be defined in the configuration. If it is not, returns an empty
     * string.
     * @return the name of the associated board with this alphabet
     */
    std::string GetBoardName() {
        return board_name_;
    }

    Rack RemainingTiles(const Board& board, const Rack& rack) const;

    static std::string GetConfigPath(Config config) {
        return kConfigPathMap.at(config);
    }

private:
    /// Mapping of the Config enum to paths of the resource files.
    static const std::map<Config, std::string> kConfigPathMap;

    static WeakCache<Config, Alphabet> alphabetCache;

    /// Map from characters to tile scores and frequencies.
    std::map<char, ScoreFreq> score_freq_map_;
    /// Set of valid letters in the alphabet.
    std::set<char> letters_;
    /// The character that represents blank tiles in this alphabet.
    char blank_character_;
    /// The name of the alphabet.
    std::string name_;
    /// The nameof the suggested board for use with this alphabet.
    std::string board_name_;

    void ReadFromFile(const std::string& path);
    void ReadFromFile(std::istream& is);
};

}

#endif
