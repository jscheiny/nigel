/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file CompositeStrategy.h
 * @brief Declaration and implementation of CompositeStrategy class.
 */

#ifndef COMPOSITE_STRATEGY_H
#define COMPOSITE_STRATEGY_H

#include "Player.h"
#include "Strategy.h"

namespace nigel {

struct TileBagFull {
    bool operator() (Player* player) const {
        auto bag = player->GetTileBag();
        return bag->Size() == bag->GetFullSize();
    }
};

struct TileBagEmpty {
    bool operator() (Player* player) const {
        return player->GetTileBag()->IsEmpty();
    }
};

template<typename IntPred> struct TileBagRemaining {
    bool operator() (Player* player) {
        return predicate(player->GetTileBag()->Size());
    }
private:
    IntPred predicate;
};

template<typename IntPred> struct TileBagUsed {
    bool operator() (Player* player) {
        auto bag = player->GetTileBag();
        return predicate(bag->GetFullSize() - bag->Size());
    }
private:
    IntPred predicate;
};

template<int value> struct GT {
    bool operator() (int test) const { return test > value; }
};

template<int value> struct LT {
    bool operator() (int test) const { return test < value; }
};

template<int value> struct GTE {
    bool operator() (int test) const { return test >= value; }
};

template<int value> struct LTE {
    bool operator() (int test) const { return test <= value; }
};

template<int value> struct EQ {
    bool operator() (int test) const { return test == value; }
};

template<int value> struct NEQ {
    bool operator() (int test) const { return test != value; }
};

template<typename StratList, typename PredList> class PredicateEvaluator;

template<typename StratList, typename PredList>
class CompositeStrategy : public Strategy {
    static_assert(StratList::length == PredList::length + 1,
        "The number of strategies should be 1 more than the number of predicates.");
    static_assert(StratList::length >= 2,
        "The number of strategies must be at least 2.");

public:
    template<typename SL, typename PL> friend class PredicateEvaluator;

    explicit CompositeStrategy(Player* player);

    void Preprocess(std::vector<std::shared_ptr<Move>>& moves) override;
    void Postprocess(std::vector<std::shared_ptr<Move>>& moves) override;
    double Utility(Rack& leave) override;

private:
    Player* player_;
    PredicateEvaluator<StratList, PredList> evaluator_;
    Strategy* current_;

    template<typename Strat>
    Strategy* Create() {
        return new Strat(player_);
    }
};

template<typename StratList, typename PredList>
CompositeStrategy<StratList, PredList>::CompositeStrategy(Player* player) :
Strategy(player), player_(player), evaluator_(this, player), current_(nullptr) {

}

template<typename StratList, typename PredList>
void CompositeStrategy<StratList, PredList>::Preprocess(std::vector<std::shared_ptr<Move>>& moves) {
    current_ = evaluator_.strategy();
    if(current_) {
        current_->Preprocess(moves);
    }
}

template<typename StratList, typename PredList>
void CompositeStrategy<StratList, PredList>::Postprocess(std::vector<std::shared_ptr<Move>>& moves) {
    if(current_) {
        current_->Postprocess(moves);
    }
}

template<typename StratList, typename PredList>
double CompositeStrategy<StratList, PredList>::Utility(Rack& leave) {
    if(current_) {
        return current_->Utility(leave);
    } else {
        return 0.0;
    }
}

// Template meta programming crap

template<typename Head, typename... Tail>
struct TypeList {
    enum { length = sizeof...(Tail) + 1 };
    using HeadType = Head;
    using TailType = TypeList<Tail...>;
};

template<typename Head>
struct TypeList<Head> {
    enum { length = 1 };
    using HeadType = Head;
};

template<typename Head, typename... Tail>
using Strategies = TypeList<Head, Tail...>;
template<typename Head, typename... Tail>
using Predicates = TypeList<Head, Tail...>;

// Predicate evaluator (More TMP crap)

template<typename StratList, typename PredList>
class PredicateEvaluator {

public:
    static_assert(StratList::length == PredList::length + 1,
        "The number of strategies should be 1 more than the number of predicates.");

    template<typename SuperStratList, typename SuperPredList>
    PredicateEvaluator(CompositeStrategy<SuperStratList, SuperPredList>* composite,
                       Player* player) :
        player_(player), strategy_(composite->template Create<StratType>()), predicate_(),
        subevaluator_(composite, player_) {}

    ~PredicateEvaluator() {
        delete strategy_;
    }

    Strategy* strategy() {
        return predicate(player_) ? strategy_ : subevaluator_();
    }

private:
    using StratType = typename StratList::HeadType;

    Player* player_;
    Strategy* strategy_;
    typename PredList::HeadType predicate_;

    PredicateEvaluator<typename StratList::TailType, typename PredList::TailType>
        subevaluator_;
};

// Predicate evaluator (base case)

template<typename Strat1, typename Strat2, typename Predicate>
class PredicateEvaluator<Strategies<Strat1, Strat2>, Predicates<Predicate>> {

public:

    template<typename SuperStratList, typename SuperPredList>
    PredicateEvaluator(CompositeStrategy<SuperStratList, SuperPredList>* composite,
                       Player* player) :
        player_(player),
        strategy1_(composite->template create<Strat1>()),
        strategy2_(composite->template create<Strat2>()) {}

    ~PredicateEvaluator() {
        delete strategy1_;
        delete strategy2_;
    }

    Strategy* strategy() {
        return predicate_(player_) ? strategy1_ : strategy2_;
    }

private:
    Player* player_;
    Strategy* strategy1_;
    Strategy* strategy2_;
    Predicate predicate_;

};

}


#endif
