/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file CLI.h
 * @brief Declaration of the CommandLineInterface class.
 */

#ifndef CLI_H
#define CLI_H

#include "Board.h"
#include "Lexicon.h"
#include "TileBag.h"

#include <functional>
#include <string>
#include <set>
#include <map>

namespace nigel {

class Player;


class CommandLineInterface {

public:
    CommandLineInterface(int argc, char* argv[]);

private:
    class CommandArg;

    using CliMemberFunctionPtr = void (CommandLineInterface::*)();

    static const std::map<std::string, CliMemberFunctionPtr> kModeOpts;
    static const std::map<std::string, Board::Config> kBoardOpts;
    static const std::map<std::string, Alphabet::Config> kAlphabetOpts;
    static const std::map<std::string, Lexicon::Config> kLexiconOpts;
    static const std::string kShortHelpPath;
    static const std::string kFullHelpPath;
    static const std::string kVersionPath;

    std::vector<std::string> args_;
    CliMemberFunctionPtr mode_;

    Board::Config board_config_;
    std::string board_config_path_;

    Alphabet::Config alphabet_config_;
    std::string alphabet_config_path_;

    Lexicon::Config lexicon_config_;
    std::string lexicon_config_path_;

    std::string rack_;
    std::string computer_;
    std::string contents_path_;
    std::string gcg_path_;
    std::string out_path_;
    int num_self_play_;
    std::string word_list_path_;
    bool quiet_;

    std::vector<CommandArg> parsers_;

    void InitParsers();

    void Parse();
    void CheckForNext(const std::vector<std::string>::iterator& itr,
                      const std::string& error) const;

    void PrintFile(const std::string& path);

    void Moves();
    void SelfPlay();
    void BuildDawg();
    void BuildDict();
    void Gui();

    std::string BoardPath() const;
    std::string AlphabetPath() const;
    std::string LexiconPath() const;

    void SetPlayerStrategy(Player* player) const;

    std::vector<std::string> ReadWordList();
    std::ofstream OpenOutPath();

    struct SetField {
        SetField(std::string& field) : field_(field) {}
        void operator() (const std::string& arg) {
            field_ = arg;
        }
    private:
        std::string& field_;
    };

};

class CommandLineInterface::CommandArg {

public:
    using NoArgParser = std::function<void(void)>;
    using ArgParser = std::function<void(const std::string&)>;
    using ArgVec = std::vector<std::string>;

private:
    CommandArg(const std::string& full_cmd, char abbreviation, bool needs_arg,
               const std::string& error, ArgParser parser)
          : full_cmd_(full_cmd),
            abbreviation_(abbreviation),
            needs_arg_(needs_arg),
            error_(error),
            parser_(parser) {}

public:
    CommandArg(const std::string& full_cmd, NoArgParser&& parser)
          : CommandArg(full_cmd, full_cmd[0], false, "",
                       ConvertParser(std::forward<NoArgParser>(parser))) {}

    CommandArg(const std::string& full_cmd, const std::string& error,
               ArgParser&& parser)
          : CommandArg(full_cmd, full_cmd[0], true, error, parser) {}

    CommandArg(const std::string& full_cmd, char abbreviation,
               NoArgParser&& parser)
          : CommandArg(full_cmd, abbreviation, false, "",
                move(ConvertParser(std::forward<NoArgParser>(parser)))) {}

    CommandArg(const std::string& full_cmd, char abbreviation,
               const std::string& error, ArgParser&& parser)
          : CommandArg(full_cmd, abbreviation, true, error, parser) {}

    bool operator== (const std::string& probe) const;

    void Parse(const ArgVec& args, ArgVec::iterator& itr);

private:
    std::string full_cmd_;
    char abbreviation_;
    bool needs_arg_;
    std::string error_;
    ArgParser parser_;

    static ArgParser ConvertParser(NoArgParser&& parser) {
        return [parser](const std::string& s) {
            return parser();
        };
    }
};

}

#endif
