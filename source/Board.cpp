/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Board.cpp
 * @brief Implementation of the Board class.
 */

#include "Board.h"

#include "Move.h"
#include "Color.h"
#include "TileBag.h"
#include "Alphabet.h"
#include "Lexicon.h"
#include "BoardDefinition.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;
using namespace nigel;

const string Board::kWordPremiums   = "-234567890";
const string Board::kLetterPremiums = "-BCDEFGHIJ";

const map<Board::Config, string> Board::kConfigPathMap {
    {Config::SCRABBLE,       NIGEL_RESOURCE("boards/scrabble.board")},
    {Config::SUPER_SCRABBLE, NIGEL_RESOURCE("boards/superscrabble.board")},
    {Config::WWF,            NIGEL_RESOURCE("boards/wwf.board")}
};

bool IsValidPremiumChar(char c);

Board::Board(shared_ptr<Alphabet> alphabet,
             shared_ptr<Lexicon> lexicon,
             Config config)
      : Board(alphabet, lexicon, kConfigPathMap.at(config)) {
}

Board::Board(shared_ptr<Alphabet> alphabet, shared_ptr<Lexicon> lexicon,
             const string& configPath)
      : occupied_(0)
      , alphabet_(alphabet)
      , lexicon_(lexicon)
      , moves_finder_(nullptr) {
    ReadBoardSetup(configPath);
    moves_finder_ = MakeMovesFinder();
}

Board::Board(shared_ptr<Alphabet> alphabet,
             shared_ptr<Lexicon> lexicon,
             istream& configStream)
      : occupied_(0)
      , alphabet_(alphabet)
      , lexicon_(lexicon)
      , moves_finder_(nullptr) {
    ReadBoardSetup(configStream);
    moves_finder_ = MakeMovesFinder();
}

Board::Board(std::shared_ptr<Alphabet> alphabet,
             std::shared_ptr<Lexicon> lexicon,
             const BoardDefinition& definition)
      : rows_(definition.rows_)
      , cols_(definition.cols_)
      , start_row_(definition.start_row_)
      , start_col_(definition.start_col_)
      , config_name_(definition.name_)
      , premium_(definition.premium_)
      , board_(definition.board_)
      , occupied_(0)
      , alphabet_(alphabet)
      , lexicon_(lexicon)
      , moves_finder_(MakeMovesFinder()) {
}

Board::Board(const Board& other)
      : rows_(other.rows_)
      , cols_(other.cols_)
      , start_row_(other.start_row_)
      , start_col_(other.start_col_)
      , config_name_(other.config_name_)
      , premium_(other.premium_)
      , board_(other.board_)
      , occupied_(other.occupied_)
      , temp_moves_(other.temp_moves_)
      , alphabet_(other.alphabet_)
      , lexicon_(other.lexicon_)
      , moves_finder_(MakeMovesFinder()) {

}

Board::Board(Board&& other)
      : rows_(other.rows_)
      , cols_(other.cols_)
      , start_row_(other.start_row_)
      , start_col_(other.start_col_)
      , config_name_(other.config_name_)
      , premium_(move(other.premium_))
      , board_(move(other.board_))
      , occupied_(other.occupied_)
      , temp_moves_(move(other.temp_moves_))
      , alphabet_(other.alphabet_)
      , lexicon_(other.lexicon_)
      , moves_finder_(MakeMovesFinder()) {
    other.moves_finder_ = nullptr;
}

Board& Board::operator= (const Board& other) {
    Board temp(other);
    Swap(temp);
    moves_finder_ = MakeMovesFinder();
    return *this;
}

Board& Board::operator= (Board&& other) {
    Swap(other);
    moves_finder_ = MakeMovesFinder();
    other.moves_finder_.reset();
    return *this;
}

void Board::ReadBoardSetup(const std::string& path) {
    ifstream fin(path);
    if(!fin)
        throw OpenError(path);
    try {
        ReadBoardSetup(fin);
    } catch(InvalidDataError&) {
        throw InvalidDataError(path);
    }
}

void Board::ReadBoardSetup(istream& is) {
    string line;
    rows_ = -1;
    cols_ = -1;
    start_row_ = -1;
    start_col_ = -1;
    int curr_row = 0;
    try {
        while(getline(is, line)) {
            if(StartsWith(line, "NAME=")) {
                config_name_ = line.substr(5);

            } else if(StartsWith(line, "ROWS=")) {
                rows_ = ConvertToInt(line.substr(5));

            } else if(StartsWith(line, "COLS=")) {
                cols_ = ConvertToInt(line.substr(5));

            } else if(StartsWith(line, "START_ROW=")) {
                start_row_ = ConvertToInt(line.substr(10)) - 1;

            } else if(StartsWith(line, "START_COL=")) {
                start_col_ = ConvertToInt(line.substr(10)) - 1;

            } else if(curr_row < rows_) {
                vector<char> premium_row;
                for(int i = 0; i < line.size() && i < cols_; i++) {
                    if(!IsValidPremiumChar(line[i]))
                        throw InvalidDataError();
                    premium_row.push_back(line[i]);
                }
                for(int i = line.size(); i < cols_; i++) {
                    premium_row.push_back('-');
                }
                premium_.push_back(premium_row);
                curr_row++;

            } else {
                break;
            }
        }
    } catch(ConvertError&) {
        throw InvalidDataError();
    }
    FinishPremiumBoard();
    CreateEmptyBoard();
}

void Board::FinishPremiumBoard() {
    while(premium_.size() < rows_) {
        vector<char> premium_row(cols_, '-');
        premium_.push_back(move(premium_row));
    }
}

void Board::CreateEmptyBoard() {
    for(int row = 0; row < rows_; row++) {
        vector<Tile> board_row(cols_, Tile());
        board_.push_back(move(board_row));
    }
}

bool IsValidPremiumChar(char c) {
    return c == '-' or (c >= 'B' and c <= 'J') or
           (c >= '2' and c <= '9') or c == '0';
}

void Board::ReadBoardContents(const string& path, shared_ptr<Alphabet> alphabet) {
    ifstream fin(path);
    if(!fin)
        throw OpenError(path);
    try {
        ReadBoardContents(fin, alphabet);
    } catch(InvalidDataError&) {
        throw InvalidDataError(path);
    }
    fin.close();
}

void Board::ReadBoardContents(istream& is, shared_ptr<Alphabet> alphabet) {
    Clear();
    string line;
    auto row = board_.begin();
    while(getline(is, line) && row != board_.end()) {
        for(int col = 0; col < line.size() && col < cols_; col++) {
            char let = line[col];
            if(let >= 'A' && let <= 'Z') {
                (*row)[col] = alphabet->CreateTile(line[col]);
                occupied_++;
            } else if(let >= 'a' && let <= 'z') {
                (*row)[col] = Tile(let - 'a' + 'A', 0);
                occupied_++;
            } else {
                (*row)[col] = Tile();
            }
        }
        row++;
    }
}

void Board::Play(shared_ptr<Move> move) {
    vector<Coord> locs;
    if(move->IsMoveOnBoard()) {
        MoveCoord curr_loc = move->GetCoord();
        auto itr = move->GetPlaced().begin();
        while(itr != move->GetPlaced().end()) {
            if((*this)(curr_loc).IsEmpty()) {
                (*this)(curr_loc) = *itr++;
                locs.push_back(curr_loc);
                occupied_++;
            }
            curr_loc = curr_loc.Next();
        }
    }
    moves_finder_->UpdateLocations(locs);
}

void Board::Clear() {
    for(auto& row : board_) {
        for(Tile& tile : row) {
            tile.SetLetter('\0');
            tile.SetScore(-1);
        }
    }
    occupied_ = 0;
}

void Board::PrintWithColor(ostream& os, int indent) const {
    static const string kColumnHeaders = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for(int i = 0; i < indent; i++)
        os << ' ';
    os << "    ";
    for(int col = 0; col < cols_; col++) {
        os << " " << kColumnHeaders[col % kColumnHeaders.size()] << " ";
    }
    os << endl;
    for(int row = 0; row < rows_; row++) {
        for(int i = 0; i < indent; i++)
            os << ' ';
        os << setw(3) << row + 1 << " ";
        for(int col = 0; col < cols_; col++) {
            char pm = premium_[row][col];
            const Tile& tile = board_[row][col];

            os << Console::kBlackFg;
            switch(pm) {
                case '4': os << Console::kPurpleBg;   break;
                case '3': os << Console::kRedBg;      break;
                case '2': os << Console::kYellowBg;   break;
                case 'D': os << Console::kBlueBg;
                          os << Console::kWhiteFg;    break;
                case 'C': os << Console::kGreenBg;    break;
                case 'B': os << Console::kCyanBg;     break;
                default:  os << Console::kWhiteBg;
            }
            if(tile.IsEmpty()) {
                os << "   ";
            } else {
                os << " ";
                if(tile.IsBlank()) {
                    os << Console::kUnderlineOn;
                    os << tile.GetLetter();
                    os << Console::kUnderlineOff;
                } else {
                    os << tile.GetLetter();
                }
                os << " ";
            }
            os << Console::kReset;
        }
        os << endl;
    }
}

int Board::WordMultiplier(char premium) {
    int val = kWordPremiums.find(premium);
    return val == string::npos ? 1 : (val + 1);
}

int Board::LetterMultiplier(char premium) {
    int val = kLetterPremiums.find(premium);
    return val == string::npos ? 1 : (val + 1);
}

void Board::PushTempMove(shared_ptr<Move> move) {
    vector<Coord> locs;
    MoveCoord curr_loc = move->GetCoord();
    auto itr = move->GetPlaced().begin();
    while(itr != move->GetPlaced().end()) {
        if((*this)(curr_loc).IsEmpty()) {
            (*this)(curr_loc) = *itr++;
            locs.push_back(curr_loc);
            occupied_++;
        }
        curr_loc = curr_loc.Next();
    }

    temp_moves_.push(make_pair(move, locs));
}

shared_ptr<Move> Board::PopTempMove() {
    auto temp_move = temp_moves_.top();
    auto move = temp_move.first;
    auto locs = temp_move.second;
    for(auto& loc : locs) {
        (*this)(loc).Clear();
        occupied_--;
    }
    temp_moves_.pop();
    return move;
}

void Board::ClearTempMoves() {
    while(not temp_moves_.empty()) {
        PopTempMove();
    }
}

void Board::Swap(Board& other) {
    swap(rows_, other.rows_);
    swap(cols_, other.cols_);
    swap(start_row_, other.start_row_);
    swap(start_col_, other.start_col_);
    swap(config_name_, other.config_name_);
    swap(premium_, other.premium_);
    swap(board_, other.board_);
    swap(occupied_, other.occupied_);
    swap(temp_moves_, other.temp_moves_);
    swap(alphabet_, other.alphabet_);
    swap(lexicon_, other.lexicon_);
}

unique_ptr<MovesFinder> Board::MakeMovesFinder() {
    return unique_ptr<MovesFinder>(
        new MovesFinder(*this, alphabet_, lexicon_));
}


