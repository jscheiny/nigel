/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Color.h
 * @brief Constants for color console output on unix systems.
 */

#ifndef COLOR_H
#define COLOR_H

#include <string>

namespace nigel {

/// Contains constants that provide colors and styles for unix console output.
namespace Console {

/// Console style escape sequence
const std::string kEsc = "\033[";

/// Black foreground color
const std::string kBlackFg  = kEsc + "30m";
/// Red foreground color
const std::string kRedFg    = kEsc + "31m";
/// Green foreground color
const std::string kGreenFg  = kEsc + "32m";
/// Yellow foreground color
const std::string kYellowFg = kEsc + "33m";
/// Blue foreground color
const std::string kBlueFg   = kEsc + "34m";
/// Purple foreground color
const std::string kPurpleFg = kEsc + "35m";
/// Cyan foreground color
const std::string kCyanFg   = kEsc + "36m";
/// White foreground color
const std::string kWhiteFg  = kEsc + "37m";

/// Black background color
const std::string kBlackBg  = kEsc + "40m";
/// Red background color
const std::string kRedBg    = kEsc + "41m";
/// Green background color
const std::string kGreenBg  = kEsc + "42m";
/// Yellow background color
const std::string kYellowBg = kEsc + "43m";
/// Blue background color
const std::string kBlueBg   = kEsc + "44m";
/// Purple background color
const std::string kPurpleBg = kEsc + "45m";
/// Cyan background color
const std::string kCyanBg   = kEsc + "46m";
/// White background color
const std::string kWhiteBg  = kEsc + "47m";

/// Turn bold text on
const std::string kBoldOn   = kEsc +  "1m";
/// Turn bold text off
const std::string kBoldOff  = kEsc + "22m";
/// Turn text blink on
const std::string kBlinkOn  = kEsc +  "5m";
/// Turn text blink off
const std::string kBlinkOff = kEsc + "25m";
/// Turn underlined text on
const std::string kUnderlineOn = kEsc + "4m";
/// Turn underlined text off
const std::string kUnderlineOff= kEsc + "24m";
/// Turn inverted background on
const std::string kRevOn    = kEsc +  "7m";
/// Turn inverted background off
const std::string kRevOff   = kEsc + "27m";
/// Reset console colors
const std::string kReset    = kEsc +  "0m";

}

}

#endif
