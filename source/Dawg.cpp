/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Dawg.cpp
 * @brief Implementation of the Dawg class.
 */

#include "Dawg.h"
#include "Utility.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cctype>
#include <vector>
#include <set>

using namespace std;
using namespace nigel;

using Edge = pair<int, int>;

const char Dawg::kEow = 'Z' + 1;

// Traverse the dawg represented by the given node, adding all nodes to the
// deleted nodes set.
void DeleteNode(Dawg::Node* node, set<Dawg::Node*>& deleted_nodes);

// Connects the nodes in the vector together using the edges provided.
void MakeConnections(vector<Dawg::Node*>& nodes, vector<Edge>& edges);

string Dawg::Node::GetChildren() const {
    string result;
    for(char letter = 'A'; letter <= 'Z'; letter++) {
        if(children_[Index(letter)] != nullptr) {
            result += letter;
        }
    }
    return result;
}

Dawg::Dawg()
      : words_(0)
      , nodes_(0)
      , root_(new Node('\0', *this))
      , eow_(new Node(kEow, *this)) {
}

Dawg::Dawg(const Dawg& other) : Dawg() {
    other.EachWord([this] (const string& word) {
        Insert(word);
    });
}

Dawg::Dawg(Dawg&& other)
      : words_(other.words_)
      , nodes_(other.nodes_)
      , root_(other.root_)
      , eow_(other.eow_) {
    other.root_ = nullptr;
    other.eow_ = nullptr;
}

Dawg::Dawg(istream& is) {
    ReadFromFile(is);
}

Dawg::Dawg(const string& path)
      : words_(0)
      , nodes_(0)
      , root_(nullptr)
      , eow_(nullptr) {
    ifstream fin(path);
    if(fin.fail())
        throw OpenError(path);

    try {
        ReadFromFile(fin);
    } catch(InvalidDataError&) {
        throw InvalidDataError(path);
    }
    fin.close();
}

void Dawg::ReadFromFile(istream& is) {
    int num_nodes = ReadNumNodes(is);
    Node* temp_root;
    Node* temp_eow = new Node(kEow, *this);

    vector<Node*> nodes;
    vector<Edge> edges;

    try {
        for(int index = 0; index < num_nodes; index++) {
            bool is_root = ReadNodeLine(index, is, nodes, edges, temp_eow, num_nodes);
            if(is_root)
                temp_root = nodes.back();
        }
    } catch(InvalidDataError&) {
        // Clean up temporary nodes created before rethrowing the error.
        for(Node* node : nodes)
            delete node;
        throw;
    }
    MakeConnections(nodes, edges);
    root_ = temp_root;
    eow_ = temp_eow;
    CountWords();
}

int Dawg::ReadNumNodes(istream& fin) const {
    string line;
    int num_nodes;
    getline(fin, line);
    try {
        num_nodes = ConvertToInt(line);
        if(num_nodes <= 0)
            throw InvalidDataError();
    } catch(...) {
        throw InvalidDataError();
    }
    return num_nodes;
}

bool Dawg::ReadNodeLine(int index,
                        istream& fin,
                        vector<Dawg::Node*>& nodes,
                        vector<Edge>& edges,
                        Dawg::Node* eow_node,
                        int num_nodes) {
    bool is_root = false;
    string line;
    getline(fin, line);
    vector<string> tokens = Split(line);
    Node* curr_node;

    if(tokens[0] == "root") {
        curr_node = new Node('\0', *this);
        is_root = true;
    } else if(tokens[0].size() != 1) {
        throw InvalidDataError();
    } else {
        curr_node = new Node(tokens[0][0], *this);
    }

    nodes.push_back(curr_node);
    for(auto itr = tokens.begin() + 1; itr != tokens.end(); ++itr) {
        if(*itr == "EOW") {
            curr_node->PutChild(eow_node);
        } else {
            int child_id = 0;
            try {
                child_id = ConvertToInt(*itr);
            } catch(ConvertError&) {
                throw InvalidDataError();
            }

            if(child_id < 0 || child_id >= num_nodes) {
                throw InvalidDataError();
            }

            if(child_id < index) {
                curr_node->PutChild(nodes[child_id]);
            } else {
                edges.push_back(make_pair(index, child_id));
            }
        }
    }
    return is_root;
}

void MakeConnections(vector<Dawg::Node*>& nodes, vector<Edge>& edges) {
    for(Edge& e : edges) {
        Dawg::Node* parent = nodes[e.first];
        Dawg::Node* child = nodes[e.second];
        parent->PutChild(child);
    }
}

Dawg::~Dawg() {
    Clear();
    delete root_;
    delete eow_;
}

void Dawg::Clear() {
    words_ = 0;
    nodes_ = 0;
    if(root_ != nullptr) {
        set<Node*> deleted_nodes;
        DeleteNode(root_, deleted_nodes);
    }
    root_ = new Node('\0', *this);
    eow_ = new Node(kEow, *this);
}

void DeleteNode(Dawg::Node* node, set<Dawg::Node*>& deleted_nodes) {
    if(deleted_nodes.count(node) == 0) {
        for(char letter = 'A'; letter <= Dawg::kEow; letter++) {
            if(node->HasChild(letter)) {
                DeleteNode(node->GetChild(letter), deleted_nodes);
            }
        }
        deleted_nodes.insert(node);
        delete node;
    }
}

Dawg& Dawg::operator= (const Dawg& other) {
    Dawg copy(other);
    Swap(copy);
    return *this;
}

Dawg& Dawg::operator= (Dawg&& other) {
    Swap(other);
    return *this;
}

bool Dawg::Insert(const string& word) {
    Node* current = root_;
    for(char letter : word) {
        Node* child = current->GetChild(letter);
        if(!child) {
            child = new Node(letter, *this);
            current->PutChild(child);
        }
        current = child;
    }
    if(current->HasChild(kEow))
        return true;
    words_++;
    current->PutChild(eow_);
    return false;
}

bool Dawg::Contains(const string& word) const {
    Node* current = root_;
    for(char let : word) {
        Node* child = current->GetChild(let);
        if(child) {
            current = child;
        } else {
            return false;
        }
    }
    return current->HasChild(kEow);
}

const Dawg::Node* Dawg::Prefix(const string& prefix) const {
    Node* current = root_;
    for(char let : prefix) {
        Node* child = current->GetChild(let);
        if(child) {
            current = child;
        } else {
            return nullptr;
        }
    }
    return current;
}

void Dawg::Swap(Dawg& other) {
    swap(words_, other.words_);
    swap(nodes_, other.nodes_);
    swap(root_, other.root_);
    swap(eow_, other.eow_);
}

void Dawg::CountWords() {
    words_ = 0;
    CountWords(root_, &words_);
}

void Dawg::CountWords(Node* root, int* words) {
    if(root->HasChild(kEow)) {
        (*words)++;
    }
    for(char let = 'A'; let <= 'Z'; let++) {
        if(root->HasChild(let)) {
            CountWords(root->GetChild(let), words);
        }
    }
}
