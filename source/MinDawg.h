/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file MinDawg.h
 * @brief Declaration of the MinDawg class.
 */

#ifndef MIN_DAWG_H
#define MIN_DAWG_H

#include <string>
#include <vector>
#include <list>
#include <set>

namespace nigel {

void BuildNigelDict(const std::string& word_list_path,
                    const std::string& out_path,
                    bool quiet);

class Dawg;

/// A trie that can be minimized into a directed acyclic word graph.
/**
 * The MinDawg provides the capability of taking a word list, inserting all of
 * the words into the trie, and then compressing that trie into a fully
 * minimized DAWG, and then save that as a .dawg file. This class should only
 * be used to create a minimized DAWG for a word list a single time, which
 * should then be saved and only used by reading the .dawg file in through the
 * Dawg class.
 */
class MinDawg {

public:
    /// Construct an empty MinDawg.
    MinDawg();

    /// Construct a MinDawg from the word list contained in a Dawg.
    /**
     * @param other the dawg from which to create this MinDawg.
     */
    explicit MinDawg(const Dawg& other);

    /// Construct a MinDawg from the word list contained in a given stream.
    /**
     * @param is the stream from which to read the words
     */
    explicit MinDawg(std::istream& is);

    /// Construct a MinDawg from the word list contained in a file at the path.
    /**
     * If the file at the path could not be opened, throws an OpenError.
     * @param path the path from which to read the word list
     */
    explicit MinDawg(const std::string& path);

    /// Construct a MinDawg from a range defined by a pair of iterators.
    /**
     * The iterators should dereference as strings.
     * @param begin the iterator at the start of the range
     * @param end the iterator one past the end of the range
     */
    template<typename InputIterator>
    MinDawg(InputIterator begin, InputIterator end);

    /// Move constructor
    MinDawg(MinDawg&& other);

    /// Destructor
    ~MinDawg();

    /// Move assignment
    MinDawg& operator= (MinDawg&& other);

    MinDawg(const MinDawg&) = delete;
    MinDawg& operator= (const MinDawg&) = delete;

    /// Remove all words and reset to the default empty state.
    void Clear();

    /// Return the number of nodes in the MinDawg.
    /**
     * This is agood method of getting a sense of the real size of the MinDawg.
     * Call this method before and after calling minimize() to see how effective
     * the minimization was.
     * @return the number of nodes used sto represent this Dawg.
     */
    int GetNodes() const {
        return nodes_;
    }

    /// Insert the word into the MinDawg.
    /**
     * @param word the word to insert into the Dawg
     * @return true if the word was already in the Dawg, false otherwise
     */
    bool Insert(const std::string& word);

    /// Minmizes the MinDawg from a trie into a fully minimized DAWG.
    /**
     * For best efficiency, add all desired words to the dawg, then call this
     * method, then call one of the save methods. To determine how much memory
     * was saved, call the nodes() method before and after calling this method.
     */
    void Minimize();

    /// Save the MinDawg in the .dawg file format in the output stream.
    /**
     * @param os the stream to which to save the dawg
     */
    void Save(std::ostream& os);

    /// Save the MinDawg in the .dawg file format in the file at the given path.
    /**
     * If the file could not be opened for writing, throws an OpenError.
     * @param path the path of the file to which to save the dawg
     */
    void Save(const std::string& path);

    /// Swap this MinDawg with another MinDawg.
    void Swap(MinDawg& other);

private:
    /// The end-of-word character.
    static const char kEow;

    class Node;
    friend class Node;

    /// The nodes in this MinDawg.
    std::vector<Node*> nodes_in_dawg_;
    /// The number of nodes in this MinDawg.
    int nodes_;
    /// The root node.
    Node* root_;
    /// The end-of-word node.
    Node* eow_;

    /// Joins the set of nodes together as a single sub-dawg
    /**
     * Picks a single node in the list as a "representative node" and for
     * each of the parents of the nodes in the list, puts the representative
     * node as their child.
     * @param nodes the nodes to join together as a single sub-dawg
     */
    void Join(std::list<Node*>& nodes);

    /// Traverse the dawg given by the node, adding nodes to the deleted set.
    /**
     * @param node the root of the dawg to traverse
     * @param deletedNodes will contain every node in the dawg
     */
    static void DeleteNode(Node* node, std::set<Node*>& deleted_nodes);
};

/// A node in the MinDawg
/**
 * This is kept private because users of the dawg should only be using the
 * MinDawg class to insert, minimize, and save. For more details on the
 * functionality of this class see Dawg::Node since this is pretty much
 * identical. The main difference is that MinDawg nodes keep track of their
 * parents to facilitate minimization
 */
class MinDawg::Node {
public:
    /// Construct a node with the given letter for the MinDawg.
    /**
     * The ID number for this node is set by a static variable that
     * increments everytime this constructor is called.
     * @param let the node's letter
     * @param md the MinDawg creating the node
     */
    Node(char letter, MinDawg& md) :
    letter_(letter), removed_(false), children_(), parents_() {
        static int id_counter = 0;
        id_ = id_counter++;
        md.nodes_in_dawg_.push_back(this);
        md.nodes_++;
    }

    // Deleted copy and move construction and assignment
    Node(const Node&) = delete;
    Node(Node&&) = delete;
    Node& operator=(const Node&) = delete;
    Node& operator=(Node&&) = delete;

    /// Returns the letter that this node represents.
    char GetLetter() const {
        return letter_;
    }

    /// Puts the given node as a child of this node.
    void PutChild(Node* child) {
        children_[Index(child->letter_)] = child;
        child->parents_.push_back(this);
    }

    /// Returns whether this node has a child of the given letter.
    bool HasChild(char letter) const {
        return children_[Index(letter)] != nullptr;
    }

    /// Returns this node's child of the given letter.
    Node* GetChild(char letter) {
        return children_[Index(letter)];
    }

    /// Returns this node's child of the given letter.
    const Node* GetChild(char letter) const {
        return children_[Index(letter)];
    }

    /// Removes this node's child of the given letter.
    void RemoveChild(char letter) {
        children_[Index(letter)] = nullptr;
    }

    /// Whether this node has been marked as removed.
    bool IsRemoved() const {
        return removed_;
    }

    /// Marks this node as removed from the dawg.
    void SetRemoved() {
        removed_ = true;
    }

    /// Returns this node's ID number.
    int GetId() const {
        return id_;
    }

    /// Sets this node's ID number.
    void SetId(int id) {
        id_ = id;
    }

    /// Returns a string with all the suffixes reachable from this node.
    std::string GetSuffixString() const;

    /// Calls a function on every suffix reachable from this node.
    template<typename Function>
    void Each(Function fn) const;

    friend MinDawg;

private:
    /// This node's ID number.
    int id_;
    /// The letter of this node.
    char letter_;
    /// Whether this node has been marked as removed, false by default.
    bool removed_;
    /// This node's children.
    Node* children_[27];

    /// Recursive helper function for the each method
    template<typename Function>
    static void Each(const Node* root, const std::string& prefix,
                     Function fn);

    /// Returns the index into the children array of the given letter.
    static int Index(char let) {
        return let - 'A';
    }

public:
    /// The set of parent's of this node.
    std::vector<Node*> parents_;

};

// MinDawg template function definitions

template<typename InputIterator>
MinDawg::MinDawg(InputIterator begin, InputIterator end) : MinDawg() {
    for(auto itr = begin; itr != end; ++itr) {
        Insert(*itr);
    }
}

template<typename Function>
void MinDawg::Node::Each(Function fn) const {
    Each(this, {letter_, '\0'}, fn);
}

template<typename Function>
void MinDawg::Node::Each(const Node* root, const std::string& prefix, Function fn) {
    if(root->HasChild(kEow)) {
        fn(prefix);
    }
    for(char letter = 'A'; letter <= 'Z'; letter++) {
        if(root->HasChild(letter)) {
            Each(root->GetChild(letter), prefix + letter, fn);
        }
    }
}

}

#endif
