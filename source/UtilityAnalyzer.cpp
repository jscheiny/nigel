/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file UtilityAnalyzer.cpp
 * @brief Implementation of the UtilityAnalyzer class.
 */

#include "UtilityAnalyzer.h"

#include "Move.h"
#include "Rack.h"
#include "TileBag.h"

#include <fstream>

using namespace nigel;
using namespace analyze;
using namespace std;

struct UtilityStatUpdater {
    using UtilityMap = typename UtilityAnalyzer::UtilityMap;

    UtilityStatUpdater(UtilityMap& utilities, shared_ptr<Alphabet> alphabet,
                       shared_ptr<Move> move)
        : utilities_(utilities)
        , alphabet_(alphabet)
        , score_(move->GetScore()) {}

    template<typename Iterator>
    void operator() (Iterator begin, Iterator end) {
        std::string subset = Rack{begin, end}.ToBlanksString(alphabet_);
        if(!subset.empty()) {
            sort(subset.begin(), subset.end());
            utilities_[subset.size()][subset].Add(score_);
        }
    }

private:
    UtilityMap& utilities_;
    shared_ptr<Alphabet> alphabet_;
    int score_;

};

void UtilityAnalyzer::ObserveMove(Game* game, shared_ptr<Move> move) {
    if(move->GetPlaced().size() + move->GetLeave().size() == 7 && move->IsMoveOnBoard()) {
        Rack placed = move->GetPlaced();
        placed.Sort();
        UtilityStatUpdater updater(utilities_, game->GetTileBag()->GetAlphabet(), move);
        ForEachSubset(placed.begin(), placed.end(), updater);
    }
}

void UtilityAnalyzer::Save(size_t k, ostream& os) const {
    auto itr = utilities_.find(k);
    if(itr != utilities_.end()) {
        const auto& k_letter_utils = itr->second;
        for(const auto& elem : k_letter_utils) {
            os << elem.first << "\t" << elem.second << "\n";
        }
    }
    os << flush;
}

void UtilityAnalyzer::SaveAll(const string& directory) const {
    for(const auto& entry : utilities_) {
        stringstream path;
        path << directory << entry.first << ".txt";
        ofstream fout(path.str());
        Save(entry.first, fout);
    }
}

