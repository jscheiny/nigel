/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Rack.h
 * @brief Declaration of the Rack class.
 */

#ifndef RACK_H
#define RACK_H

#include "Tile.h"

#include <vector>
#include <string>
#include <algorithm>

namespace nigel {

class Alphabet;

class Rack : public std::vector<Tile> {

public:
    Rack() {}

    template<typename Input>
    Rack(Input begin, Input end) : std::vector<Tile>(begin, end) {}

    Rack(const std::string& source, std::shared_ptr<Alphabet> alphabet);

    std::string ToString() const;
    std::string ToBlanksString(std::shared_ptr<Alphabet> alphabet) const;
    int Score() const;
    void Remove(const Rack& to_remove);
    void Sort() { sort(begin(), end()); }

    void PrintWithLowercaseBlanks(std::ostream& os) const;
    void PrintWithBlankSymbol(std::ostream& os, char blank_character='?') const;

};

std::ostream& operator<< (std::ostream& os, const Rack& rack);

}

#endif
