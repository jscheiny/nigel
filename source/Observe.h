/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Observe.h
 * @brief Declaration and implementation of Observer and Subject classes.
 */

#ifndef OBSERVE_H
#define OBSERVE_H

#include <vector>

namespace nigel {

template<typename T> class Subject;

template<typename T>
class Observer {

public:
    virtual ~Observer() {}

protected:
    virtual void Notify(Subject<T>* s, T value) = 0;

    friend class Subject<T>;
};

template<typename T>
class Subject {

public:
    using ObserverType = Observer<T>;
    using SubjectType = Subject<T>*;

    virtual ~Subject() {}

    void RegisterObserver(Observer<T>* o) {
        observers_.push_back(o);
    }

    void RegisterObserver(Observer<T>& o) {
        observers_.push_back(&o);
    }

    template<typename... Args>
    void RegisterObservers(Observer<T>* o, Args&&... rest) {
        observers_.push_back(o);
        RegisterObservers(std::forward<Args...>(rest)...);
    }

    template<typename... Args>
    void RegisterObservers(Observer<T>& o, Args&&... rest) {
        observers_.push_back(&o);
        RegisterObservers(std::forward<Args...>(rest)...);
    }

    void DeleteObserver(Observer<T>* o) {
        auto itr = std::find(observers_.begin(), observers_.end(), o);
        if(itr == observers_.end()) {
            observers_.erase(itr);
        }
    }

    void DeleteObserver(Observer<T>& o) {
        DeleteObserver(&o);
    }

    void DeleteObservers() {
        observers_.clear();
    }

protected:
    void NotifyObservers(T value) {
        for(auto obs : observers_) {
            obs->Notify(this, value);
        }
    }

private:
    std::vector<Observer<T>*> observers_;

    void RegisterObservers(Observer<T>* o) {
        observers_.push_back(o);
    }

    void RegisterObservers(Observer<T>& o) {
        observers_.push_back(&o);
    }

};

}

#endif
