/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file MinDawg.cpp
 * @brief Implementation of the MinDawg class.
 */

#include "MinDawg.h"

#include "Dawg.h"
#include "Utility.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cctype>
#include <vector>
#include <map>

using namespace std;
using namespace nigel;

const char MinDawg::kEow = 'Z' + 1;

string MinDawg::Node::GetSuffixString() const {
    string suffix_str;
    Each([&suffix_str] (const string& word) {
        suffix_str += word;
        suffix_str += "|";
    });
    return suffix_str;
}

MinDawg::MinDawg()
      : nodes_(0)
      , root_(new Node('\0', *this))
      , eow_(new Node(kEow, *this)) {
}

MinDawg::MinDawg(const Dawg& other) : MinDawg() {
    other.EachWord([this] (const string& word) {
        Insert(word);
    });
}

inline MinDawg::MinDawg(MinDawg&& other)
      : nodes_in_dawg_()
      , nodes_(0)
      , root_(other.root_)
      , eow_(other.eow_) {
    other.root_ = nullptr;
    other.eow_ = nullptr;
}

MinDawg::MinDawg(istream& is)
      : MinDawg(Dawg(is)) {
}

MinDawg::MinDawg(const string& path)
      : MinDawg(Dawg(path)) {
}

MinDawg::~MinDawg() {
    Clear();
    delete root_;
    delete eow_;
}

MinDawg& MinDawg::operator= (MinDawg&& other) {
    Swap(other);
    return *this;
}

void MinDawg::Clear() {
    if(root_ != nullptr) {
        set<Node*> deleted_nodes;
        DeleteNode(root_, deleted_nodes);
    }
    root_ = new Node('\0', *this);
    eow_ = new Node(kEow, *this);
}

void MinDawg::DeleteNode(Node* node, set<Node*>& deleted_nodes) {
    if(deleted_nodes.count(node) == 0) {
        for(char let = 'A'; let <= MinDawg::kEow; let++) {
            if(node->HasChild(let)) {
                DeleteNode(node->GetChild(let), deleted_nodes);
            }
        }
        deleted_nodes.insert(node);
        delete node;
    }
}

bool MinDawg::Insert(const string& word) {
    Node* current = root_;
    for(char letter : word) {
        Node* child = current->GetChild(letter);
        if(!child) {
            child = new Node(letter, *this);
            current->PutChild(child);
        }
        current = child;
    }
    if(current->HasChild(kEow))
        return true;
    current->PutChild(eow_);
    return false;
}

void MinDawg::Minimize() {
    map<string, list<Node*>> groupings;
    for(Node* node : nodes_in_dawg_) {
        string key = node->GetSuffixString();
        groupings[key].push_back(node);
    }

    for(auto entry : groupings) {
        auto& nodes = entry.second;
        if(nodes.size() > 1) {
            Join(nodes);
        }
    }

    int index = 0;
    vector<Node*> unremoved_nodes;
    for(Node* node : nodes_in_dawg_) {
        if(node != eow_) {
            if(!node->IsRemoved()) {
                node->SetId(index);
                index++;
                unremoved_nodes.push_back(node);
            } else {
                nodes_--;
                delete node;
            }
        }
    }
    nodes_in_dawg_ = unremoved_nodes;
}

void MinDawg::Join(list<MinDawg::Node*>& nodes) {
    Node* rep = nullptr;
    for(Node* node : nodes) {
        if(!node->IsRemoved()) {
            if(rep == nullptr) {
                rep = node;
            } else {
                for(Node* parent : node->parents_) {
                    parent->RemoveChild(node->GetLetter());
                    parent->PutChild(rep);
                }
                node->SetRemoved();
            }
        }
    }
}

void MinDawg::Save(ostream& os) {
    os << nodes_in_dawg_.size() << endl;
    for(Node* node : nodes_in_dawg_) {
        if(node != eow_) {
            if(node == root_) {
                os << "root";
            } else {
                os << node->GetLetter();
            }
            for(char let = 'A'; let <= 'Z'; let++) {
                if(node->HasChild(let))
                    os << "\t" << node->GetChild(let)->GetId();
            }
            if(node->HasChild(kEow))
                os << "\tEOW";
            os << endl;
        }
    }
}

void MinDawg::Save(const string& path) {
    ofstream fout(path);
    if(!fout)
        throw OpenError(path);
    Save(fout);
    fout.close();
}

void MinDawg::Swap(MinDawg& other) {
    swap(root_, other.root_);
    swap(eow_, other.eow_);
}

void nigel::BuildNigelDict(const string& word_list_path,
                           const string& out_path,
                           bool quiet) {
    list<string> words;
    map<int, MinDawg> dawgs;

    ifstream fin(word_list_path);
    string line;
    while(getline(fin, line)) {
        line = Upper(line);
        char last = line[line.size() - 1];
        if(last < 'A' || last > 'Z') {
            line = line.substr(0, line.size() - 1);
        }

        words.push_back(line);
        int word_length = line.size();
        if(dawgs.count(word_length) == 0) {
            dawgs.emplace(word_length, MinDawg());
        }
        dawgs[word_length].Insert(line);
    }

    if(not quiet)
        cout << "Words: " << words.size() << endl;

    for(auto& entry : dawgs) {
        MinDawg& dawg = entry.second;
        if(not quiet)
            cout << "Minimizing " << entry.first << "-dawg...\t" << flush;
        int start = dawg.GetNodes();
        dawg.Minimize();
        int end = dawg.GetNodes();
        double percent = 1 - (double)end / start;
        percent = (int)(percent * 10000) / 100.;
        if(not quiet)
            cout << percent << '%' << endl;
    }

    if(not quiet)
        cout << "Writing out word list..." << endl;
    ofstream listout(out_path + "list.txt");
    for(auto& word : words) {
        listout << word << '\n';
    }
    listout.close();

    if(not quiet)
        cout << "Writing out minimized DAWGs..." << endl;
    for(auto& entry : dawgs) {
        stringstream ss;
        ss << out_path << entry.first << ".dawg";
        entry.second.Save(ss.str());
    }
}
