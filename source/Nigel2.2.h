/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NIGEL_2_2_H
#define NIGEL_2_2_H

#include "Nigel1.h"
#include "Utility.h"

#include <unordered_map>

namespace nigel {

/// A strategy that defines utilities via the Nigel 2.2 utility valuations.
NIGEL_DEFINE_STRATEGY(2,2) : public Nigel<1,0> {

public:
    double Utility(Rack& leave) override;

};

class Nigel22Resources {

public:
    static Nigel22Resources& Instance() {
        static Nigel22Resources instance;
        return instance;
    }

    Nigel22Resources(const Nigel22Resources&) = delete;
    Nigel22Resources(Nigel22Resources&&) = delete;
    Nigel22Resources& operator= (const Nigel22Resources&) = delete;
    Nigel22Resources& operator= (Nigel22Resources&&) = delete;

    double ComputeUtility(const Rack& leave);
    double ComputeNLetterUtil(const std::string& leave);

    static const double kAveragePlayScore;

private:
    static const int kCountThreshold;
    static const LinearFunction kLeave4Fn;
    static const LinearFunction kLeave5Fn;
    static const LinearFunction kLeave6Fn;

    std::unordered_map<std::string, double> utility_map_;

    Nigel22Resources();
    void ReadUtility(const std::string& path);

};

}

#endif
