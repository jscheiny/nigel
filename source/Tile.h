/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Tile.h
 * @brief Declaration of the Tile class.
 */

#ifndef TILE_H
#define TILE_H

#include <iosfwd>

namespace nigel {

/// A Scrabble tile.
/**
 * Represents a character score pair which defines a Scrabble tile. A default
 * constructed tile (or one that has been cleared) is considered to be "empty"
 * and it has letter '\0' and score -1. A tile with a score of 0 is considered
 * to be a blank tile.
 */
class Tile {

public:
    /// Creates an empty tile
    Tile() : Tile(kEmptyLetter, kEmptyScore) {}

    /// Creates a tile from the given letter and score.
    /**
     * @param letter_ the tile's letter
     * @param score_ the tile's score
     */
    Tile(char letter, int score) : letter_(letter), score_(score) {}

    // Defaulted copy/move construction/assignment and destruction

    Tile(const Tile&) = default;
    Tile(Tile&&) = default;
    ~Tile() = default;
    Tile& operator= (const Tile&) = default;
    Tile& operator= (Tile&&) = default;

    /// Returns true if this is an empty tile (the score is -1).
    /**
     * Default constructed tiles, and tiles that have just been cleared are
     * considered empty.
     * @return true if this tile is empty, false otherwise
     */
    bool IsEmpty() const {
        return score_ == kEmptyScore;
    }

    /// Clears this tile reseting it to the empty state.
    /**
     * After calling this, empty() will return true.
     */
    void Clear() {
        letter_ = kEmptyLetter;
        score_ = kEmptyScore;
    }

    /// Returns this tile's letter.
    char GetLetter() const {
        return letter_;
    }

    /// Sets this tile's letter.
    void SetLetter(char letter) {
        letter_ = letter;
    }

    /// Returns this tile's score.
    int GetScore() const {
        return score_;
    }

    /// Sets this tile's score.
    void SetScore(int score) {
        score_ = score;
    }

    /// Returns true if this is a blank tile.
    /**
     * A blank tile is defined as a tile with score 0.
     * @return true if the tile is a blank tile, false otherwise
     */
    bool IsBlank() const {
        return score_ == 0;
    }

    /// Returns true if this tile is considered less than the other.
    /**
     * Compares first by character, then by score.
     * @param other the tile to compare this one to
     * @return true if this tile is less than the other, false otherwise
     */
    bool operator< (const Tile& other) const;

    /// Returns true if this tile is equal to the other.
    /**
     * @param other the tile to compare this one to
     * @return true if this tile has the same letter and score as the other,
     * false otheriwse
     */
    bool operator== (const Tile& other) const;

    /// Returns true if this tile is not equal to the other.
    /**
     * @param other the tile to compare this one to
     * @return true if this tile has a different letter or score as the other,
     * false otherwise.
     */
    bool operator!= (const Tile& other) const;

private:
    /// The letter of the empty tile.
    static const char kEmptyLetter;
    /// The score of the empty tile.
    static const int kEmptyScore;

    /// This tile's letter.
    char letter_;
    /// This tile's score.
    int score_;
};

/// Prints the tile in the form A# to the output stream
/**
 * For example, Tile('Z', 10) will be printed as "Z10".
 * @param os the output stream to print to
 * @param tile the tile to print
 */
std::ostream& operator<< (std::ostream& os, const Tile& tile);

}

#endif
