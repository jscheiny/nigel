#include "BoardDefinition.h"
#include "Tile.h"

#include <algorithm>

using namespace nigel;
using namespace std;

const BoardDefinition BoardDefinition::SCRABBLE {
    "Scrabble", 8, 8, {
        {"3--B---3---B--3"},
        {"-2---C---C---2-"},
        {"--2---B-B---2--"},
        {"B--2---B---2--B"},
        {"----2-----2----"},
        {"-C---C---C---C-"},
        {"--B---B-B---B--"},
        {"3--B---2---B--3"},
        {"--B---B-B---B--"},
        {"-C---C---C---C-"},
        {"----2-----2----"},
        {"B--2---B---2--B"},
        {"--2---B-B---2--"},
        {"-2---C---C---2-"},
        {"3--B---3---B--3"}
    }
};

const BoardDefinition BoardDefinition::SUPER_SCRABBLE {
    "Super Scrabble", 11, 11, {
        {"4--B---3--B--3---B--4"},
        {"-2--C---2---2---C--2-"},
        {"--2--D---2-2---D--2--"},
        {"B--3--B---3---B--3--B"},
        {"-C--2---C---C---2--C-"},
        {"--D--2---B-B---2--D--"},
        {"---B--2---B---2--B---"},
        {"3------2-----2------3"},
        {"-2--C---C---C---C--2-"},
        {"--2--B---B-B---B--2--"},
        {"B--3--B---2---B--3--B"},
        {"--2--B---B-B---B--2--"},
        {"-2--C---C---C---C--2-"},
        {"3------2-----2------3"},
        {"---B--2---B---2--B---"},
        {"--D--2---B-B---2--D--"},
        {"-C--2---C---C---2--C-"},
        {"B--3--B---3---B--3--B"},
        {"--2--D---2-2---D--2--"},
        {"-2--C---2---2---C--2-"},
        {"4--B---3--B--3---B--4"}
    }
};

const BoardDefinition BoardDefinition::WWF {
    "Words With Friends", 8, 8, {
        {"---3--C-C--3---"},
        {"--B--2---2--B--"},
        {"-B--B-----B--B-"},
        {"3--C---2---C--3"},
        {"--B---B-B---B--"},
        {"-2---C---C---2-"},
        {"C---B-----B---C"},
        {"---2-------2---"},
        {"C---B-----B---C"},
        {"-2---C---C---2-"},
        {"--B---B-B---B--"},
        {"3--C---2---C--3"},
        {"-B--B-----B--B-"},
        {"--B--2---2--B--"},
        {"---3--C-C--3---"}
    }
};


BoardDefinition::BoardDefinition(const string& name,
                                 int start_row, int start_col,
                                 const vector<string>& board)
      : start_row_(start_row)
      , start_col_(start_col) {

    rows_ = board.size();
    if(rows_ == 0) {
        throw BoardDefinitionError::Empty();
    }
    cols_ = board[0].size();
    if(cols_ == 0) {
        throw BoardDefinitionError::Empty();
    }
    for(const auto& row : board) {
        if(row.size() != cols_) {
            throw BoardDefinitionError("Cannot create board with ragged rows.");
        }
    }
    if(start_row_ < 0 || start_row_ >= rows_ || start_col_ < 0 || start_col_ >= cols_) {
        throw BoardDefinitionError("Start position outside of board.");
    }
    name_ = name;
    transform(board.begin(), board.end(), back_inserter(premium_),
        [](const string& s) { return vector<char>(s.begin(), s.end()); });
    board_ = {rows_, {cols_, Tile()}};
}
