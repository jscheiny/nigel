/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Player.h
 * @brief Declaration of the Player class.
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "TileBag.h"
#include "Lexicon.h"
#include "Strategy.h"
#include "Rack.h"
#include "Versioning.h"

#include <iostream>
#include <memory>
#include <string>

namespace nigel {

class Game;
class Move;
class Board;

/// A player in a Scrabble game.
/**
 * A player is associated with a game of Scrabble, and has associated with it a
 * single strategy instance. Players create their strategy instance take sole
 * ownership of the strategy. A player can either be an independent entity
 * (for simple analysis usage) or a member of a game. If a player is a member of
 * a game, then the Game object is responsible for the creation and management
 * of its own players.
 *
 * A player maintains a rack, and a list of all of its moves, and provides the
 * capability to generate the player's best move (based on the player's
 * strategy).
 */
class Player {

public:

    /// Create a player with a rack drawn from the bag.
    Player(const std::string& name,
           std::shared_ptr<Board> board,
           std::shared_ptr<TileBag> tilebag,
           std::shared_ptr<Lexicon> lexicon);

    /// Create a player with the given rack.
    Player(const std::string& name,
           std::shared_ptr<Board> board,
           std::shared_ptr<TileBag> tilebag,
           std::shared_ptr<Lexicon> lexicon,
           const Rack& rack);

    virtual ~Player() {}

    // Deleted copy/move construction/assignment.

    Player(const Player&) = delete;
    Player(Player&& p) = delete;
    Player& operator= (const Player&) = delete;
    Player& operator= (Player&& other) = delete;

    /// Reset this player to its default state for use in a new game.
    void Reset();

    /// Returns this player's current rack.
    Rack GetRack() const {
        return rack_;
    }

    /// Sets this player's rack.
    void SetRack(const Rack& rack) {
        rack_ = rack;
    }

    /// Returns true if this player's rack is empty, false otherwise.
    bool IsRackEmpty() const {
        return rack_.empty();
    }

    /// Returns this player's name.
    std::string GetName() const {
        return name_;
    }

    /// Set's this player's name (but not their nickname).
    void SetName(const std::string& name) {
        name_ = name;
    }

    /// Returns this player's nickname which is typically their first name.
    std::string GetNickname() const {
        return nickname_;
    }

    /// Sets this player's nickname.
    void SetNickname(const std::string& nickname) {
        nickname_ = nickname;
    }

    /// Returns this player's current score.
    int GetScore() const {
        return score_;
    }

    /// Returns the game that this player is a part of or nullptr if not in one.
    const Game* GetGame() const {
        return game_;
    }

    /// Sets this player's strategy.
    /**
     * The template parameter Strat must be a subclass of Strategy, with a
     * construct whose first parameter is a Player pointer.
     * @param args the arguments passed to the strategy constructor of the
     * strategy.
     */
    template<typename Strat, typename... Args>
    void SetStrategy(Args&&... args) {
        strategy_ = std::unique_ptr<Strategy>(new Strat(std::forward<Args>(args)...));
        strategy_->SetPlayer(this);
    }

    template<version_no_t Major, version_no_t Minor, typename... Args>
    void SetStrategy(Args&&... args) {
        SetStrategy<Nigel<Major, Minor>>(std::forward<Args>(args)...);
    }

    void SetStrategy(version_no_t major, version_no_t minor);

    /// Returns the board this player is playing on.
    std::shared_ptr<Board> GetBoard() const {
        return board_;
    }

    /// Returns the tile bag this player is drawing from.
    std::shared_ptr<TileBag> GetTileBag() const {
        return tilebag_;
    }

    /// Returns the lexicon this player is using to make moves.
    std::shared_ptr<Lexicon> GetLexicon() const {
        return lexicon_;
    }

    /// Plays the specified move for the player.
    /**
     * This plays the move for the player, placing the move on the board and
     * updating the player's rack (including drawing new tiles).
     * @param move the move to play on the player's behalf
     * @return a copy of the played move that is the player's copy and has the
     * move's player set to this player
     */
    std::shared_ptr<Move> Play(std::shared_ptr<Move> move);

    /// Gets this player's moves on the board without evaluating for utility.
    /**
     * This simply consults the board using this player's rack, and returns
     * all the plays that can be made by the player on the board (does not
     * include swaps). This player's strategy is not consulted to assign the
     * utilities of the plays.
     * @return the moves that can be made on the board by the player
     */
    std::vector<std::shared_ptr<Move>> RawMovesOnBoard() const;

    /// Returns all possible moves that can be made by this player.
    /**
     * The reutrned plays will include all swaps and the passing play, and will
     * be assigned their respective utilities based on the player's strategy.
     * @return all moves that can be made by the player
     */
    std::vector<std::shared_ptr<Move>> Moves();

    /// Plays this Player's best move
    std::shared_ptr<Move> MakeBestMove();

    std::shared_ptr<Move> LastMove() {
        if(played_moves_.empty())
            throw std::out_of_range("No previous moves");
        return played_moves_.back();
    }

    void PrintPragma(int player_num, std::ostream& os) const;

    friend Game;

private:
    static const int kMinTilebagSizeSwapThreshold;

    std::string name_;
    std::string nickname_;

    std::shared_ptr<Board> board_;
    std::shared_ptr<TileBag> tilebag_;
    std::shared_ptr<Lexicon> lexicon_;

    Rack rack_;
    std::vector<std::shared_ptr<Move>> played_moves_;

    int score_;

    std::unique_ptr<Strategy> strategy_;
    Game* game_;

    Player(const std::string& name, Game* game_);

    void GenerateSwapsAndUtilities(std::vector<std::shared_ptr<Move>>& moves);
};

}

#endif
