#ifndef NIGEL_EMSCRIPTEN_BINDINGS_H
#define NIGEL_EMSCRIPTEN_BINDINGS_H


#include <emscripten/bind.h>

#include "Game.h"
#include "Tile.h"
#include "Alphabet.h"
#include "Board.h"
#include "Lexicon.h"
#include "Move.h"

EMSCRIPTEN_BINDINGS(nigel) {
    using AConfig = nigel::Alphabet::Config;
    emscripten::enum_<AConfig>("Alphabet")
        .value("ENGLISH",     AConfig::ENGLISH)
        .value("ENGLISH_SS",  AConfig::ENGLISH_SS)
        .value("ENGLISH_WWF", AConfig::ENGLISH_WWF)
        .value("FRENCH",      AConfig::FRENCH)
        .value("ITALIAN",     AConfig::ITALIAN)
        ;

    using BConfig = nigel::Board::Config;
    emscripten::enum_<BConfig>("Board")
        .value("SCRABBLE",       BConfig::SCRABBLE)
        .value("SUPER_SCRABBLE", BConfig::SUPER_SCRABBLE)
        .value("WWF",            BConfig::WWF)
        ;

    using LConfig = nigel::Lexicon::Config;
    emscripten::enum_<LConfig>("Lexicon")
        .value("CSW12",   LConfig::CSW12)
        .value("ODS4",    LConfig::ODS4)
        .value("ODS5",    LConfig::ODS5)
        .value("OSPD4",   LConfig::OSPD4)
        .value("OSWI",    LConfig::OSWI)
        .value("SOWPODS", LConfig::SOWPODS)
        .value("TWL06",   LConfig::TWL06)
        .value("TWL98",   LConfig::TWL98)
        .value("WWF",     LConfig::WWF)
        .value("ZINGA05", LConfig::ZINGA05)
        ;

    using GConfig = nigel::Game::Config;
    emscripten::enum_<GConfig>{"GameConfig"}
        .value("SCRABBLE",         GConfig::SCRABBLE)
        .value("SCRABBLE_FRENCH",  GConfig::SCRABBLE_FRENCH)
        .value("SCRABBLE_ITALIAN", GConfig::SCRABBLE_ITALIAN)
        .value("SUPER_SCRABBLE",   GConfig::SUPER_SCRABBLE)
        .value("WWF",              GConfig::WWF)
        ;

    emscripten::class_<nigel::Tile>("Tile")
        .constructor<char, int>()
        .property("score",   &nigel::Tile::GetScore, &nigel::Tile::SetScore)
        .property("letter",  &nigel::Tile::GetLetter, &nigel::Tile::SetLetter)
        .function("IsBlank", &nigel::Tile::IsBlank)
        .function("IsEmpty", &nigel::Tile::IsEmpty)
        .function("Clear",   &nigel::Tile::Clear)
        ;

    emscripten::value_object<nigel::Coord>("Coord")
        .field("row", &nigel::Coord::row)
        .field("col", &nigel::Coord::col)
        ;

    emscripten::value_object<nigel::MoveCoord>("MoveCoord")
        .field("row",   &nigel::Coord::row)
        .field("col",   &nigel::Coord::col)
        .field("horiz", &nigel::MoveCoord::horiz)
        ;

    emscripten::class_<nigel::Move>("Move")
        .smart_ptr<std::shared_ptr<nigel::Move>>("Move")
        .function("Empty", &nigel::Move::empty)
        .function("GetWord", &nigel::Move::GetWord)
        .function("GetScore", &nigel::Move::GetScore)
        // .function("GetCoord", &nigel::Move::GetCoord)
        .function("GetLeave", &nigel::Move::GetLeave)
        .function("GetPlaced", &nigel::Move::GetPlaced)
        ;

    emscripten::class_<nigel::Game>("Game")
        // Static constructors
        .class_function("FromConfig", &nigel::Game::FromConfig)
        .class_function("FromParts", &nigel::Game::FromParts)
        // Constructor / smart pointer
        .smart_ptr<std::shared_ptr<nigel::Game>>("GameSharedPtr")
        // Player management
        .function("AddPlayer", &nigel::Game::AddPlayer, emscripten::allow_raw_pointers())
        .function("GetPlayer", &nigel::Game::GetPlayer, emscripten::allow_raw_pointers())
        .function("NumPlayers", &nigel::Game::NumPlayers)
        .function("OnTurnPlayer", &nigel::Game::OnTurnPlayer, emscripten::allow_raw_pointers())
        .function("OtherPlayer", &nigel::Game::OtherPlayer, emscripten::allow_raw_pointers())
        .function("Winner", &nigel::Game::Winner, emscripten::allow_raw_pointers())
        // Game management
        .function("IsOver", &nigel::Game::IsOver)
        .function("Reset", &nigel::Game::Reset)
        .function("MakeNextMove", &nigel::Game::MakeNextMove)
        .function("LastMove", &nigel::Game::LastMove)
        ;
}

#endif
