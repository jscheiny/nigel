/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Version.h
 * @brief Nigel versioning meta data.
 */

#ifndef VERSIONING_H
#define VERSIONING_H

#include "Strategy.h"
#include "Utility.h"

#include <map>
#include <tuple>
#include <type_traits>

namespace nigel {

using version_no_t = unsigned short;

template<version_no_t Major, version_no_t Minor, typename... Args>
std::unique_ptr<Strategy> MakeStrategy(Args&&... args);

std::unique_ptr<Strategy> MakeStrategy(version_no_t major, version_no_t minor);

std::unique_ptr<Strategy> MakeStrategy(const std::string& verison);

template<version_no_t Major, version_no_t Minor>
class Nigel;

template<version_no_t Major, version_no_t Minor>
struct NigelRegistry { using type = void; };

template<version_no_t Major, version_no_t Minor>
struct nigel_version_exists : public std::integral_constant<bool,
        !std::is_void<
            typename NigelRegistry<Major, Minor>::type
        >::value
    > {};

namespace impl {

class RuntimeVersions {

public:
    using version_result_t = std::unique_ptr<Strategy>;

    RuntimeVersions(const RuntimeVersions&) = delete;
    RuntimeVersions(RuntimeVersions&&) = delete;
    RuntimeVersions& operator= (const RuntimeVersions&) = delete;
    RuntimeVersions& operator= (RuntimeVersions&&) = delete;

    template<version_no_t Major, version_no_t Minor>
    static void Insert() {
        Instance().versions_.emplace(std::make_tuple(Major, Minor), [] {
            return MakeStrategy<Major, Minor>();
        });
    }

    static version_result_t MakeVersion(version_no_t major, version_no_t minor);

private:
    using version_tuple_t = std::tuple<version_no_t, version_no_t>;
    using version_fn_t = std::function<version_result_t()>;
    using version_map_t = std::map<version_tuple_t, version_fn_t>;

    static RuntimeVersions& Instance();

    RuntimeVersions() {}

    version_map_t versions_;
};

}

struct VersionError : public Error {
    VersionError(version_no_t major, version_no_t minor);
    VersionError(const std::string& version);
};

template<version_no_t Major, version_no_t Minor, typename... Args>
std::unique_ptr<Strategy> MakeStrategy(Args&&... args) {
    static_assert(nigel_version_exists<Major, Minor>::value,
                  "No such version of nigel strategy exists.");
    return std::unique_ptr<Strategy>(
        new Nigel<Major, Minor>(std::forward<Args>(args)...));
}

#define NIGEL_DEFINE_STRATEGY(Major, Minor)                                    \
    template<> class Nigel< Major , Minor >;                                   \
    template<> struct NigelRegistry< Major , Minor > {                         \
        using type = Nigel< Major , Minor >;                                   \
    };                                                                         \
    template<> class Nigel< Major , Minor >


}

#endif
