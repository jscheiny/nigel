/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Move.h
 * @brief Declaration of the Move class and its subclasses.
 */

#ifndef MOVE_H
#define MOVE_H

#include "Coordinate.h"
#include "Rack.h"

#include <memory>
#include <iosfwd>
#include <vector>

namespace nigel {

class Tile;
class Player;

class Move {

public:

    Move() : empty_(true) {};

    Move(const std::string& word,
         const Rack& leave,
         const Rack& placed,
         const Rack& tiles_on_board,
         int row, int col, bool horiz,
         int score);

    Move(const std::string& word,
         const Rack& leave,
         const Rack& placed,
         const Rack& tiles_on_board,
         const MoveCoord& coord,
         int score);

    virtual ~Move() {}

    bool empty() const {
        return empty_;
    }

    std::string GetWord() const {
        return word_;
    }

    int GetScore() const {
        return score_;
    }

    MoveCoord GetCoord() const {
        return MoveCoord(row_, col_, horiz_);
    }

    const Rack& GetLeave() const {
        return leave_;
    }

    const Rack& GetPlaced() const {
        return placed_;
    }

    double GetRawUtility() const {
        return raw_utility_;
    }

    void SetRawUtility(double utility) {
        raw_utility_ = utility;
    }

    int GetScoreAfterPlay() const {
        return score_after_play_;
    }

    void SetScoreAfterPlay(int score_after_play) {
        score_after_play_ = score_after_play;
    }

    double GetUtility() const {
        return score_ + raw_utility_;
    }

    void SetPlayer(Player* player) {
        player_ = player;
        UpdateBlank();
    }

    const Player* GetPlayer() const {
        return player_;
    }

    virtual bool IsSwap() const { return false; }

    virtual bool IsMoveOnBoard() const { return true; }

    virtual bool IsPlayerActionRequired() const { return true; }

    virtual std::shared_ptr<Move> Clone() const;

    void Print(std::ostream& os) const;

    bool HasNote() const { return !note_.empty(); }

    std::string GetNote() const { return note_; }

    void SetNote(const std::string& note) { note_ = note; }

    bool operator< (const Move& other) const;

    static bool ComparePtrs(const std::shared_ptr<Move> a,
                             const std::shared_ptr<Move> b);

    static bool ComparePtrsDesc(const std::shared_ptr<Move> a,
                                const std::shared_ptr<Move> b);

protected:
    virtual void PrintMoveDetails(std::ostream& os) const;

    void PrintPlayerInfo(std::ostream& os) const;

    void PrintPlayerRack(std::ostream& os) const;

    void PrintScoreInfo(std::ostream& os) const;

    char blank_character() const { return blank_character_; }

private:
    bool empty_;
    std::string word_;

    Rack leave_;
    Rack placed_;
    Rack tiles_on_board_;

    int row_;
    int col_;
    bool horiz_;

    int score_;
    double raw_utility_;

    Player* player_;
    int score_after_play_;

    std::string note_;
    char blank_character_='?';

    void UpdateBlank();
};

class Swap : public Move {

public:
    Swap(const Rack& leave, const Rack& swapped);

    ~Swap() {}

    Swap(const Swap&) = default;
    Swap(Swap&&) = default;
    Swap& operator= (const Swap&) = default;
    Swap& operator= (Swap&&) = default;

    const Rack& GetSwapped() const { return Move::GetPlaced(); }

    bool IsSwap() const override { return true; }
    bool IsMoveOnBoard() const override { return false; }

    std::shared_ptr<Move> Clone() const override;

protected:
    void PrintMoveDetails(std::ostream& os) const override;
};

class EndGameGain : public Move {

public:
    EndGameGain(const Rack& gained, int multiplier = 2);

    ~EndGameGain() {}

    bool IsMoveOnBoard() const override { return false; }
    bool IsPlayerActionRequired() const override { return false; }

    std::shared_ptr<Move> Clone() const override;

protected:
    void PrintMoveDetails(std::ostream& os) const override;
};

class EndGameLoss : public Move {

public:
    EndGameLoss(const Rack& lost);

    ~EndGameLoss() {}

    bool IsMoveOnBoard() const override { return false; }
    bool IsPlayerActionRequired() const override { return false; }

    std::shared_ptr<Move> Clone() const override;

protected:
    void PrintMoveDetails(std::ostream& os) const override;
};

class TimePenalty : public Move {

public:
    TimePenalty(const Rack& rack, int penalty);

    ~TimePenalty() {}

    bool IsMoveOnBoard() const override { return false; }
    bool IsPlayerActionRequired() const override { return false; }

    std::shared_ptr<Move> Clone() const override;

protected:
    void PrintMoveDetails(std::ostream& os) const override;
};

class WithdrawnPhoney : public Move {

public:
    WithdrawnPhoney(const Rack& rack, int lost_points);

    ~WithdrawnPhoney() {}

    bool IsMoveOnBoard() const override { return false; }
    bool IsPlayerActionRequired() const override { return false; }

    std::shared_ptr<Move> Clone() const;

protected:
    void PrintMoveDetails(std::ostream& os) const override;

};

class ChallengeBonus : public Move {

public:
    ChallengeBonus(const Rack& rack);
    ChallengeBonus(const Rack& rack, int bonus);

    ~ChallengeBonus() {}

    bool IsMoveOnBoard() const override { return false; }
    bool IsPlayerActionRequired() const override { return false; }

    std::shared_ptr<Move> Clone() const;

protected:
    void PrintMoveDetails(std::ostream& os) const override;

};

}

#endif
