/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file GameObserver.h
 * @brief Declaration of the GameObserver class.
 */

#ifndef GAME_OBSERVER_H
#define GAME_OBSERVER_H

#include "Game.h"

namespace nigel {
namespace analyze {

class GameObserver : public Game::ObserverType {

public:
    virtual ~GameObserver() {}

    void Notify(Game::SubjectType s,
                std::shared_ptr<Move> move) override final;
    void Watch(Game* game);
    void Watch(Game& game);

protected:
    virtual void ObserveMove(Game* game, std::shared_ptr<Move> move) = 0;

};

}
}

#endif
