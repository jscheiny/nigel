#include "EndGameStrategy.h"

#include "Move.h"
#include "Game.h"
#include "Board.h"
#include "Player.h"

#include <algorithm>

using namespace std;
using namespace nigel;

const int EndGameStrategy::kThreshold = 30;
const int EndGameStrategy::kNumSubMovesValuated = 20;

void InitializeMoves(const vector<shared_ptr<Move>>& moves);
bool FindWinningMove(const vector<shared_ptr<Move>>& moves, int scoreDiff,
                     const Rack& player_rack, const Rack& opponent_rack);

void EndGameStrategy::Postprocess(vector<shared_ptr<Move>>& moves) {
    const auto& tilebag = GetPlayer()->GetTileBag();
    if(tilebag->IsEmpty()) {
        InitializeMoves(moves);

        const Player* opponent = GetPlayer()->GetGame()->OtherPlayer(GetPlayer());
        int score_diff = GetPlayer()->GetScore() - opponent->GetScore();
        Rack opponent_rack = tilebag->GetAlphabet()->RemainingTiles(
            *(GetPlayer()->GetBoard()), GetPlayer()->GetRack());

        if(FindWinningMove(moves, score_diff, GetPlayer()->GetRack(), opponent_rack)) {
            return;
        }

        for(int index = 0; index < moves.size() and index < kThreshold; index++) {

        }

    }
}

void InitializeMoves(const vector<shared_ptr<Move>>& moves) {
    for(auto move : moves) {
        move->SetRawUtility(-move->GetScore());
    }
}

class IsWinningMove {
public:
    IsWinningMove(const Rack& player_rack, const Rack& opponent_rack, int score_diff) :
        player_rack_size_(player_rack.size()),
        opponent_rack_score_(opponent_rack.Score()),
        score_diff_(score_diff) {}

    bool operator() (shared_ptr<Move> move) {
        return move->GetPlaced().size() == player_rack_size_ and
               score_diff_ + move->GetScore() + 2 * opponent_rack_score_ > 0;
    }

private:
    int player_rack_size_;
    int opponent_rack_score_;
    int score_diff_;

};

bool FindWinningMove(const vector<shared_ptr<Move>>& moves, int score_diff,
                     const Rack& playerRack, const Rack& opponent_rack) {
    auto winning_move = find_if(moves.begin(), moves.end(),
            IsWinningMove(playerRack, opponent_rack, score_diff));
    if(winning_move != moves.end()) {
        (*winning_move)->SetRawUtility((*winning_move)->GetScore());
        return true;
    }
    return false;
}



struct Mover {
    Mover(shared_ptr<Board> board_, shared_ptr<Move> move) :
    board(board_) {
        board->PushTempMove(move);
    }

    ~Mover() {
        board->PopTempMove();
    }

private:
    shared_ptr<Board> board;
};

double EndGameStrategy::ValuateMove(shared_ptr<Move> move,
                                    const Rack& player_rack,
                                    const Rack& opponent_rack,
                                    int score_diff,
                                    int score_modifier,
                                    int consec_skips,
                                    int level) {
    auto board = GetPlayer()->GetBoard();
    Mover mover(board, move);

    // const Rack& new_player_rack = move->GetLeave();

    vector<shared_ptr<Move>> opponent_moves;
    board->FindMoves(opponent_rack, &opponent_moves);
    sort(opponent_moves.begin(), opponent_moves.end(), Move::ComparePtrs);

    for(int index = 0; index < min(static_cast<int>(opponent_moves.size()), kNumSubMovesValuated);
        index++) {
        opponent_moves[index]->Print(cout);
    }

    return 0.0;
}
