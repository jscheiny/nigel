/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file WeakCache.h
 * @brief Declaration and implementation of the WeakCache class.
 */

#ifndef CACHE_H
#define CACHE_H

#include <memory>
#include <map>

template<typename Key, typename Value>
class WeakCache {

public:
    template<typename... Args>
    WeakCache(Args&&... args) : cache_(std::forward<Args>(args)...) {}

    std::shared_ptr<Value> Retrieve(const Key& key);

private:
    std::map<Key, std::weak_ptr<Value>> cache_;

};

template<typename Key, typename Value>
std::shared_ptr<Value> WeakCache<Key, Value>::Retrieve(const Key& key) {
    auto& value = cache_[key];
    auto ptr = value.lock();
    if(ptr) {
        return ptr;
    }
    ptr = std::make_shared<Value>(key);
    value = ptr;
    return ptr;
}

#endif
