/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Game.cpp
 * @brief Implementation of the Game class.
 */

#include "Game.h"

#include "Rack.h"
#include "Move.h"
#include "Player.h"
#include "TileBag.h"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <utility>

using namespace std;
using namespace nigel;

using BCon = Board::Config;
using ACon = Alphabet::Config;
using DCon = Lexicon::Config;

const map<Game::Config, Game::ConfigTuple> Game::kConfigMap {
    {Config::SCRABBLE,         make_tuple(BCon::SCRABBLE, ACon::ENGLISH, DCon::TWL06)},
    {Config::SCRABBLE_FRENCH,  make_tuple(BCon::SCRABBLE, ACon::FRENCH, DCon::ODS5)},
    {Config::SCRABBLE_ITALIAN, make_tuple(BCon::SCRABBLE, ACon::ITALIAN, DCon::ZINGA05)},
    {Config::SUPER_SCRABBLE,   make_tuple(BCon::SUPER_SCRABBLE, ACon::ENGLISH_SS, DCon::TWL06)},
    {Config::WWF,              make_tuple(BCon::WWF, ACon::ENGLISH_WWF, DCon::WWF)}
};

const map<string, Game::PragmaParseMemFn> Game::kPragmaParsers {
    {"#title",       &Game::ParseTitlePragma},
    {"#description", &Game::ParseDescriptionPragma},
    {"#incomplete",  &Game::ParseIncompletePragma},
    {"#note",        &Game::ParseNotePragma},
    {"#id",          &Game::ParseIdPragma}
};


const int Game::kNumConsecSkipsForceEnd = 3;

Game::Game(Config config) : Game(kConfigMap.at(config)) {

}

Game::Game(const ConfigTuple& config)
      : Game(get<0>(config), get<1>(config), get<2>(config)) {

}

Game::Game(Board::Config board_config,
           Alphabet::Config alphabet_config,
           Lexicon::Config lexicon_config)
      : Game(Board::GetConfigPath(board_config),
             Alphabet::GetConfigPath(alphabet_config),
             Lexicon::GetConfigPath(lexicon_config)) {

}

Game::Game(const string& board_setup,
           const string& alphabet_setup,
           const string& lexicon_setup)
      : tilebag_(make_shared<TileBag>(alphabet_setup))
      , lexicon_(make_shared<Lexicon>(lexicon_setup))
      , board_(make_shared<Board>(tilebag_->GetAlphabet(), lexicon_, board_setup))
      , current_turn_(0)
      , game_over_(false)
      , consec_skips_(0) {
}

Game::Game(const string& path, Config config)
      : Game(path, kConfigMap.at(config)) {}

Game::Game(const string& path, const ConfigTuple& config)
      : Game(path, get<0>(config), get<1>(config), get<2>(config)) {}

Game::Game(const string& path,
           Board::Config board_config,
           Alphabet::Config alphabet_config,
           Lexicon::Config lexicon_config)
      : Game(path, Board::GetConfigPath(board_config),
             Alphabet::GetConfigPath(alphabet_config),
             Lexicon::GetConfigPath(lexicon_config)) {}

Game::Game(const string& path,
           const string& board_setup,
           const string& alphabet_setup,
           const string& lexicon_setup)
      : Game(board_setup, alphabet_setup, lexicon_setup) {
    ifstream fin(path);
    if(!fin)
        throw OpenError(path);
    try {
        ReadGcg(fin, board_setup, alphabet_setup, lexicon_setup);
    } catch(InvalidDataError& e) {
        throw InvalidDataError(path);
    }
    fin.close();
}

Game::Game(istream& is, Config config)
      : Game(is, kConfigMap.at(config)) {}

Game::Game(istream& is, const ConfigTuple& tup)
      : Game(is, get<0>(tup), get<1>(tup), get<2>(tup)) {}

Game::Game(istream& is,
           Board::Config board_config,
           Alphabet::Config alphabet_config,
           Lexicon::Config lexicon_config)
      : Game(is, Board::GetConfigPath(board_config),
             Alphabet::GetConfigPath(alphabet_config),
             Lexicon::GetConfigPath(lexicon_config)) {}

Game::Game(istream& is,
           const string& board_setup,
           const string& alphabet_setup,
           const string& lexicon_setup)
      : Game(board_setup, alphabet_setup, lexicon_setup) {
    ReadGcg(is, board_setup, alphabet_setup, lexicon_setup);
}

Game::~Game() {
    for(Player* player : players_)
        delete player;
}

void Game::ReadGcg(istream& is,
                   const string& board_setup,
                   const string& alphabet_setup,
                   const string& lexicon_setup) {
    tilebag_ = make_shared<TileBag>(alphabet_setup);
    lexicon_ = make_shared<Lexicon>(lexicon_setup);
    board_ = make_shared<Board>(tilebag_->GetAlphabet(), lexicon_, board_setup);

    string line;
    ParseData data(tilebag_->GetAlphabet());
    while(getline(is, line)) {
        cout << line << endl;
        line = Strip(line);
        if(line.empty())
            continue;
        if(line[0] == '#') {
            ParsePragma(line, &data);
        } else if(line[0] == '>') {
            ParseEvent(line, &data);
        }
    }
}

void Game::ParsePragma(const string& line, ParseData* data) {
    vector<string> parts = Split(line);

    if(StartsWith(line, "#player")) {
        ParsePlayerPragma(parts, data);
    } else if (StartsWith(line, "#rack")) {
        ParseRackPragma(parts, data);
    } else {
        auto itr = kPragmaParsers.find(parts[0]);
        if(itr != kPragmaParsers.end()) {
            auto fn = itr->second;
            (this->*fn)(parts, data);
        } else {
            throw InvalidDataError();
        }
    }

    // } else if(parts[0] == "#title") {
    //     ParseTitlePragma(parts, data);
    // } else if(parts[0] == "#description") {
    //     ParseDescriptionPragma(parts, data);
    // } else if(parts[0] == "#incomplete") {
    //     ParseIncompletePragma(parts, data);
    // } else if(parts[0] == "#note") {
    //     ParseNotePragma(parts, data);
    // } else if(parts[0] == "#id") {
    //     ParseIdPragma(parts, data);
    // }
}

void Game::ParsePlayerPragma(vector<string>& parts, ParseData* data) {
    int number = 0;
    try {
        number = ConvertToInt(parts[0].substr(7));
    } catch(ConvertError) {
        return;
    }
    if(parts.size() == 1)
        throw InvalidDataError();
    string nickname = parts[1];
    string name = nickname;
    if(parts.size() > 2)
        name = Join(parts.begin() + 2, parts.end());
    while(NumPlayers() < number) {
        AddPlayer("AUTO NAMED PLAYER");
    }
    players_[number - 1]->SetName(name);
    players_[number - 1]->SetNickname(nickname);
}

void Game::ParseRackPragma(vector<string>& parts, ParseData* data) {
    int number = 0;
    try {
        number = ConvertToInt(parts[0].substr(5));
    } catch(ConvertError) {
        return;
    }
    if(parts.size() == 1)
        throw InvalidDataError();
    while(NumPlayers() < number) {
        AddPlayer("AUTO NAMED PLAYER");
    }
    data->specified_racks.emplace(number - 1, parts[1]);
}

void Game::ParseTitlePragma(vector<string>& parts, ParseData* data) {
    if(parts.size() == 1)
        return;
    title_ = Join(parts.begin() + 1, parts.end());
}

void Game::ParseDescriptionPragma(vector<string>& parts, ParseData* data) {
    if(parts.size() == 1)
        return;
    description_ = Join(parts.begin() + 1, parts.end());
}

void Game::ParseIncompletePragma(vector<string>& parts, ParseData* data) {
    if(parts.size() == 1)
        throw InvalidDataError();
    data->incomplete_rack = parts[1];
}

void Game::ParseNotePragma(vector<string>& parts, ParseData* data) {
    if(parts.size() == 1)
        return;
    string note = Join(parts.begin() + 1, parts.end());
    if(!moves_.empty()) {
        moves_.back()->SetNote(note);
    }
}

void Game::ParseIdPragma(vector<string>& parts, ParseData* data) {
    if(parts.size() != 3)
        return;
    idAuth_ = parts[1];
    uniqId_ = parts[2];
}

void Game::ParseEvent(const string& line, ParseData* data) {
    auto parts = Split(line);
    auto nickname = parts[0].substr(1, parts[0].size() - 2);
    Player* player = GetPlayerByNickname(nickname);
    if(!player)
        throw InvalidDataError();
    current_turn_ = GetPlayerIndex(player);
    int score = 0;
    try {
        score = ConvertToInt(parts[parts.size() - 2].substr(1));
    } catch(ConvertError) {
        throw InvalidDataError();
    }

    if(parts[parts.size() - 2][0] == '-')
        score = -score;

    shared_ptr<Move> move;
    if(parts.size() == 6) { // regular move
        Rack rack(parts[1], data->alphabet);
        MoveCoord coord(parts[2]);

    } else if(parts.size() == 5) { // swap or passing play or end game loss
        Rack rack(parts[1], data->alphabet);
        if(parts[2] == "(time)") {
            move = make_shared<TimePenalty>(rack, score);
        } else if(parts[2] == "challenge") {
            move = make_shared<ChallengeBonus>(rack, score);
        } else if(StartsWith(parts[2], "(") && EndsWith(parts[2], ")")) { // end game loss
            move = make_shared<EndGameLoss>(rack);
        } else if(parts[2] == "--") {
            move = make_shared<WithdrawnPhoney>(rack, score);
        } else if(parts[2][0] == '-') {
            Rack swapped(parts[2].substr(1), data->alphabet);
            rack.Remove(swapped);
            move = make_shared<Swap>(rack, swapped);
        }

    } else if(parts.size() == 4) {
        Rack rack(parts[1].substr(1, parts[1].size() - 2), data->alphabet);
        int rack_score = rack.Score();
        int multiplier = score / rack_score;
        move = make_shared<EndGameGain>(rack, multiplier);
    } else {
        throw InvalidDataError();
    }
}

Player* Game::GetPlayerByNickname(const string& nickname) {
    for(Player* player : players_) {
        if(player->GetNickname() == nickname)
            return player;
    }
    auto upperNickname = Upper(nickname);
    for(Player* player : players_) {
        if(Upper(player->GetNickname()) == upperNickname)
            return player;
    }
    return nullptr;
}

int Game::GetPlayerIndex(const Player* player) const {
    for(int index = 0; index < players_.size(); index++) {
        if(players_[index] == player)
            return index;
    }
    return -1;
}

Player* Game::AddPlayer(const string& name) {
    Player* player = new Player(name, this);
    players_.push_back(player);
    return player;
}

void Game::RotatePlayers() {
    Player* last = players_.back();
    players_.pop_back();
    players_.insert(players_.begin(), last);
}

void Game::Reset() {
    board_->Clear();
    tilebag_->Refill();
    moves_.clear();
    current_turn_ = 0;
    game_over_ = false;
    consec_skips_ = 0;
    for(Player* player : players_)
        player->Reset();
}

const Player* Game::OtherPlayer(const Player* player) const {
    if(NumPlayers() != 2)
        throw Error("Game must have two players");
    if(player == players_[0])
        return players_[1];
    if(player == players_[1])
        return players_[0];
    throw Error("Player not in the game");
}

Player* Game::Winner() const {
    if(!game_over_)
        throw Error("Game not over");

    int tie_score = players_.front()->GetScore();
    bool tie = true;

    int max_score = tie_score;
    Player* max_player = players_.front();

    for(auto itr = players_.begin() + 1; itr != players_.end(); ++itr) {
        Player* player = *itr;
        if(player->GetScore() != tie_score)
            tie = false;
        if(player->GetScore() > max_score) {
            max_score = player->GetScore();
            max_player = player;
        }
    }
    if(tie)
        return nullptr;
    return max_player;
}

shared_ptr<Move> Game::MakeNextMove() {
    Player* on_turn = players_[current_turn_];
    shared_ptr<Move> move = on_turn->MakeBestMove();
    moves_.push_back(move);
    if(move->IsSwap()) {
        consec_skips_++;
    } else {
        consec_skips_ = 0;
    }

    if(consec_skips_ == NumPlayers() * kNumConsecSkipsForceEnd) {
        HandleConsecSkipsEndGame();
    } else if(on_turn->IsRackEmpty()) {
        HandleNormalEndGame();
    } else {
        current_turn_++;
        current_turn_ %= NumPlayers();
    }
    return move;
}

void Game::Play(bool print_moves, bool print_boards) {
    if(NumPlayers() == 0)
        AddPlayer("");

    while(!game_over_) {
        shared_ptr<Move> move = MakeNextMove();
        NotifyObservers(move);
        if(print_moves)
            move->Print(cout);
        if(print_boards)
            board_->PrintWithColor(cout);
    }
}

void Game::HandleConsecSkipsEndGame() {
    game_over_ = true;
    for(Player* player : players_) {
        shared_ptr<EndGameLoss> loss = make_shared<EndGameLoss>(player->GetRack());
        moves_.push_back(player->Play(loss));
    }
}

void Game::HandleNormalEndGame() {
    Player* last_player = players_[current_turn_];
    game_over_ = true;
    if(NumPlayers() == 2) {
        Player* other_player = players_[(current_turn_ + 1) % 2];
        Rack rack = other_player->GetRack();

        shared_ptr<EndGameGain> gain = make_shared<EndGameGain>(rack);
        moves_.push_back(last_player->Play(gain));

    } else if(NumPlayers() > 2) {
        Rack gained_tiles;
        for(Player* player : players_) {
            if(player != last_player) {
                Rack rack = player->GetRack();
                shared_ptr<EndGameLoss> loss = make_shared<EndGameLoss>(rack);
                moves_.push_back(player->Play(loss));

                copy(rack.begin(), rack.end(), back_inserter(gained_tiles));
            }
        }
        shared_ptr<EndGameGain> gain = make_shared<EndGameGain>(gained_tiles, 1);
        moves_.push_back(last_player->Play(gain));
    }
}

void Game::SaveGcg(const string& path) {
    ofstream fout(path);
    if(!fout)
        throw OpenError(path);
    SaveGcg(fout);
    fout.close();
}

void Game::SaveGcg(ostream& os) {
    for(int index = 0; index < players_.size(); index++) {
        players_[index]->PrintPragma(index + 1, os);
    }

    if(!title_.empty())
        os << "#title " << title_ << '\n';
    if(!description_.empty())
        os << "#description " << description_ << '\n';
    if(!idAuth_.empty() and !uniqId_.empty())
        os << "#id " << idAuth_ << " " << uniqId_ << '\n';

    for(auto move : moves_) {
        move->Print(os);
    }
}
