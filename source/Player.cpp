/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Player.cpp
 * @brief Implementation of the Player class.
 */

#include "Player.h"
#include "Nigel.h"
#include "Move.h"
#include "Board.h"
#include "Game.h"

#include <algorithm>
#include <iterator>
#include <iostream>
#include <cctype>
#include <list>
#include <map>
#include <ctime>

using namespace std;
using namespace nigel;

string DefaultNickname(const string& name);

using UtilityMap = map<Rack, double>;

const int Player::kMinTilebagSizeSwapThreshold = 7;

Player::Player(const string& name,
               shared_ptr<Board> board,
               shared_ptr<TileBag> tilebag,
               shared_ptr<Lexicon> lexicon)
      : Player(name, board, tilebag, lexicon, tilebag->Draw(7)) {
}

Player::Player(const string& name,
               shared_ptr<Board> board,
               shared_ptr<TileBag> tilebag,
               shared_ptr<Lexicon> lexicon,
               const Rack& rack)
      : name_(name)
      , nickname_(DefaultNickname(name))
      , board_(board)
      , tilebag_(tilebag)
      , lexicon_(lexicon)
      , rack_(rack)
      , score_(0)
      , strategy_(nullptr)
      , game_(nullptr) {
    SetStrategy<Nigel<1,0>>();
}

Player::Player(const string& name, Game* game)
      : Player(name, game->GetBoard(), game->GetTileBag(), game->GetLexicon()) {
    game_ = game;
}

string DefaultNickname(const string& name) {
    auto itr = name.begin();
    while(itr != name.end() && !isspace(*itr))
        ++itr;
    return string(name.begin(), itr);
}

void Player::SetStrategy(version_no_t major, version_no_t minor) {
    strategy_ = MakeStrategy(major, minor);
    strategy_->SetPlayer(this);
}

void Player::Reset() {
    played_moves_.clear();
    rack_ = tilebag_->Draw(7);
    score_ = 0;
}

shared_ptr<Move> Player::Play(shared_ptr<Move> move) {
    if(!move)
        throw Error("Move must not be null");

    if(move->IsMoveOnBoard()) {
        const Rack& placed = move->GetPlaced();
        rack_.Remove(placed);
        Rack drawn = tilebag_->Draw(placed.size());
        copy(drawn.begin(), drawn.end(), back_inserter(rack_));
    } else if(move->IsSwap() && tilebag_->Size() > 7) {
        const Rack& swapped = dynamic_pointer_cast<Swap>(move)->GetSwapped();
        rack_.Remove(swapped);
        Rack drawn = tilebag_->Swap(swapped);
        copy(drawn.begin(), drawn.end(), back_inserter(rack_));
    }

    board_->Play(move);
    score_ += move->GetScore();

    shared_ptr<Move> player_copy(move->Clone());
    player_copy->SetPlayer(this);
    player_copy->SetScoreAfterPlay(score_);
    played_moves_.push_back(player_copy);

    return player_copy;
}

shared_ptr<Move> Player::MakeBestMove() {
    vector<shared_ptr<Move>> moves;
    board_->FindMoves(rack_, &moves);

    strategy_->Preprocess(moves);
    GenerateSwapsAndUtilities(moves);
    strategy_->Postprocess(moves);

    if(moves.empty()) {
        shared_ptr<Swap> pass(make_shared<Swap>(rack_, Rack()));
        shared_ptr<Move> player_copy = Play(pass);
        return player_copy;
    } else {
        shared_ptr<Move> best = *max_element(moves.begin(), moves.end(),
                                             Move::ComparePtrs);
        shared_ptr<Move> player_copy_best = Play(best);
        return player_copy_best;
    }
}

vector<shared_ptr<Move>> Player::RawMovesOnBoard() const {
    vector<shared_ptr<Move>> moves;
    board_->FindMoves(rack_, &moves);
    return moves;
}

vector<shared_ptr<Move>> Player::Moves() {
    vector<shared_ptr<Move>> moves;
    board_->FindMoves(rack_, &moves);
    strategy_->Preprocess(moves);
    GenerateSwapsAndUtilities(moves);
    strategy_->Postprocess(moves);
    return moves;
}

class InsertSubset {
public:
    InsertSubset(UtilityMap& leave_utility_map,
                  vector<Rack>& subsets,
                  std::unique_ptr<Strategy>& strategy)
        : leave_utility_map_(leave_utility_map)
        , subsets_(subsets)
        , strategy_(strategy) {}

    template<typename Iter>
    void operator() (Iter begin, Iter end) {
        Rack subset(begin, end);
        subset.Sort();
        if(leave_utility_map_.count(subset) == 0) {
            leave_utility_map_.emplace(subset, strategy_->Utility(subset));
            subsets_.push_back(subset);
        }
    }

private:
    UtilityMap& leave_utility_map_;
    vector<Rack>& subsets_;
    std::unique_ptr<Strategy>& strategy_;

};

void Player::GenerateSwapsAndUtilities(vector<shared_ptr<Move>>& moves) {
    UtilityMap leave_utility_map;
    vector<Rack> subsets;

    ForEachSubset(rack_.begin(), rack_.end(), InsertSubset(leave_utility_map, subsets, strategy_));

    for(shared_ptr<Move> move : moves) {
        move->SetRawUtility(leave_utility_map[move->GetLeave()]);
    }
    if(tilebag_->Size() >= kMinTilebagSizeSwapThreshold) {
        rack_.Sort();
        for(const Rack& leave : subsets) {
            Rack swapped;
            set_difference(rack_.begin(), rack_.end(), leave.begin(), leave.end(),
                           back_inserter(swapped));
            shared_ptr<Swap> swap = make_shared<Swap>(leave, swapped);
            swap->SetRawUtility(leave_utility_map[leave]);
            moves.push_back(swap);
        }
    } else {
        shared_ptr<Swap> pass(make_shared<Swap>(rack_, Rack()));
        pass->SetRawUtility(leave_utility_map[rack_]);
        moves.push_back(pass);
    }
}

void Player::PrintPragma(int player_num, ostream& os) const {
    os << "#player" << player_num << " " << nickname_ << " " << name_ << endl;
}
