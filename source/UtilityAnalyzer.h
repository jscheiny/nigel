/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file UtilityAnalyzer.h
 * @brief Declaration of the UtilityAnalyzer class.
 */

#ifndef UTILITY_ANALYZER_H
#define UTILITY_ANALYZER_H

#include "GameObserver.h"
#include "RunningStats.h"

#include <map>
#include <string>
#include <iostream>

namespace nigel {
namespace analyze {

class UtilityAnalyzer : public GameObserver {

public:
    using KLetterMap = std::map<std::string, RunningStats>;
    using UtilityMap = std::map<size_t, KLetterMap>;

    virtual ~UtilityAnalyzer() {}

    void Save(size_t k, std::ostream& os) const;
    void SaveAll(const std::string& directory) const;

protected:
    void ObserveMove(Game* game, std::shared_ptr<Move> move) override;

private:
    UtilityMap utilities_;

};

}
}

#endif
