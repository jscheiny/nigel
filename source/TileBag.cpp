/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file TileBag.cpp
 * @brief Implementation of the TileBag class.
 */

#include "TileBag.h"

#include "Tile.h"
#include "Rack.h"
#include "Board.h"
#include "Utility.h"

#include <algorithm>
#include <fstream>
#include <cstdlib>
#include <chrono>

using namespace std;
using namespace nigel;

auto getCurrentTime = []() {
    return chrono::system_clock::now().time_since_epoch().count();
};

TileBag::TileBag(Alphabet::Config config)
      : TileBag(Alphabet::Create(config)) {}

TileBag::TileBag(istream& is)
      : TileBag(make_shared<Alphabet>(is)) {}

TileBag::TileBag(const string& path)
      : TileBag(make_shared<Alphabet>(path)) {}

TileBag::TileBag(shared_ptr<Alphabet> alphabet)
      : alphabet_(alphabet)
      , random_engine_(getCurrentTime())
      , full_size_(0) {
    ComputeFullBagSize();
    Refill();
}

TileBag::TileBag(shared_ptr<Alphabet> alphabet, const Board& board)
      : alphabet_(alphabet)
      , random_engine_(getCurrentTime())
      , full_size_(0) {
    ComputeFullBagSize();
    RefillFromBoard(board);
}

void TileBag::ComputeFullBagSize() {
    for(char letter : alphabet_->GetLetters()) {
        full_size_ += alphabet_->TileFrequency(letter);
    }
}

Rack TileBag::Draw(int number) {
    Rack drawn;
    for(int i = 0; i < number && bag_.size() > 0; i++) {
        drawn.push_back(bag_.back());
        bag_.pop_back();
    }
    return drawn;
}

Rack TileBag::Swap(const Rack& to_swap) {
    if(bag_.size() < to_swap.size()) {
        return Rack();
    }
    Rack drawn = Draw(to_swap.size());
    for(const Tile& tile : to_swap) {
        bag_.push_back(tile);
    }
    Shuffle();
    return drawn;
}

void TileBag::Refill() {
    bag_.clear();
    for(char letter : alphabet_->GetLetters()) {
        int frequency = alphabet_->TileFrequency(letter);
        for(int i = 0; i < frequency; i++) {
            bag_.push_back(alphabet_->CreateTile(letter));
        }
    }
    Shuffle();
}

void TileBag::RefillFromBoard(const Board& board) {
    if(board.IsEmpty()) {
        Refill();
    } else {
        bag_.clear();
        map<char, int> board_tile_freqs;
        for(int row = 0; row < board.GetRows(); row++) {
            for(int col = 0; col < board.GetColumns(); col++) {
                const Tile& tile = board(row,col);
                if(!tile.IsEmpty()) {
                    board_tile_freqs[tile.GetLetter()]++;
                }
            }
        }
        for(char letter : alphabet_->GetLetters()) {
            int frequency = alphabet_->TileFrequency(letter);
            if(board_tile_freqs.count(letter) != 0)
                frequency -= board_tile_freqs[letter];
            for(int i = 0; i < frequency; i++) {
                bag_.push_back(alphabet_->CreateTile(letter));
            }
        }
        Shuffle();
    }
}

void TileBag::Shuffle() {
    shuffle(bag_.begin(), bag_.end(), random_engine_);
}
