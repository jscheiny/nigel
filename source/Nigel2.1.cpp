/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Nigel2.1.h"
#include "Player.h"
#include "Rack.h"

#include <sstream>
#include <fstream>

using namespace nigel;
using namespace std;

string ConcatTiles(Tile a, Tile b, Tile c);

// Nigel 2.1 constants
const LinearFunction Nigel<2,1>::kThreeFromFourSubsetsFn(1.5161, -17.693);

double Nigel<2,1>::Utility(Rack& leave) {
    using Resources = Nigel21Resources;

    if(GetPlayer()->GetTileBag()->IsEmpty())
        return -2 * leave.Score();

    string rackString = leave.ToString();

    if(leave.size() >= 1 or leave.size() <= 3)
        return Resources::Instance().GetExpectation(rackString);

    if(leave.size() == 4) {
        if(Resources::Instance().HasExpectation(rackString)) {
            return Resources::Instance().GetExpectation(rackString);
        } else {
            vector<string> subsets = Get3From4Subsets(leave);
            double total = 0;
            for(const string& sub : subsets) {
                total += Resources::Instance().GetExpectation(sub);
            }
            total /= 4;
            return kThreeFromFourSubsetsFn(total);
        }
    }

    if(leave.size() == 5) {
        if(Resources::Instance().HasExpectation(rackString)) {
            return Resources::Instance().GetExpectation(rackString);
        } else {
            return Resources::Instance().GetExpectation(Vcb(leave));
        }
    }

    if(leave.size() == 6)
        return Resources::Instance().GetExpectation(Vcb(leave));

    return 34.76;
}

Vcb::Vcb(const Rack& tiles) : Vcb(tiles.begin(), tiles.end()) {}

bool Vcb::operator== (const Vcb& other) const {
    return vowels == other.vowels &&
           consonants == other.consonants &&
           blanks == other.blanks;
}

size_t Vcb::Hash::operator() (const Vcb& entry) const {
    stringstream ss;
    ss << entry.vowels << "|" << entry.consonants << "|" << entry.blanks;
    return hash<string>()(ss.str());
}

vector<string> Nigel<2,1>::Get3From4Subsets(Rack& leave) {
    vector<string> ret;
    ret.push_back(ConcatTiles(leave[0], leave[1], leave[2]));
    ret.push_back(ConcatTiles(leave[0], leave[1], leave[3]));
    ret.push_back(ConcatTiles(leave[0], leave[2], leave[3]));
    ret.push_back(ConcatTiles(leave[1], leave[2], leave[3]));
    return ret;
}

string ConcatTiles(Tile a, Tile b, Tile c) {
    string s;
    s += a.GetLetter();
    s += b.GetLetter();
    s += c.GetLetter();
    return s;
}

Nigel21Resources::Nigel21Resources() {
    ReadExpectations(NIGEL_RESOURCE("expectations/1.txt"));
    ReadExpectations(NIGEL_RESOURCE("expectations/2.txt"));
    ReadExpectations(NIGEL_RESOURCE("expectations/3.txt"));
    ReadExpectations(NIGEL_RESOURCE("expectations/4.txt"));
    ReadExpectations(NIGEL_RESOURCE("expectations/5.txt"));

    ReadVcbExpectations(NIGEL_RESOURCE("expectations/4vc.txt"));
    ReadVcbExpectations(NIGEL_RESOURCE("expectations/5vc.txt"));
    ReadVcbExpectations(NIGEL_RESOURCE("expectations/6vc.txt"));
}

void Nigel21Resources::ReadExpectations(const string& path) {
    string line;
    ifstream fin(path);
    while(getline(fin, line)) {
        vector<string> parts = Split(line);
        string key = Upper(parts[0]);
        double value = atof(parts[1].c_str());

        expectations_.emplace(key, value);
    }
    fin.close();
}

void Nigel21Resources::ReadVcbExpectations(const string& path) {
    string line;
    ifstream fin(path);
    while(getline(fin, line)) {
        int v = atoi(line.substr(0,1).c_str());
        int c = atoi(line.substr(2,1).c_str());
        int b = atoi(line.substr(4,1).c_str());
        double value = atof(line.substr(6).c_str());

        vcb_expectations_.emplace(Vcb(v,c,b), value);
    }
    fin.close();
}
