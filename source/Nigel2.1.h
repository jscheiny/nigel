/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NIGEL_2_1_H
#define NIGEL_2_1_H

#include "Nigel1.h"
#include "Utility.h"
#include "Tile.h"

#include <unordered_map>

namespace nigel {

/// A strategy that defines utilities via the Nigel 2.1 expectation valuations.
NIGEL_DEFINE_STRATEGY(2,1) : public Nigel<1,0> {

public:
    double Utility(Rack& leave) override;

private:

    /// A function that transforms the average of the three-letter subsets into
    /// a utility.
    static const LinearFunction kThreeFromFourSubsetsFn;

    /// Returns the set of all 3 letter subsets of the leave of length 4.
    /**
     * @param leave the leave from which 3 letter subsets will be extracted,
     * must be of length 4
     * @return all 3 letter subsets of the input as a vector of concatenated
     * tiles
     */
    static std::vector<std::string> Get3From4Subsets(Rack& leave);

};

/// A count of the number of vowels, consonants, and blanks in a leave.
struct Vcb {
    /// Constructs a vcb from the specific values.
    Vcb(int v, int c, int b) : vowels(v), consonants(c), blanks(b) {}

    explicit Vcb(const Rack& tiles);

    /// Constructs a vcb by counting the occurances in a range of iterators.
    template<typename In>
    Vcb(In begin, In end);

    bool operator== (const Vcb& other) const;

    int vowels;
    int consonants;
    int blanks;

    struct Hash {
        size_t operator() (const Vcb& entry) const;
    };
};

class Nigel21Resources {

public:
    static Nigel21Resources& Instance() {
        static Nigel21Resources instance;
        return instance;
    }

    Nigel21Resources(const Nigel21Resources&) = delete;
    Nigel21Resources(Nigel21Resources&&) = delete;
    Nigel21Resources& operator= (const Nigel21Resources&) = delete;
    Nigel21Resources& operator= (Nigel21Resources&&) = delete;

    double GetExpectation(const std::string& entry) { return expectations_[entry]; }
    double GetExpectation(const Vcb& entry) { return vcb_expectations_[entry]; }

    bool HasExpectation(const std::string& entry) {
        return expectations_.count(entry) != 0;
    }

private:
    std::unordered_map<std::string, double> expectations_;
    std::unordered_map<Vcb, double, Vcb::Hash> vcb_expectations_;

    Nigel21Resources();
    void ReadExpectations(const std::string& path);
    void ReadVcbExpectations(const std::string& path);
};

template<typename In>
Vcb::Vcb(In begin, In end) : vowels(0), consonants(0), blanks(0) {
    const static std::string kVowelsRef = "AEIOU";
    for(auto itr = begin; itr != end; ++itr) {
        const Tile& tile = *itr;
        if(tile.IsBlank())
            blanks++;
        else if(kVowelsRef.find(tile.GetLetter()) != std::string::npos)
            vowels++;
        else
            consonants++;
    }
}

}

#endif
