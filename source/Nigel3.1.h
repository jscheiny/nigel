/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NIGEL_3_1_H
#define NIGEL_3_1_H

#include "Nigel2.2.h"
#include "Rack.h"

namespace nigel {

/// A strategy that performs sampling of the bag to perform utility valuations.
NIGEL_DEFINE_STRATEGY(3,1) : public Nigel<1,0> {

public:
    explicit Nigel<3,1>(int sample_size = 200);

    int GetSampleSize() const {
        return sample_size_;
    }

    Nigel<3,1>* SetSampleSize(int sample_size) {
        sample_size_ = sample_size;
        return this;
    }

    void Preprocess(std::vector<std::shared_ptr<Move>>& moves) override;
    double Utility(Rack& leave) override;

protected:
    double SubsetsUtility(const std::string& s);
    virtual double SampleUtility(const std::string& sample);
    std::string SampleRemaining(int size);

    const Rack& GetSampleSpace() const {
        return sample_space_;
    }

private:
    static const LinearFunction kSampleUtilityFn;

    int sample_size_;
    Rack sample_space_;

};

}

#endif
