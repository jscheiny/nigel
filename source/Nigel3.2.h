/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NIGEL_3_2_H
#define NIGEL_3_2_H

#include "Nigel3.1.h"

#include <unordered_set>

namespace nigel {

/// A strategy that acts like Nigel 3.1 but also checks for bingoability.
NIGEL_DEFINE_STRATEGY(3,2) : public Nigel<3,1> {

public:
    explicit Nigel<3,2>(int sample_size = 200);

protected:
    double SampleUtility(const std::string& sample) override;

private:
    static const LinearFunction kHasAnagramFn;
    static const LinearFunction kNoAnagramFn;

    std::unordered_set<std::string> anagrams_;

    void SetupAnagrams();
    bool HasAnagram(const std::string& letters);

};

}

#endif
