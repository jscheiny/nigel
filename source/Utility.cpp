/*
 * Nigel Scrabble artificial intelligence and analysis.
 * Copyright (C) 2013 by Jonah and Daniel Scheinerman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file Utility.cpp
 * @brief Implementation of utility functions.
 */

#include "Utility.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>
#include <regex>

using namespace std;
using namespace nigel;

namespace nigel {

vector<string> Split(const string& input) {
    vector<string> splitting;
    stringstream ss;
    ss << input;
    while(!ss.eof()) {
        string word;
        ss >> word;
        splitting.push_back(word);
    }
    return splitting;
}

string Join(const vector<string>& parts) {
    return Join(parts, " ");
}

string Join(const vector<string>& parts, const string& delimiter) {
    string result;
    for(int i = 0; i < parts.size(); i++) {
        result += parts[i];
        if(i != parts.size() - 1)
            result += delimiter;
    }
    return result;
}

bool EndsWith(const string& str, const string& suffix) {
    if(str.length() < suffix.length())
        return false;
    return str.substr(str.length() - suffix.length()) == suffix;
}

bool StartsWith(const string& str, const string& prefix) {
    if(str.length() < prefix.length())
        return false;
    return str.substr(0, prefix.length()) == prefix;
}

int ConvertToInt(const string& str) {
    istringstream iss(str);
    int result;
    iss >> result;
    if(iss.fail() || !iss.eof())
        throw ConvertError();
    return result;
}

bool FileExists(const string& filename) {
    ifstream ifile(filename);
    return ifile.good();
}

string Upper(const string& str) {
    stringstream ret;
    for(int i = 0; i < str.length(); i++) {
        ret << (char)toupper(str[i]);
    }
    return ret.str();
}

string Strip(const string& str) {
    int size = str.size();
    int startIndex = 0;
    int endIndex = size - 1;
    while(startIndex < size && isspace(str[startIndex]))
        startIndex++;
    while(endIndex >= startIndex && isspace(str[endIndex]))
        endIndex--;
    return str.substr(startIndex, endIndex - startIndex + 1);
}

double unif() {
    return rand() / double(RAND_MAX);
}

int unif(double a, double b) {
    return int((b-a) * unif() + a);
}

}
