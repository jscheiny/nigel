cmake_minimum_required(VERSION 2.8.7)

project(CMakeNigel)
set(Nigel_VERSION_MAJOR 3)
set(Nigel_VERSION_MINOR 2)

# Detect if we're using emscripten
set(EMSCRIPTEN OFF)
if(${CMAKE_CXX_COMPILER} MATCHES ".*/em\\+\\+")
    set(EMSCRIPTEN ON)
endif()

# Get the resource directory
get_filename_component(RESOURCES_DIR "${CMAKE_SOURCE_DIR}" ABSOLUTE)
set(RESOURCES_DIR ${RESOURCES_DIR}/resources)

# Set flags
function(add_cxx_flags extras)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${extras}" PARENT_SCOPE)
endfunction(add_cxx_flags)

add_cxx_flags("-g -Wall -std=c++11 -stdlib=libc++ -Wno-unused-function -pipe")
add_cxx_flags("-DNIGEL_RESOURCES_DIR=${RESOURCES_DIR}")
if(EMSCRIPTEN)
    add_cxx_flags("-s ALLOW_MEMORY_GROWTH=1")
    add_cxx_flags("-Wno-warn-absolute-paths")
    add_cxx_flags("--bind --preload-file ${RESOURCES_DIR} -DNIGEL_EMSCRIPTEN")
endif()

# Release type flags
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -fno-inline")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -march=native -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -march=native -DNDEBUG")

# Line counting
add_custom_target(linecount
    COMMAND wc -l source/* | sort
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

include_directories(source)
add_subdirectory(source)

if(EMSCRIPTEN)

    add_executable(nigel webmain.cpp)
    target_link_libraries(nigel Nigel)

else()

    enable_testing()

    add_executable(nigel main.cpp)
    target_link_libraries(nigel Nigel)
    add_subdirectory(test)

endif()

